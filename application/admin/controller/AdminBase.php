<?php
/**
 * 	app后台
 *  =============================
 * 	后台父类
 * 	=============================
 *  Date: 2017-12-06
 */
namespace app\admin\controller;
use think\Controller;
use think\Session;
use think\Db;

class AdminBase extends Controller {
    /*
     * 初始化操作
     */
    public function _initialize() {
    	parent::_initialize();
    	//管理员信息
    	$admin_id = session('admin_id');
    	//验证登录
    	if (empty($admin_id)){
    		$this->redirect('Login/index');
    	}
    	
    	$admin_user = Db::name("app_admin")->where("admin_id='$admin_id'")->find();
    	$this->admin = $admin_user;
    	//配置项
    	$config = Db::name("app_config")->field('key,value')->select();
    	foreach ($config as $k=>$v){
    		$config[$v['key']]=$v['value'];
    	}
    	$this->config=$config;
    	//**************权限管理***********
    	$adminquanxian= Db::name("app_admin")->field('nav')->where(array('admin_id'=>$admin_id))->find();
    	$rules=$adminquanxian['nav'];
    	if(empty($rules)){
    		$this->error('此账号尚未分配权限');
    	}
    	$rules=explode(',', $rules);
    	foreach ($rules as $k=>$v){
    		$list[]=M("app_nav")->where('nav_id='.$v)->find();
    	}
    	foreach ($list as $k=>$v){
    		$v['nav_url'] = '/admin'.$v['nav_url'];
    		$value[$v['cat_id']][]=$v;
    	}
    	foreach ($value as $k=>$v){
    		$this->assign($k."_nav",$v);
    	}

    }
    
	/**
     * 添加财务日志方法
     * @param unknown $member_id
     * @param unknown $type  
     * @param unknown $content
     * @param unknown $money
     * @param unknown $money_type  收入=1/支出=2
     * @param unknown $currency_id  币种id 0是rmb
     * @return 
     */
    public function addFinance($member_id,$type,$content,$money,$currency_id,$money_type,$balance){
    	$data['member_id']=$member_id;
    	$data['type']=$type;
    	$data['content']=$content;
    	$data['money']=$money;
    	$data['add_time']=time();
    	$data['money_type'] = $money_type;
    	$data['currency_id']=$currency_id;
    	$data['balance']=$balance;
        $data['ip'] = request()->ip();
    	$list=M('app_finance')->add($data);
    	if($list){
    		return $list;
    	}else{
    		return false;
    	}
    }
    /**
     * 查询用户余额
     * @param unknown $member_id 用户id
     * @param unknown $account_id 币种id
     * @return unknown 返回该用户该币种的余额   类型float
     */
    public function balance($member_id,$account_id){
    	$bizhong = M('app_member_account')->where(array('member_id'=>$member_id))->find();
    	$yue = $bizhong['account_type_'.$account_id];
    	//dump($yue);die();
    	return $yue;
    }
    
    /**
     * 获得一个6位的数字字母（唯一）
     */
    public function getTuiJianCode(){
    	$str = rand('10000000','99999999');
    	$member = M('App_member_relation')->where(array('code'=>$str))->find();
    	if($member){
    		$str = $this->getTuiJianCode();
    	}
    	return $str;
    }
    
	/**
     * 查询身份证是否唯一 ('修改的时候后传member_id' 添加不需要)
     * @param unknown $srt 身份证号码
     * @param string $member_id 用户Id
     * @return boolean
     */
    public function getAttestation($srt,$member_id=""){
    	$where['idcard']=$srt;
    	if(!empty($member_id)){
    		$where['member_id']=array('neq',$member_id);
    	}
    	$list = db('App_member_relation')->where($where)->select();
    	if($list){
    		return false;		
    	}else {
    		return true;
    	}
    }
   	
    /**
     * 获得配置项
     * @return unknown
     */
    public function getConfig(){
    	$list=M('App_config')->select();
    	foreach ($list as $k=>$v){
    		$list[$v['key']]=$v['value'];
    	}
    	return $list;
    }
    
    
    
    
   
}