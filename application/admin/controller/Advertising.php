<?php
/**
 * 	app后台广告管理
 *  =============================
 *	Author: 李宏宇
 *  Date: 2017-10-13
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Advertising extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
    /**
     * 首页文章
     * 
     */
   	public function index(){
   		//添加分页读取数据
        $list=M('app_advertising')->order('sort desc')->paginate(15);
    	$page = $list->render ();
    	$list = $list->all();//object转换array
        foreach ($list as $k=>$v){
        	//进行标题与内容输出字符控制
             $list[$k]['title']=mb_substr((strip_tags(html_entity_decode($v['title']))),0,14,'utf-8');
             $list[$k]['content']=mb_substr((strip_tags(html_entity_decode($v['content']))),0,30,'utf-8');
        }
       	$this->assign('page',$page);
       	$this->assign('list',$list);
       	return $this->fetch();
    }
    
    public function addShow(){
    	$advertising_id = input('advertising_id');
    	if(!empty($advertising_id)){
    		$list = M('App_advertising')->where(array('advertising_id'=>$advertising_id))->find();
    		$this->assign('flash',$list);
    	}
    	return  $this->fetch();
    }
	
    public function add(){
    	$advertising_id = input('advertising_id');
    		if($_FILES["Filedata"]["tmp_name"]){
    			$file = request()->file('Filedata');
    			$info = $file->move(ROOT_PATH . 'public' . DS . 'pic' );
    			$data['pic'] = '/public/pic/' . str_replace('\\', '/', $info->getsaveName ());
    		}
    		
    		if(!empty(input('url'))){
    			$data['url']=input('url');
    		}
    		if(!empty(input('title'))){
    			$data['title']=input('title');
    		}
    		
    		if(!empty(input('content'))){
    			$data['content']=input('content');
    		}
    		
    		if(!empty(input('status'))){
    			$data['status']=input('status');
    		}
    		
    		
    		if(!empty(input('sort'))){
    			$data['sort'] = input('sort');
    		}
    		$data['add_time'] = time();
    		if(!empty($advertising_id)){
    			$data['advertising_id']=$advertising_id;
    			$rs=M('App_advertising')->update($data);
    		}else{
    			$rs=M('App_advertising')->insert($data);
    		}
    		
    		if($rs){
    			$this->success('操作成功');
    		}else{
    			$this->error('操作失败');
    		}
    }
    
    public function del(){
    	$advertising_id=input('advertising_id');
    	/**
    	 * 判断id是否存在 存在进行删除
    	 */
    	if(empty($advertising_id)){
    		$this->error('参数错误');
    	}else{
    		$res = db('App_advertising')->where(array('advertising_id'=>$advertising_id))->delete();
    	}
    	if(!$res){
    		$this->error('操作失败');
    	}
    	$this->success('操作成功');
    }
  
}