<?php
/**
 *  =============================
 * 	地级代理管理
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-12-12
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
use think\Collection;
use Think\page;
require_once (APP_PATH . 'api/FinanceApi.php');
class Area extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
    /**
     * 地级代理管理
     */
   	public function index(){
		$where['parentId'] = 0;
        $list = Db::name('Areas')->where($where)->order('areaId asc')->paginate(20,false,['query'=>input()]);
    	$page = $list->render();
    	$list = $list->all();
        foreach($list as $k=>$v){
        	$member = Db::name('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
        	$list[$k]['name'] = $member['loginName'];
        	$where1['member_id'] = $list[$k]['member_id'];
        	$arr[] = 4;
        	$arr[] = 5;
        	$where1['finance_type'] = array('in',$arr);
        	$all_chongzhi = Db::name('App_finance')->where($where1)->sum('money');
        	$list[$k]['all_chongzhi'] = $all_chongzhi;
        }
        
        $arr=M('App_config')->select();
        foreach ($arr as $k=>$v){
        	$arr[$v['key']]=$v['value'];
        
        }
        $this->assign('config',$arr);
        
       $this->assign('page',$page);
       $this->assign('info',$list);
       
       
       return $this->fetch();
    }
    /**
     * 市
     */
   	public function city(){
   		$id = input('id');
   		$now_area = Db::name('Areas')->where(array('areaId'=>$id))->find();
   		$where['parentId'] = $id;
   		$list = Db::name('Areas')->where($where)->paginate(20,false,['query'=>input()]);
   		$page = $list->render();
   		$list = $list->all();
   		foreach($list as $k=>$v){
   			$member = Db::name('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
   			$list[$k]['name'] = $member['loginName'];
   			$where1['member_id'] = $list[$k]['member_id'];
   			$arr[] = 4;
   			$arr[] = 5;
   			$where1['finance_type'] = array('in',$arr);
   			$all_chongzhi = Db::name('App_finance')->where($where1)->sum('money');
   			$list[$k]['all_chongzhi'] = $all_chongzhi;
   		}
   		$this->assign('info',$list);
   		$this->assign('p',$now_area);
   		$this->assign('page',$page);
   		return $this->fetch();
   	}
   	/**
   	 * 区
   	 */
   	public function qu(){
   		$id = input('id');
   		$now_area = Db::name('Areas')->where(array('areaId'=>$id))->find();
   		$where['parentId'] = $id;
   		$list = Db::name('Areas')->where($where)->paginate(20,false,['query'=>input()]);
   		$page = $list->render();
   		$list = $list->all();
   		foreach($list as $k=>$v){
   			$member = Db::name('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
   			$list[$k]['name'] = $member['loginName'];
   			$where1['member_id'] = $list[$k]['member_id'];
   			$arr[] = 4;
   			$arr[] = 5;
   			$where1['finance_type'] = array('in',$arr);
   			$all_chongzhi = Db::name('App_finance')->where($where1)->sum('money');
   			$list[$k]['all_chongzhi'] = $all_chongzhi;
   		}
   		$this->assign('info',$list);
   		$this->assign('p',$now_area);
   		$this->assign('page',$page);
   		return $this->fetch();
   	}
   	/**
   	 * 填写
   	 */
    public function details(){
    	$id = input('id');
    	$list = Db::name('Areas')->where(array('areaId'=>$id))->find();
    	
    	$this->assign('list',$list);
    	return $this->fetch();
    }
   	/**
   	 * 添加代理提交
   	 */
    public function addDaiLi(){
    	$id = input('id');
    	$loginName = input('loginName');
    	$user = Db::name('Users')->where(array('loginName'=>$loginName))->find();
    	$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$user['userId']))->find();
    	if($member_relation['is_baodan'] == 0){
    		$this->error('此用户不是报单中心');
    	}
    	$area = Db::name('Areas')->where(array('member_id'=>$member_relation['member_id']))->find();
    	if($area){
    		$arr['member_id'] = '';
    		$arr['status'] = 0;
    		$arr['add_time'] = '';
    		$res[] = Db::name('Areas')->where(array('areaId'=>$area['areaId']))->update($arr);
    	}
    	
    	$data['member_id'] = $member_relation['member_id'];
    	$data['add_time'] = time();
    	$data['status'] = 1;
    	$res[] = Db::name('Areas')->where(array('areaId'=>$id))->update($data);
    	if($res){
    		$this->success('成功');
    	}else{
    		$this->error('失败');
    	}
    	
    }
    /**
     * 删除代理
     */
    public function deleteDaiLi(){
    	$id = input('area_id');
    	$arr['member_id'] = '';
    	$arr['status'] = 0;
    	$arr['add_time'] = '';
    	$res[] = Db::name('Areas')->where(array('areaId'=>$id))->update($arr);
    	if($res){
    		$this->success('成功');
    	}else{
    		$this->error('失败');
    	}
    }
    
    
}