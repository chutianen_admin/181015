<?php
/**
 * 	app后台轮播
 *  =============================
 * 	后台父类
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Art extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
    /**
     * 首页文章
     * 
     */
   	public function index(){
   		
        $list=M('app_article')->order('sort desc')->paginate(15);
    	$page = $list->render ();
    	$list = $list->all();
        foreach ($list as $k=>$v){
             $list[$k]['title']=mb_substr((strip_tags(html_entity_decode($v['title']))),0,14,'utf-8');
             $list[$k]['content']=mb_substr((strip_tags(html_entity_decode($v['content']))),0,30,'utf-8');
        }
       $this->assign('page',$page);
       $this->assign('info',$list);
       return $this->fetch();
    }
    /**
     * 留言管理
     *
     */
    public function message(){
    	
    	$list=M('Message')->order('status ,add_time desc')->limit($Page->firstRow.','.$Page->listRows)->paginate(15);
    	$page = $list->render ();
    	foreach ($list as $k=>$v){
    		$info[$k]['title']=mb_substr((strip_tags(html_entity_decode($v['title']))),0,14,'utf-8');
    		$info[$k]['content']=mb_substr((strip_tags(html_entity_decode($v['content']))),0,30,'utf-8');
    	}
    	$this->assign('list',$list);
    	$this->assign ( 'page',$page); // 赋值分页输出
    	return $this->fetch();
    }
    /**
     * 留言回复
     *
     */
    public function insert(){
    	if(IS_POST){
    		$id = input('post.id');
    		$title = input('post.title');
    		$content = input('post.content');
    		if(empty($title)||empty($content)){
    			$data['status'] = -1;
    			$data['info'] = '请填写完整信息';
    			$this->ajaxReturn($data);
    		}
    		$data['member_id']=session('USER_KEY_ID');
    		$data['title']= $title;
    		$data['content'] =  input('post.content','','html_entity_decode');//内容
    		$data['add_time']=time();
    		$data['back_id']=$id;
    		$data['status']=0;
    		$r=M('Message')->add($data);
    		M('Message')->where('id='.$id)->setField('status',1);
    		if($r){
				$this->success('提交成功');
			}else{
				$this->success('服务器繁忙,请稍后重试');
			}
    		
    	}
    	$id=I('id');
    	$this->assign('id',$id);
    	return $this->fetch();
    	
    }
    //修改文章
    public function details(){
    	if(IS_POST){
    		$data['type'] = input('post.class_id');//文章类型
    		$data['top'] = input('post.top');//是否在首页显示
    		$data['sort'] = input('post.sort');//排序
    		$data['title']=input('post.title');//标题
    		
    		if($_FILES["pic"]["tmp_name"]){
    			$file = request()->file('pic');
    			$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
    			$data['pic'] = '/public/uploads/' . str_replace('\\', '/', $info->getsaveName ());
    		}
    		$data['content']=input('post.content','','html_entity_decode');//内容
    		$data['add_time']=time();
    		
    		if($_POST['article_id']){
    			$data['article_id']=input('post.article_id');
    			$list=M('app_article')->update($data);
    		}else{
    			$list=M('app_article')->add($data);
    		}
    		if($list){
    			$this->success('操作成功');
    		}else{
    			$this->error('操作失败');
    		}
    		
    	}
    	if($_GET['article_id']){
    		$article_id=I('get.article_id');
	    	if(!$article_id){
	    		$this->error('参数不全');
	    	}
	    	$list=M('app_article')->where(array('article_id'=>$article_id))->find();
	    	if(!$list){
	    		$this->error('参数错误');
	    	}
	    	$this->assign('list',$list);
    	}
    	
    	/**
    	 * 查询分类信息
    	 */
    	$class = db('App_article_class')->select();
    	$this->assign('class',$class);
    	return $this->fetch();
    }
    /**
     * 删除文章
     * return boolen
     */
    public function delete(){
        $id = input('article_id');
        $re = Db::name('app_article')->where(array('article_id'=>$id))->delete();
        if($re){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
            return;
        }
    }
    /**********************************李宏宇 文章分类 2017-10-11*****************************************/
    
    public function articleClass(){
    	$list = db('App_article_class')->select();
    	$this->assign('list',$list);
    	return $this->fetch();
    }
    /**
     * 
     */
    public function articleAdd(){
    	$class_id = input('class_id');
    	/**
    	 * 判断id是否存在  存在返回修改数据
    	 */
    	if(!empty($class_id)){
    		$list = db('App_article_class')->where(array('class_id'=>$class_id))->find();
    		$this->assign('list',$list);
    	}
    	 return $this->fetch();
    }
    
    public function addClass(){
    	$class_id=input('class_id');
    	$name = input('name');
    	/**
    	 * 判断id是否存在 存在进行修改否则进行添加
    	 */
    	if(empty($class_id)){
    		$insert['name'] = $name;
    		$insert['status'] = 1;
    		$insert['add_time'] = time();
    		$res = db('App_article_class')->insert($insert);
    	}else{
    		$update['class_id'] = $class_id;
    		$update['name'] = $name;
    		$update['add_time'] = time();
    		$res = db('App_article_class')->update($update);
    	}
    	
    	if($res){
    		$this->success('操作成功');
    	}else{
    		$this->error('操作失败');
    		return;
    	}   
    }
    public function delClass(){
    	$class_id=input('class_id');
    	/**
    	 * 判断id是否存在 存在进行删除
    	 */
    	if(empty($class_id)){
    		$this->error('参数错误');
    	}else{
    		$res = db('App_article_class')->where(array('class_id'=>$class_id))->delete();
    	}
    	if(!$res){
    		$this->error('操作失败');
    	}
    	$this->success('操作成功');
    }
  
}