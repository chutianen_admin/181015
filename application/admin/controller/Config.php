<?php
/**
 * 	app后台配置项
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Config extends AdminBase {
	public function _initialize(){
		parent::_initialize();
	}
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	
	public function index(){
		$list = $this->getConfig();
		$this->assign('config',$list);
       	return $this->fetch();
     }
     public function fuwu(){
     	$list = $this->getConfig();
     	$this->assign('config',$list);
     	return $this->fetch();
     }
     /**
      * 服务商奖励管理
      */
     public function jiang(){
     	$list = $this->getConfig();
     	$this->assign('config',$list);
     	return $this->fetch();
     }
    
     
     
     /**
      * 支付参数修改
      */
     public function pay(){
     	$list=M('App_config')->select();
     	foreach ($list as $k=>$v){
     		$list[$v['key']]=$v['value'];
     		 
     	}
     	//精品区（组合）
     	if(!empty($list['boutique_pay'])){
     		$arr_id1 = explode(',', $list['boutique_pay']);
     	}
     	if(!empty($list['boutique_money'])){
     		$arr_money1 = explode(',', $list['boutique_money']);
     	}
     	
     	for($i=0;$i<count($arr_id1);$i++){
     		$arr[$arr_id1[$i]]['id'] = $arr_id1[$i];
     		$arr[$arr_id1[$i]]['money'] =$arr_money1[$i];
     	}
     	
     	$all = M('App_account_type')->select();
     	$count = count($all);
     	if(!empty($arr_id1)){
     		foreach($all as $k=>$v){
     			if(in_array($all[$k]['id'], $arr_id1)){
     				$all[$k]['status'] = 1;
     				$all[$k]['money'] = $arr[$all[$k]['id']]['money'];
     	
     			}else{
     				$all[$k]['status'] = 0;
     			}
     		}
     	}
     	
     	$this->assign('all',$all);
     	//增值区（组合）
     	if(!empty($list['premium_pay'])){
     		$arr_id2 = explode(',', $list['premium_pay']);
     	}
     	if(!empty($list['premium_money'])){
     		$arr_money2 = explode(',', $list['premium_money']);
     	}
     	for($i=0;$i<count($arr_id2);$i++){
     		$arr[$arr_id2[$i]]['id'] = $arr_id2[$i];
     		$arr[$arr_id2[$i]]['money'] =$arr_money2[$i];
     	}
     	$all1 = M('App_account_type')->select();
     	if(!empty($arr_id2)){
     		foreach($all1 as $k=>$v){
     			if(in_array($all1[$k]['id'], $arr_id2)){
     				$all1[$k]['status'] = 1;
     				$all1[$k]['money'] = $arr[$all1[$k]['id']]['money'];
     			}else{
     				$all1[$k]['status'] = 0;
     			}
     		}
     	}
     	//精品区（单积分）
     	if(!empty($list['boutique_one_pay'])){
     		$arr_id3 = explode(',', $list['boutique_one_pay']);
     	}
     	if(!empty($list['boutique_one_pay_bili'])){
     		$arr_money3 = explode(',', $list['boutique_one_pay_bili']);
     	}
     	for($i=0;$i<count($arr_id3);$i++){
     		$arr[$arr_id3[$i]]['id'] = $arr_id3[$i];
     		$arr[$arr_id3[$i]]['money'] =$arr_money3[$i];
     	}
     	$all3 = M('App_account_type')->select();
     	if(!empty($arr_id3)){
     		foreach($all3 as $k=>$v){
     			if(in_array($all3[$k]['id'], $arr_id3)){
     				$all3[$k]['status'] = 1;
     				$all3[$k]['money'] = $arr[$all3[$k]['id']]['money'];
     			}else{
     				$all3[$k]['status'] = 0;
     			}
     		}
     	}
     	//增值区（单积分）
     	if(!empty($list['premium_one_pay'])){
     		$arr_id4 = explode(',', $list['premium_one_pay']);
     	}
     	if(!empty($list['premium_one_pay_bili'])){
     		$arr_money4 = explode(',', $list['premium_one_pay_bili']);
     	}
     	for($i=0;$i<count($arr_id4);$i++){
     		$arr[$arr_id4[$i]]['id'] = $arr_id4[$i];
     		$arr[$arr_id4[$i]]['money'] =$arr_money4[$i];
     	}
     	$all4 = M('App_account_type')->select();
     	if(!empty($arr_id4)){
     		foreach($all4 as $k=>$v){
     			if(in_array($all4[$k]['id'], $arr_id4)){
     				$all4[$k]['status'] = 1;
     				$all4[$k]['money'] = $arr[$all4[$k]['id']]['money'];
     			}else{
     				$all4[$k]['status'] = 0;
     			}
     		}
     	}
     	//店铺申请支付积分
     	if(!empty($list['shop_type'])){
     		$arr_id5 = explode(',', $list['shop_type']);
     	}
     	for($i=0;$i<count($arr_id5);$i++){
     		$arr[$arr_id5[$i]]['id'] = $arr_id5[$i];
     	}
     	$all5 = M('App_account_type')->select();
     	if(!empty($arr_id5)){
     		foreach($all5 as $k=>$v){
     			if(in_array($all5[$k]['id'], $arr_id5)){
     				$all5[$k]['status'] = 1;
     			}else{
     				$all5[$k]['status'] = 0;
     			}
     		}
     	}
     	//店铺申请区域开关
     	$array = $this->quyuArray();
     	if(!empty($list['shop_qu'])){
     		$all_id6 = explode(',', $list['shop_qu']);
     	}
     	foreach($array as $k=>$v){
     		if(in_array($array[$k]['id'], $all_id6)){
     			$array[$k]['status'] = 1;
     		}else{
     			$array[$k]['status'] = 0;
     		}
     	}
     	
     	$type = db('App_account_type')->select();
     	$this->assign('type',$type);
     	$this->assign('all1',$all1);
     	$this->assign('all3',$all3);
     	$this->assign('all4',$all4);
     	$this->assign('all5',$all5);
     	$this->assign('all6',$array);
     	$this->assign('config',$list);
     	return $this->fetch();
     }
     public function quyuArray(){
     	$a['id'] = 1;
     	$a['name'] = '精品区';
     	$b['id'] = 2;
     	$b['name'] = '增值区';
     	$c['id'] = 3;
     	$c['name'] = '公益区';
     	$array = array($a,$b,$c);
     	return $array;
     	
     }
     
     /***********************************************************************************************************************************/
  
    
     /**
      * 奖励参数设置
      */
     public function boss(){
     	$list=M('Config')->select();
     	foreach ($list as $k=>$v){
     		$list[$v['key']]=$v['value'];
     	}
     	$all = M('Account_type')->select();
     	foreach($all as $m=>$n){
     		if($all[$m]['id'] == $list['jiaru_cash']){
     			$all[$m]['status'] = 1;
     		}
     	}
     	
     	$this->assign('all',$all);
     	$this->assign('config',$list);
	    $this->display();
     }
     /**
      * 奖励参数修改提交
      */
     public function updateBoss(){
     	
     	if($_POST['vr_danwei']){
     		$now_price = $this->getNowPrice(1);
     		$data['status'] = 1;
     		$list = M('Ratio')->where(array('status'=>0))->save($data);
     		$arr['first_price'] = $now_price[0]['money'];
     		$arr['num'] = 0;
     		$arr['add_time'] = time();
     		$arr['status'] = 0;
     		$arr['up_num'] = $_POST['vr_danwei'];
     		$arr['up_bili'] = $_POST['vr_money'];
     		$re = M('Ratio')->add($arr);
     	}	
     	foreach ($_POST as $k=>$v){	 
     		$rs[]=M('KConfig')->where(C("DB_PREFIX")."k_config.key='{$k}'")->setField('value',$v);
     	}
     	if($rs){
     		$this->success('配置修改成功');
     	}else{
     		$this->error('配置修改失败');
     	}
     }
     /**
      * 奖励参数设置
      */
     public function chongzhi(){
     	$list=M('KConfig')->select();
     	foreach ($list as $k=>$v){
     		$list[$v['key']]=$v['value'];
     	}
     	$this->assign('config',$list);
     	$this->display();
     }
     public function doChongzhi(){
     
     	foreach ($_POST as $k=>$v){
     		$res[]=M('Config')->where(C("DB_PREFIX")."config.key='{$k}'")->setField('value',$v);
     	}
     	
     	if($res){
     		$this->success('参数修改成功');
     	}else{
     		$this->error('参数修改失败');
     	}
     }
     /**
      * 获得当前股票交易金额
      * @param unknown $now_num
      * @return number
      */
     public function getNowPrice($now_num){//200
     	$where['type'] = 'buy';
     	$where['status'] = 1;
     	$ratio = M('Ratio')->where(array('status'=>0))->find();
     	$where['ratio_id'] = $ratio['id'];
     	$num = M('Orders')->where($where)->sum('num');
     	$order = M('Orders')->where($where)->order('add_time desc')->find();
     	if(empty($order)){
     		$order['price'] = 1;
     	}
     	$base = $this->config['st_up_num'];
     	$bili = $this->config['st_up_bili'];
     	//取余
     	$r = fmod($num,$base);   //99    0    1
     	//没满足第一次涨价的股票量
     	$a = $base-$r;			//1    100   99
     	if($now_num>$a){
     		$f = $a;
     	}else{
     		$f = $now_num;		//1    1      1
     	}
     	//满足涨价的股票量
     	$b = $now_num-$f;   	// 0     0          0
     	//涨了几次价格 (取整)
     	$c = ceil($b/$base);   // 0     0       0
     	$d = fmod($b,$base);   //1     1       1
     	for($i=0;$i<$c+1;$i++){
     		//     		$now_bili = $i*$bili;
     		if($i == 0){
     			$arr[$i]['num'] = $f;
     			//     			if($now_num == 1){
     			//判断是否涨价
     			if($r == 0){
     				if($num !=0){
     					$now_bili = $bili;
     				}else{
     					$now_bili = $i*$bili;
     				}
     			}else{
     				$now_bili = $i*$bili;
     			}
     			//     			}else{
     			//     				$now_bili = $i*$bili;
     			//     			}
     		}else{
     			if($i == $c){
     				if($d == 0){
     					//整除的
     					$arr[$i]['num'] = $base;
     				}else{
     					$arr[$i]['num'] = $d;
     				}

     			}else{
     				$arr[$i]['num'] = $base;
     			}
     			$now_bili = $i*$bili;
     		}
     		$arr[$i]['money'] = $order['price']+$now_bili;
     		$arr[$i]['all_money'] = $arr[$i]['money']*$arr[$i]['num'];
     	}
     	return $arr;
     }
     
     public function updateSheZhi(){
     	//其他配置想修改
     	foreach ($_POST as $k=>$v){
     		 
     		$rs[]=M('app_config')->where("wst_app_config.key='{$k}'")->setField('value',$v);
     	}
     	if($rs){
     		$this->success('配置修改成功');
     	}else{
     		$this->error('配置修改失败');
     	}
     }
    /**
     * 修改配置项
     */
	public function updateCofig(){
		
		
		$all = M('App_account_type')->select();
		//精品区组合支付
		foreach($all as $k=>$v){
			if(!empty(I('boutique_conversion_'.$all[$k]['id']))){
				$conversion[] = I('boutique_conversion_'.$all[$k]['id']);
				$money[] = I('boutique_money_'.$all[$k]['id']);
			}
		}
		if(!empty($conversion)){
			$_POST['boutique_pay'] = implode(',',$conversion);
			$_POST['boutique_money'] = implode(',',$money);
		}else{
			$_POST['boutique_pay'] = '';
			$_POST['boutique_money'] = '';
		}
		//增值区组合支付
		foreach($all as $k=>$v){
			if(!empty(I('premium_conversion_'.$all[$k]['id']))){
				$conversion1[] = I('premium_conversion_'.$all[$k]['id']);
				$money1[] = I('premium_money_'.$all[$k]['id']);
			}
		}
		if(!empty($conversion1)){
			$_POST['premium_pay'] = implode(',',$conversion1);
			$_POST['premium_money'] = implode(',',$money1);
		}else{
			$_POST['premium_pay'] = '';
			$_POST['premium_money'] = '';
		}
		//精品区单积分支付
		foreach($all as $k=>$v){
			if(!empty(I('boutique_one_'.$all[$k]['id']))){
				$conversion2[] = I('boutique_one_'.$all[$k]['id']);
				$money2[] = I('boutique_one_bili_'.$all[$k]['id']);
			}
		}
		if(!empty($conversion2)){
			$_POST['boutique_one_pay'] = implode(',',$conversion2);
			$_POST['boutique_one_pay_bili'] = implode(',',$money2);
		}else{
			$_POST['boutique_one_pay'] = '';
			$_POST['boutique_one_pay_bili'] = '';
		}
		//增值区单积分支付
		foreach($all as $k=>$v){
			if(!empty(I('premium_one_'.$all[$k]['id']))){
				$conversion3[] = I('premium_one_'.$all[$k]['id']);
				$money3[] = I('premium_one_bili_'.$all[$k]['id']);
			}
		}
		if(!empty($conversion3)){
			$_POST['premium_one_pay'] = implode(',',$conversion3);
			$_POST['premium_one_pay_bili'] = implode(',',$money3);
		}else{
			$_POST['premium_one_pay'] = '';
			$_POST['premium_one_pay_bili'] = '';
		}
		//店铺申请积分选择
		foreach($all as $k=>$v){
			if(!empty(I('shop_type_'.$all[$k]['id']))){
				$conversion4[] = I('shop_type_'.$all[$k]['id']);
			}
		}
		if(!empty($conversion4)){
			$_POST['shop_type'] = implode(',',$conversion4);
		}else{
			$_POST['shop_type'] = '';
		}
		//店铺申请区域选择
		$array = $this->quyuArray();
		foreach($array as $k=>$v){
			if(!empty(I('shop_qu_'.$array[$k]['id']))){
				$conversion5[] = I('shop_qu_'.$array[$k]['id']);
			}
		}
		if(!empty($conversion5)){
			$_POST['shop_qu'] = implode(',',$conversion5);
		}else{
			$_POST['shop_qu'] = '';
		}
		
		
		//其他配置想修改
     	foreach ($_POST as $k=>$v){
     		
     		$rs[]=M('app_config')->where("wst_app_config.key='{$k}'")->setField('value',$v);
     	}
     	if($rs){
     		$this->success('配置修改成功');
     	}else{
     		$this->error('配置修改失败');
     	}
    }
	//滚动条数据设置
	public function huilv(){
		$count = M('Huilv')->count (); // 查询满足要求的总记录数
        // 实例化分页类 传入总记录数和每页显示的记录数
        $Page  = new \Think\Page($count,20);
        //分页显示输出性
       $show = $Page->show();
       $list=M('Huilv')->order('add_time')->limit($Page->firstRow.','.$Page->listRows)->select();
       $this->assign('page',$show);
       $this->assign('info',$list);
       $this->display();
	}
	public function delete(){
		$where['id']=I('id');
		$rs=M('Huilv')->where($where)->delete();
     	if($rs){
     		$this->success('配置修改成功');
     	}else{
     		$this->error('配置修改失败');
     	}
	}
	 //修改矿机数据
    public function details(){
    	if(IS_POST){   		
    		$data['name_left']=I('post.name_left');
    		$data['name_right']=I('post.name_right');
    		$data['num']=I('post.num');
    		$data['jiantou']=I('post.sign');
    		$data['add_time']=time();
    		if($_POST['id']){
    			$data['id']=I('post.id');
    			$list=M('Huilv')->save($data);
    		}else{
    			$list=M('Huilv')->add($data);
    		}
    		if($list){
    			$this->success('操作成功');exit();
    		}else{
    			$this->error('操作失败');exit();
    		}
    		
    	}
    	if($_GET['id']){
    		$id=I('get.id');
	    	if(!$id){
	    		$this->error('参数不全');exit();
	    	}
	    	$list=M('Huilv')->where('id='.$id)->find();
	    	if(!$list){
	    		$this->error('参数错误');exit();
	    	}
    	}
    	$this->assign('list',$list);
    	$this->display();
    }
   public  function  qingkong(){
   	$Model=new Model();
   	$sql="TRUNCATE TABLE yang_finance";
   		$Model->execute($sql);
   	$sql="TRUNCATE TABLE yang_kuangji_log";
   		$Model->execute($sql);
   	M('Member')->where('member_id=90')->setField('xnb',0);
   	M('Member')->where('member_id=90')->setField('jihuobi',0);
   	$sql="TRUNCATE TABLE yang_money_email";
   		$Model->execute($sql);
   	$sql="DELETE FROM yang_member WHERE member_id!=90";
   	$Model->execute($sql);
   	$this->success('数据库清空成功');
   }
   /**
    * 积分管理
    */
   public function qita(){
   		$list = M('app_account_type')->select();
   		foreach($list as $k=>$v){
   			$arr = explode(',', $list[$k]['conversion']);
   			$where['id'] = array('in',$arr); 
   			$name = M('app_account_type')->field('name')->where($where)->select();
   			$list[$k]['conversion_name'] =$name;
   		}
   		$this->assign('list',$list);
   		return $this->fetch();
   }
   /**
    * 积分修改
    */
   public function jifenUpdate(){
   		$id = I('id');
   		$list = M('app_account_type')->where(array('id'=>$id))->find();
   		$arr = explode(',', $list['conversion']);
   		$map['id']  = array('neq',$id);
   		$all = M('app_account_type')->where($map)->select();
   		foreach($all as $k=>$v){
   			if(in_array($all[$k]['id'], $arr)){
   				$all[$k]['status'] = 1;
   			}else{
   				$all[$k]['status'] = 0;
   			}
   		}
   		$this->assign('all',$all);
   		$this->assign('list',$list);
   		return $this->fetch();
   }
   /**
    * 积分修改提交
    */
   public function jifenSave(){
//    		$aa =input();
//    		dump($aa);die;
	   	$id = input('id');
   		$all = M('app_account_type')->select();
   		foreach($all as $k=>$v){
   			if(input('conversion_'.$all[$k]['id'])){
   				$conversion[] = input('conversion_'.$all[$k]['id']);
   			}
   		}
   		$data['name'] = input('name');
   		$data['transfer'] = input('transfer');
   		$data['tixian'] = input('tixian');
   		if(!empty($conversion)){
   			$data['conversion'] = implode(',',$conversion);
   		}else{
   			$data['conversion'] = '';
   		}
   		$data['limit_type'] = input('limit_type');
   		$data['limit_transfer_money'] = input('limit_transfer_money');
   		$data['transfer_fee_bili'] = input('transfer_fee_bili');
   		$data['limit_withdrawals_money'] = input('limit_withdrawals_money');
   		$data['withdrawals_fee_bili'] = input('withdrawals_fee_bili');
   		$res = M('app_account_type')->where(array('id'=>$id))->save($data);
   		if($res){
   			$this->success('修改成功');
   		}else{
   			$this->error('修改失败');
   		}
   }
   
   
   
   
   /**
    * 奖励比例
    */
   public function zhitui(){
   		$list=M('App_config')->select();
   		foreach ($list as $k=>$v){
   			$list[$v['key']]=$v['value'];
   		}
   		/***********************************褚天恩 2017.6.11*******************/   		
   		$all = M('App_account_type')->select();
   		if(!empty($list['jiangli_cash'])){
   			$arr_id = explode(',', $list['jiangli_cash']);
   		}
   		if(!empty($list['jiangli_cash_bili'])){
   			$arr_bili = explode(',', $list['jiangli_cash_bili']);
   		}
   		for($i=0;$i<count($arr_id);$i++){
   		   	$arr[$arr_id[$i]]['id'] = $arr_id[$i];
   		   	$arr[$arr_id[$i]]['money'] =$arr_bili[$i];
   		}

   		foreach($all as $k=>$v){
   		   	if(in_array($all[$k]['id'], $arr_id)){
   		   		$all[$k]['status'] = 1;
   		   		$all[$k]['money'] = $arr[$all[$k]['id']]['money'];   		
   		   	}else{
   		   		$all[$k]['status'] = 0;
   		   	}
   		}
   		$this->assign('all',$all);
   		$this->assign('config',$list);
   		return $this->fetch();
   }
   /**
    * 奖励返还比例提交
    */
   public function biliUpdate(){
	   	$all = M('App_account_type')->select();
	   	foreach($all as $k=>$v){
	   		if(input('conversion_'.$all[$k]['id'])){
	   			$conversion[] = input('conversion_'.$all[$k]['id']);
	   			$money[] = input('money_'.$all[$k]['id']);
	   		}
	   	}
	   	$_POST['jiangli_cash'] = implode(',',$conversion);
	   	$_POST['jiangli_cash_bili'] = implode(',',$money);
	   	foreach ($_POST as $k=>$v){
	   		$rs[]=M('App_config')->where("jf_app_config.key='{$k}'")->setField('value',$v);
	   	}
	   	if($rs){
	   		$this->success('配置修改成功');
	   	}else{
	   		$this->error('配置修改失败');
	   	}
   }
   
  
   
   
   
   
}