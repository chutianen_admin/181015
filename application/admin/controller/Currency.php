<?php
namespace app\admin\controller;
use Think\Db;
require_once (APP_PATH .'api/FinanceApi.php');
class Currency extends AdminBase {
	protected $currency;
	public function _initialize(){
		parent::_initialize();
// 		$this->rpcuser="user";
// 		$this->rpcpassword="passwd";
// 		$this->rpcport="29998";
// 		$this->rpcurl="127.0.0.1";
// 		$this->rpckey="";
// 		$this->currency=M('Currency');
	}
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	
	/**
	 * 充值记录
	 */
	public function tibi_index(){
// 		$this->tibi_save($currency);
		$account=I('account');
		if(!empty($account)){
			$name=M("Users")->where(array('loginName'=>$account))->find();
			$where["member_id"]=$name['userId'];
		}
		$list = Db::name('App_chongzhi')->where($where)->order('add_time desc')->paginate(20,false,['query'=>input()]);
		$page = $list->render ();
		$list = $list->all();
		foreach($list as $k=>$v){
			$user = Db::name('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
			$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$list[$k]['member_id']))->find();
			$list[$k]['account'] = $user['loginName'];
			$list[$k]['bank_card'] = $member_relation['bank_card'];
			$list[$k]['kaihuren'] = $member_relation['kaihuren'];
			$list[$k]['kaihu_address'] = $member_relation['kaihu_address'];
			$list[$k]['zhifubao'] = $member_relation['zhifubao'];
			$list[$k]['weixin'] =$member_relation['weixin'];
			$list[$k]['phone'] = $member_relation['phone'];
			$account_type = Db::name('App_account_type')->where(array('id'=>$list[$k]['account_type']))->find();
			$list[$k]['account_name'] = $account_type['name'];
		}
		$this->assign('list',$list);
		$this->assign('page',$page);
		return $this->fetch();
	}
	/**
	 * 充值审核
	 */
	public function shenhe(){
		$id = I('id');
		$data['status'] = I('status');
		if(I('status') == 1){
			$chongzhi = Db::name('App_chongzhi')->where(array('id'=>$id))->find();
			$account_type = Db::name('App_account_type')->where(array('id'=>$chongzhi['account_type']))->find();
			
			$re[] = Db::name('App_member_account')->where(array('member_id'=>$chongzhi['member_id']))->setInc('account_type_'.$account_type['id'],$chongzhi['num']);
			$finance = new \FinanceApi();
			//余额
			$re[] = $finance->addFinance($chongzhi['member_id'],5, '充值成功,金额:'.$chongzhi['num'],1,$chongzhi['num'],$account_type['id']);
		}
		$res = Db::name('App_chongzhi')->where(array('id'=>$id))->save($data);
		if($res){
			$this->success('成功');
		}else{
			$this->error('审核失败');
		}

	}
	
	
}