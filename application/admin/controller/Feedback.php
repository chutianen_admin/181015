<?php
/**
 * 	app后台问题反馈
 *  =============================
 *	Author: 李宏宇
 *  Date: 2017-10-15
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Feedback extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
 
   	public function index(){
        $list=db('App_feedback')->paginate(15);
    	$page = $list->render ();
    	$list = $list->all();
        foreach ($list as $k=>$v){
        	//获取用户
        	 $res = db('App_member_relation')->where(array('member_id'=>$list[$k]['member_id']))->find();
        	 $user = Db::name('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
        	 $list[$k]['name'] = $res['name']?$res['name']:'未填写';
        	 $list[$k]['loginName'] = $user['loginName']?$user['loginName']:'未填写';
        	 
        	 //限制内容字符长度 
             $list[$k]['content']=mb_substr((strip_tags(html_entity_decode($v['content']))),0,30,'utf-8');
        }
        
      
       $this->assign('page',$page);
       $this->assign('info',$list);
       return $this->fetch();
    }
    
    public function details(){
    	$feedback_id = input('feedback_id');
    	$list = db('App_feedback')->where(array('feedback_id'=>$feedback_id))->find();
    	$this->assign('list',$list);
    	return $this->fetch();
    	
    }
    
}