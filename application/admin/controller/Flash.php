<?php
/**
 * 	app后台轮播
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Flash extends AdminBase {
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
	/**
	 * 轮播图页面及上传
	 * @return Ambigous <\think\mixed, string>
	 */
	Public function add(){
		$flash=M('app_flash');
		if(IS_POST){
			if($_FILES["Filedata"]["tmp_name"]){
				$file = request()->file('Filedata');
				$info = $file->move(ROOT_PATH . 'public' . DS . 'pic' );
				$data['pic'] = '/public/pic/' . str_replace('\\', '/', $info->getsaveName ());
			}
			if(!empty($_POST['jump_url'])){
				$data['jump_url']=$_POST['jump_url'];
			}
			if(!empty($_POST['sort'])){
				$data['sort']=$_POST['sort'];
			}
			if(!empty($_POST['title'])){
				$data['title']=$_POST['title'];
			}
			$data['add_time']=time();
			
			if(!empty($_POST['flash_id'])){
				$data['flash_id']=$_POST['flash_id'];
				//dump($data);die;
				$rs=$flash->update($data);
			}else{
				$rs=$flash->add($data);
			}
			if($rs){
				$this->success('操作成功');
			}else{
				$this->error('操作失败');
			}
		}else{
			if(!empty($_GET['flash_id'])){
				$list=$flash->where('flash_id='.$_GET['flash_id'])->find();
				$this->assign('flash',$list);
			}
			return $this->fetch();
		}
	}
	/**
	 * 轮播图展示
	 * @return Ambigous <\think\mixed, string>
	 */
	public function index(){
		$list=M('app_flash')->select();
		$this->assign('flash',$list);
		$this->assign('empty','暂无数据');
		return $this->fetch();
    }
    /**
     * 轮播图删除
     */
    public function del(){
    	if(!empty($_GET['flash_id'])){
            $list=M('app_flash')->where('flash_id='.$_GET['flash_id'])->delete();
        }
    	if($list){
    		$this->success('删除成功');
    	}else{
    		$this->error('删除失败');
    	}
    }
}