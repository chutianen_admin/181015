<?php
namespace app\admin\controller;
class Goods extends AdminBase{
    //空操作
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
   
    /**
     * 商品列表
     */
    public function index(){
    	$list = db('Goods')->order('id desc')->paginate(10,false,['query'=>input()]);
    	$page = $list->render();
    	$this->assign('page',$page);//分页
    	$this->assign('list',$list);// 赋值数据集
		return $this->fetch();
    }
  
    /**
     * 修改商品页面
     */
    public function add_goods(){
    	$gc=db("Goods_category")->select();
        $this->assign("gc",$gc);
        $id=I("id","");
        if(!empty($id)){
            $list=M("Goods")->where(['id'=>$id])->find();
            $this->assign("list",$list);
        }
       return $this->fetch();
    }
    /**
     * 添加商品方法
     */
    public function goods_add(){
		$id=I("id","");
    	foreach ($_POST as $k){
    		if(empty($k)){
    			$this->error("请补全信息");
    		}
    	}
		    	$file = request()->file('pic');
		    	if(empty($file)){
		    		if(empty($id)){
		    			$this->error('请上传图片');
		    		}
		    		$c_pic=$_POST['c_pic'];
		    		
		    	}else{
		    		$info = $file->move(ROOT_PATH . 'public' . DS . 'pic' );
		    	}
		    
		    	if (!$info) {
		    		if(!empty($id)){
		    			$c_pic=$_POST['c_pic'];
		    		}else{
		    			$this->error($file->getError());
		    		}
		    	} else {
		    		$c_pic = '/public/pic/' . str_replace('\\', '/', $info->getsaveName ());
		    	}
    	   		$_POST['pic']=$c_pic; 
    	   		$_POST['status'] = 1; 
    	   		if(empty($id)){
    	   			
    	   			$_POST['add_time'] = time();
    	   			$_POST['shengyu_num'] = $_POST['all_num'];
    	   			$re=db("Goods")->add($_POST);
    	   		}else{
    	   			$re=db("Goods")->where(array('id'=>$id))->save($_POST);
    	   		}
    	   		if($re){
    	   			$this->success("提交成功",U('Goods/index'));
    	   		}else{
    	   			$this->error("提交失败");
    	   		}
    	   		
    }
    /**
     * 商品删除
     */
	public function del(){
		$id = I('id');
		$res = db('Goods')->where(array('id'=>$id))->delete();
		if($res){
			$this->success("删除成功",U('Goods/index'));
		}else{
			$this->error("删除失败");
		}
	}
    
    
    
    
    
    
    /**
     *商品分类
     */
    public function goodslist(){
    	$list = db('Goods_category')->order('id desc')->paginate(10,false,['query'=>input()]);
    	$page = $list->render();
    	$this->assign('page',$page);//分页
    	$this->assign('list',$list);// 赋值数据集
		return $this->fetch();
    }
    
    
    /**
     * 商品分类添加页面
     */
    public function add_goods_category(){
    	$id=input("id");
    	$gc=db("Goods_category")->where(['id'=>$id])->find();
    	if(!empty($gc)){
    		$this->assign("list",$gc);
    	}
    	return $this->fetch();
    }
    
    /**
     * 添加商品分类方法
     */
    public function goods_category_add(){
    	$id=input("id");
    	foreach ($_POST as $k){
    		if(empty($k)){
    			$this->error('请补全信息');
    		}
    	}
    	if(empty($id)){
    		$re=db("Goods_category")->add($_POST);
    	}else{
    		$re=db("Goods_category")->where(['id'=>$id])->save($_POST);
    	}
    	if($re){
    		$this->success("提交成功",U("Goods/goodslist"));
    	}else{
    		$this->error("提交失败");
    	}
    	
    }
    
    /**
     * 订单管理
     */
    public function goods_user(){
    	$list = db('Goods_user')->order('add_time desc')->paginate(15,false,['query'=>input()]);
		$page = $list->render();
    	
    	$list = $list->all();
    	foreach ($list as  $k=>$v){
    		$member = db('Users')->where(['userId'=>$v['member_id']])->find();
    		$list[$k]['member_name'] = $member['loginName'];
    		//查询商品名称
    		$res = db('goods')->where(['id'=>$v['goods_id']])->find();
    		$list[$k]['goods_name'] = $res['name'];
    	}
    	$this->assign('page',$page);//分页
    	$this->assign('list',$list);// 赋值数据集
		return $this->fetch();
    }
    
    
    /**
     * 审核订单
     */
    public function gu_pass(){
    	$id=input("id","");
    	$data['status'] = 1;
    	$re=db("Goods_user")->where(array('id'=>$id))->save($data);
    	if($re){
    		$this->success("审核成功");
    	}else{
    		$this->error("系统异常，审核失败");
    	}
    	
    }
    
    /**
     * 审核驳回
     */
    public function gu_no_pass(){
    	$id=I("id");
    	$data['status'] = -1;
    	$re=db("Goods_user")->where(array('id'=>$id))->save($data);
    	if($re){
    		//返钱这块 （如果不用就禁止掉）
    		$guc=M("Goods_user")->where(['id'=>$id])->find();
    		//返还钱数
//     		M("Member_account")->where(["member_id"=>$guc['member_id']])->setInc("xiaofei_num",($guc['num']*$guc['money']));
    		
    		$this->success("审核成功");
    	}else{
    		$this->error("系统异常，审核失败");
    	}
    	 
    }
}