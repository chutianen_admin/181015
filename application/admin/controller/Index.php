<?php
/**
 * 	app后台首页
 *  =============================
 * 	后台父类
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
require_once (APP_PATH .'api/FinanceApi.php');
require_once (APP_PATH .'api/RewardApi.php');
class Index extends AdminBase  {
 	public function _initialize(){
        parent::_initialize();
    }
	/**
	 * 后台首页
	 * @return Ambigous <\think\mixed, string>
	 */
    public function index(){
    	//获取今日的时间戳
    	$beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
    	$endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
    	//获取本月的时间戳
    	$beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
    	$endThismonth=mktime(23,59,59,date('m'),date('t'),date('Y'));
    	//获取本年时间戳
    	$startyear = mktime(0,0,0,1,1,date('Y',time())); 
    	$endyear = mktime(23,59,59,12,31,date("Y",time()));
    	
    	
		//上次登录时间
		$admin = db('App_admin')->where(['admin_id'=>$_SESSION['think']['admin_id']])->find();
		$list['login_time'] = $admin['login_time'];
		$list['time_ing'] = $admin['time_ing'];
		//获取今日的新增会员数量
		$where1['createTime'] = array('BETWEEN',array($beginToday,$endToday));
		$list['member_today_num'] = db('users')->where($where1)->count('userId');
		//获取本月的新增会员
		$where2['createTime'] = array('BETWEEN',array($beginThismonth,$endThismonth));
		$list['member_month'] = db('users')->where($where2)->count('userId');
		//获取年度新会员
		$where3['createTime'] = array('BETWEEN',array($startyear,$endyear));
		$list['member_year'] = db('users')->where($where3)->count('userId');
		//获取待激活的会员
		$where4['status'] = 0;
		$list['no_activated'] = db('App_member_relation')->where($where4)->count('id');
		//获取未激活
		$list['is_activated'] = db('App_member_relation')->where(array('status'=>1))->count('id');
		//获取待处理提现申请
		$list['application'] = db('Withdraw')->where('status = 0')->count('id');
		//获取充值待处理数据
		$list['recharge'] = db('App_chongzhi')->where('status = 0')->count('id');
		
		$where5['add_time'] = array('BETWEEN',array($beginToday,$endToday));
		$where5['status'] = 1;
		$list['today_chongzhi'] = db('App_chongzhi')->where($where5)->sum('num');
		
		
		
		$where6['add_time'] = array('BETWEEN',array($beginThismonth,$endThismonth));
		$where6['status'] = 1;
		$list['month_chongzhi'] = db('App_chongzhi')->where($where6)->sum('num');
		
		
		$where7['add_time'] = array('BETWEEN',array($startyear,$endyear));
		$where7['status'] = 1;
		$list['year_chongzhi'] = db('App_chongzhi')->where($where7)->sum('num');
		//获得当前的配置项中的两个业绩
		$config = $this->getConfig();
		$num['gu'] = Db::name('App_member_relation')->where(array('level'=>5))->count();
		$num['zhuan'] = Db::name('App_member_relation')->where(array('level'=>4))->count();
		
		
		$where_fen['status'] = 1;
		$where_fen['level'] = 5;
		//当前所有的股东的数量
		$count_fen = Db::name('App_member_relation')->where($where_fen)->count();
		//总分红份数 = 股东数 × 10份 ➕ 推荐股东奖励分红份数总额
		$all_fen_num = $count_fen*11;
// 		//每份分红金额 = 每月新增业绩 × 5% /总分红份数    这个是推荐股东的会员单独获得的  1份的金额
// 		$one_fen_money = $new_yeji * 0.05 / $all_fen_num;
		//股东获得的加权分红金额
		//$gudong_money = $one_fen_money * 10;
		
		
		$this->assign('all_fen_num',$all_fen_num);
		$this->assign('config',$config);
		$this->assign('num',$num);
		$this->assign('list',$list);
		
        return $this->fetch();
    }
    public function getImformation($time){
    	//查询的日期
    	$riqi = date('Y-m-d',$time[0]);
    	//查询的数据表
    	//$db = 'App_finance_'.date('Y_m',$time[0]);
    	//Y-m-d 格式时间
    	$ymd = $this->getYmdByTime($time);
    	//统计riqi这天的数据
    	$list['time'] = $riqi;
    	//收入充值TODO
    	// 			$list['get']
    	//提现TODO
    	// 			$list['give']
    	//奖励
    	$where1['add_time'] = array('between',$time);
    	$where1['finance_type'] = array('in',$this->getTypeArr());
    	$account_type = Db::name('App_account_type')->select();
    	foreach($account_type as $m=>$n){
    		$where1['account_type'] = $account_type[$m]['id'];
    		$list['jiang_account_type_'.$account_type[$m]['id']] =  Db::name('App_finance')->where($where1)->sum('money');
    		$list['jiang_account_type_'.$account_type[$m]['id']] = $list['jiang_account_type_'.$account_type[$m]['id']]>0?$list['jiang_account_type_'.$account_type[$m]['id']]:'0.00';
    	}
    	//新增会员
    	$map2['createTime'] = array('between',$ymd);
    	$list['num'] = M('Users')->where($map2)->count();
    	
    	return $list;
    }
    public function getYmdByTime($time){
    	$date[] = date('Y-m-d H:i:s',$time[0]);
    	$date[] = date('Y-m-d H:i:s',$time[1]);
    	return $date;
    }
    //获得奖励财务日志数组
    public function getTypeArr(){
    	//推荐奖励
    	$arr[] = 8;
    	//注册奖励
    	$arr[] = 9;
    	//对碰奖励
    	$arr[] = 10;
    	//管理奖励
    	$arr[] = 11;
    	//报单中心管理奖
    	$arr[] = 12;
    	return $arr;
    }
    
    /**
     * 今日零点到24点时间段
     * 数组形式返回
     */
    public function toDayTime(){
    	$start = strtotime(date('Y-m-d',time()));
    	$time[] = $start;
    	$time[] = $start+84600;
    	return $time;
    }
	/**
	*	测试
	*/
     public function show(){
		 $where['old_yeji'] = array('gt',0);
		 $where['zhitui_num'] = 0;
		 $list = Db::name('App_member_relation')->where($where)->select();
		 dump($list);
		 die();
		 
		 
		 
		 
		 $idcard_array = array();
		foreach($list as $k=>$v){
			$idcard = $list[$k]['idcard'];
			$res = Db::name('App_member_relation')->where(array('idcard'=>$idcard))->count();
			if($res > 1){
				if(!in_array($list[$k]['idcard'],$idcard_array)){
					$idcard_array[] = $idcard;
					$arr[]['idcard'] = $idcard;
				}
			}
		}
		
	 }
	 /**
	  * 结算对碰
	  */
	 public function duipengOver(){
	 	$where['duipeng_left'] = array('gt',0);
	 	$where['duipeng_righ'] = array('gt',0);
	 	$where['status'] = array('neq',0);
	 	$member_relation = Db::name('App_member_relation')->where($where)->select();
	 	$reward = new \RewardApi();
	 	foreach($member_relation as $k=>$v){
	 		$res[] = $reward->duipeng($member_relation[$k]['member_id']); 
	 	}
	 	if($res){
	 		$this->success('成功');
	 	}else{
	 		$this->error('失败');
	 	}
	 	
	 }
	 /**
	  * 清空数据
	  */
	 public function wipe(){
	 	$where1['member_id'] = array('neq',1);
	 	$where2['userId'] = array('neq',1);
	 	$r[] = Db::name('Users')->where($where2)->delete();
	 	$r[] = Db::name('App_member_relation')->where($where1)->delete();
	 	
	 	$r[] = Db::name('Line')->where($where1)->delete();
	 	$data['yeji_righ'] = 0;
	 	$data['yeji_left'] = 0;
	 	$data['duipeng_righ'] = 0;
	 	$data['duipeng_left'] = 0;
	 	$data['righ_num'] = 0;
	 	$data['left_num'] = 0;
	 	$data['left_surplus'] = 0;
	 	$data['righ_surplus'] = 0;
	 	$data['zhitui_num'] = 0;
	 	$r[] = Db::name('App_member_relation')->where(array('member_id'=>1))->update($data);
	 	$where3['member_id'] = array('neq',0);
	 	$r[] = Db::name('App_finance')->where($where3)->delete();
	 	$r[] = Db::name('withdraw')->where($where3)->delete();
	 	$r[] = Db::name('App_chongzhi')->where($where3)->delete();
	 	$r[] = Db::name('App_transfer')->where($where3)->delete();
	 	$r[] = Db::name('Goods_user')->where($where3)->delete();
	 	$r[] = Db::name('App_layer')->where($where3)->delete();
	 	$r[] = Db::name('App_member_account')->where($where3)->delete();
	 	$one['member_id'] = 1;
	 	$one['account_type_2'] = 99999999;
	 	$r[] = Db::name('App_member_account')->insert($one);
	 	$config['now_new_yeji'] = 0;
	 	$config['now_new_jili_yeji'] = 0;
	 	foreach ($config as $k=>$v){
	 		$rs[] = Db::name('app_config')->where("wst_app_config.key='{$k}'")->setField('value',$v);
	 	}
	 	
	 	$where4['id'] = array('neq',0);
	 	$r[] = Db::name('App_statistics')->where($where4)->delete();
	 	
	 	$where7['id'] = array('neq',1);
	 	$r[] = Db::name('Line')->where($where7)->delete();
	 	if($r){
	 		$this->success('成功');
	 	}else{
	 		$this->error('失败');
	 	}
	 	
	 }
	 /**
	  * 释放股东分红
	  */
	 public function releaseGuDong(){
	 	$now_new_yeji = input('now_new_yeji');
	 	if(!$now_new_yeji){
	 		$this->error('请填写需要释放的当月新增业绩');
	 	}
	 	$where['status'] = 1;
	 	$where['level'] = 5;
	 	//当前所有的股东的数量
	 	$count = Db::name('App_member_relation')->where($where)->count();
	 	if(empty($count)){
	 		$this->error('暂无股东');
	 	}
	 	$reward = new \RewardApi();
	 	$re = $reward->gudong($now_new_yeji);
	 	if($re){
	 		$this->success('释放成功');
	 	}else{
	 		$this->error('释放完成');
	 	}
	 	
	 }
	 /**
	  * 释放激励分红
	  */
	 public function releaseJiLi(){
	 	$now_new_jili_yeji = input('now_new_jili_yeji');
	 	if(!$now_new_jili_yeji){
	 		$this->error('请填写需要释放的当月新增业绩');
	 	}
	 	$reward = new \RewardApi();
	 	$re = $reward->jili($now_new_jili_yeji);
	 	if($re){
	 		$this->success('释放成功');
	 	}else{
	 		$this->error('释放完成');
	 	}
	 }
	 
	 
	 public function bbbbb(){
	 	$where['level'] = array('neq',5);
	 	$list = Db::name('App_member_relation')->where($where)->order('id desc')->select();
	 	foreach($list as $k=>$v){
	 		$data['yeji_righ'] = 0;
	 		$data['yeji_left'] = 0;
	 		$data['tream_num'] = 0;
	 		$data['yeji'] = 0;
	 		$res[] = Db::name('App_member_relation')->where(array('member_id'=>$list[$k]['member_id']))->update($data);
	 	}
	 	dump($res);
	 }
	 
	 
	 public function ccccc(){
	 	$where['level'] = array('neq',5);
	 	$where['status'] = 1;
	 	$list = Db::name('App_member_relation')->where($where)->order('id desc')->select();
	 	foreach($list as $k=>$v){
	 		$money = $this->getLevelMoney($list[$k]['level']);
	 		$res[] = $this->addYeJi($list[$k]['member_id'], $money);
	 		unset($money);
	 	}
	 	dump($res);
	 }
	 public function getLevelMoney($level){
	 	$arr[1] = 700;
	 	$arr[2] = 2100;
	 	$arr[3] = 7000;
	 	$arr[4] = 21000;
	 	return $arr[$level];
	 }
	 
	 public function addYeJi($member_id,$money){
	 	$reward = new \RewardApi();
	 	$list = $reward->getUpUserByjiedianId($member_id,9999);
	 	for($i=1;$i<count($list);$i++){
	 		if($list[$i-1]['quyu'] == 'righ'){
	 			$data['yeji_righ'] = $list[$i]['yeji_righ']+$money;
	 		}else{
	 			$data['yeji_left'] = $list[$i]['yeji_left']+$money;
	 		}
	 		//为上线加业绩
	 		$data['yeji'] = $list[$i]['yeji']+$money;
	 		//为上线加团队人数
	 		$data['tream_num'] = $list[$i]['tream_num']+1;
	 		$res[] = M('App_member_relation')->where('member_id='.$list[$i]['member_id'])->update($data);
	 		unset($data);
	 	}
	 	return $res;
	 }
	 
	 
	 
	 
	 
	 
}