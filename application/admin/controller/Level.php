<?php
/**
 * 	app后台等级
 * 	=============================
 *  Date: 2017-12-06
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Level extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
	public function index(){
		$list=M("App_level")->select();
		foreach($list as $k=>$v){
			$account = explode(',', $list[$k]['account_type_id']);
			$where['id'] = array('in',$account);
			$account_type = M('App_account_type')->where($where)->select();
			foreach($account_type as $m=>$n){
				$list[$k]['aa'][] = $account_type[$m]['name'];
			}
		}
		$config = Db::name('App_config')->select();
     	foreach ($config as $k=>$v){
     		$config[$v['key']]=$v['value'];
     	}
		$this->assign('config',$config);
		$this->assign("list",$list);
		return $this->fetch();
	}
	/**
	 * 参数修改
	 */
	public function l_dec(){
		$id=I("id","");
		if(empty($id)){
			$this->error("参数错误");
		}
		$level=M("App_level")->where(['id'=>$id])->find();
		if(empty($level)){
			$this->error("无效数据");
		}
		
		/**********开通奖励****************/
		if(!empty($level['account_type_id'])){
			$arr_id = explode(',', $level['account_type_id']);//获取当前拥有的币种id
		}
		if(!empty($level['start_money'])){
			$arr_money = explode(',', $level['start_money']);//开通当前等级会获取当前币种多少奖励
		}
		for($i=0;$i<count($arr_id);$i++){//遍历出对应的币种 和开通后获得的奖励
			$arr[$arr_id[$i]]['id'] = $arr_id[$i];
			$arr[$arr_id[$i]]['money'] =$arr_money[$i];
		}
		//开通奖励
		for($i=0;$i<count($arr_id);$i++){
			$arr[$arr_id[$i]]['id'] = $arr_id[$i];
			$arr[$arr_id[$i]]['money'] =$arr_money[$i];
		}
		
		$all3 = M('App_account_type')->select();
		$count = count($all3);
		if(!empty($arr_id)){
			foreach($all3 as $k=>$v){
				if(in_array($all3[$k]['id'], $arr_id)){
					$all3[$k]['status'] = 1;
					$all3[$k]['money'] = $arr[$all3[$k]['id']]['money'];
		
				}else{
					$all3[$k]['status'] = 0;
				}
			}
		}
		$this->assign('all3',$all3);
		/*************开通奖励*****************************/
		/**************推荐奖*****************************/
		///增值区
		if(!empty($level['tuijian_cash'])){
			$arr_id1 = explode(',', $level['tuijian_cash']);
		}
		if(!empty($level['tuijian_cash_bili'])){
			$arr_money1 = explode(',', $level['tuijian_cash_bili']);
		}
		for($i=0;$i<count($arr_id1);$i++){
			$arr[$arr_id1[$i]]['id'] = $arr_id1[$i];
			$arr[$arr_id1[$i]]['money'] =$arr_money1[$i];
		}
		$all = M('App_account_type')->select();
		$count = count($all);
		if(!empty($arr_id1)){
			foreach($all as $k=>$v){
				if(in_array($all[$k]['id'], $arr_id1)){
					$all[$k]['status'] = 1;
					$all[$k]['money'] = $arr[$all[$k]['id']]['money'];
						
				}else{
					$all[$k]['status'] = 0;
				}
			}
		}
		$this->assign('all',$all);
		/*****************推荐奖********************/
		/*****************对碰奖*********************/
		if(!empty($level['duipeng_cash'])){
			$arr_id2 = explode(',', $level['duipeng_cash']);
		}
		if(!empty($level['duipeng_cash_bili'])){
			$arr_money2 = explode(',', $level['duipeng_cash_bili']);
		}
		for($i=0;$i<count($arr_id2);$i++){
			$arr[$arr_id2[$i]]['id'] = $arr_id2[$i];
			$arr[$arr_id2[$i]]['money'] =$arr_money2[$i];
		}
		$all1 = M('App_account_type')->select();
		if(!empty($arr_id2)){
			foreach($all1 as $k=>$v){
				if(in_array($all1[$k]['id'], $arr_id2)){
					$all1[$k]['status'] = 1;
					$all1[$k]['money'] = $arr[$all1[$k]['id']]['money'];
				}else{
					$all1[$k]['status'] = 0;
				}
			}
		}
		$this->assign('all1',$all1);
		/*************************************************/
		/*********************管理奖***********************/
		if(!empty($level['guanli_cash'])){
			$arr_id3 = explode(',', $level['guanli_cash']);
		}
		if(!empty($level['guanli_cash_bili'])){
			$arr_money3 = explode(',', $level['guanli_cash_bili']);
		}
		for($i=0;$i<count($arr_id3);$i++){
			$arr[$arr_id3[$i]]['id'] = $arr_id3[$i];
			$arr[$arr_id3[$i]]['money'] =$arr_money3[$i];
		}
		$all2 = M('App_account_type')->select();
		if(!empty($arr_id3)){
			foreach($all2 as $k=>$v){
				if(in_array($all2[$k]['id'], $arr_id3)){
					$all2[$k]['status'] = 1;
					$all2[$k]['money'] = $arr[$all2[$k]['id']]['money'];
				}else{
					$all2[$k]['status'] = 0;
				}
			}
		}
		$this->assign('all2',$all2);
		/*************************************************/
		
		
		
		
		
		$this->assign("list",$level);
		return $this->fetch();
	}
	 
	/**
	 * 参数修改方法
	 */
	public function l_save(){
		$id=I('id',"");
		if(empty($id)){
			$this->error("无效参数");
		}
		 
// 		$all = M('App_account_type')->select();
// 		foreach($all as $k=>$v){
// 			if(I('conversion_'.$all[$k]['id'])){
// 				$conversion[] = I('conversion_'.$all[$k]['id']);
// 				$money[] = I('money_'.$all[$k]['id']);
// 			}
// 		}
// 		if(!empty($conversion)){
// 			$_POST['account_type_id'] = implode(',',$conversion);
// 			$_POST['start_money'] = implode(',',$money);
// 		}else{
// 			$_POST['account_type_id'] = '';
// 			$_POST['start_money'] = '';
// 		}
// 		//分区1
// 		foreach($all as $k=>$v){
// 			if(I('tuijian_conversion_'.$all[$k]['id'])){
// 				$conversion1[] = I('tuijian_conversion_'.$all[$k]['id']);
// 				$money1[] = I('tuijian_money_'.$all[$k]['id']);
// 			}
// 		}
// 		if(!empty($conversion1)){
// 			$_POST['tuijian_cash'] = implode(',',$conversion1);
// 			$_POST['tuijian_cash_bili'] = implode(',',$money1);
// 		}else{
// 			$_POST['tuijian_cash'] = '';
// 			$_POST['tuijian_cash_bili'] = '';
// 		}
		
// 		//分区2
// 		foreach($all as $k=>$v){
// 			if(I('duipeng_conversion_'.$all[$k]['id'])){
// 				$conversion2[] = I('duipeng_conversion_'.$all[$k]['id']);
// 				$money2[] = I('duipeng_money_'.$all[$k]['id']);
// 			}
// 		}
// 		if(!empty($conversion2)){
// 			$_POST['duipeng_cash'] = implode(',',$conversion2);
// 			$_POST['duipeng_cash_bili'] = implode(',',$money2);
// 		}else{
// 			$_POST['duipeng_cash'] = '';
// 			$_POST['duipeng_cash_bili'] = '';
// 		}
		
		
		
// 		//分区3
// 		foreach($all as $k=>$v){
// 			if(I('guanli_conversion_'.$all[$k]['id'])){
// 				$conversion3[] = I('guanli_conversion_'.$all[$k]['id']);
// 				$money3[] = I('guanli_money_'.$all[$k]['id']);
// 			}
// 		}
// 		if(!empty($conversion3)){
// 			$_POST['guanli_cash'] = implode(',',$conversion3);
// 			$_POST['guanli_cash_bili'] = implode(',',$money3);
// 		}else{
// 			$_POST['guanli_cash'] = '';
// 			$_POST['guanli_cash_bili'] ='';
// 		}
		
		

		$re=M("App_level")->where(['id'=>$id])->update($_POST);
		if($re){
			$this->success("修改成功",U('Level/index'));
		}else{
			$this->error("修改失败");
		}
	}
	
	/**
	 * 修改等级状态
	 */
	public function openOrClose(){
		$id = input('id');
		$level = Db::name('App_level')->where(array('id'=>$id))->find();
		if($level['status'] == 0){
			$data['status'] = 1;
		}else{
			$data['status'] = 0;
		}
		$res = Db::name('App_level')->where(array('id'=>$id))->update($data);
		if($res){
			$this->success("修改成功",U('Level/index'));
		}else{
			$this->error("修改失败");
		}
	}
	
	
}