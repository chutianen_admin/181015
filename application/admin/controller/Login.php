<?php
/**
 * 	app后台登录
 *  =============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Session;
class Login extends Controller{
   	
	/**
	 * 登录页面
	 */
    public function index(){
    	return $this->fetch();
    }
    /**
     * 登录验证
     */
    public function checkLogin(){
    	$username=trim(input('username'));
    	$pwd=trim(input('pwd'));
    	if(empty($username)||empty($pwd)){
    		$data['info'] = '请填写完整信息';
    		$data['status'] = -2;
    		return json($data);
    	}
    	$admin = Db::name("app_admin")->where(array('username'=>$username))->find();
    	if($admin['password']!=md5($pwd)){
    		$data['info'] = '登录密码不正确';
    		$data['status'] = -1;
    		return json($data);
    	}else{
    		$save['login_time'] = $admin['time_ing'];
    		$save['time_ing'] = time();
    		db('App_admin')->where(['admin_id'=>$admin['admin_id']])->save($save);
    		session('admin_id',$admin['admin_id']);
    		$data['info'] = '登录成功';
    		$data['status'] = 1;
    		return json($data);
    	}
    }
    /**
     * 安全退出
     */
    public function loginout(){
    	session('admin_id', null);
    	$this->redirect('Login/index');
    }
}