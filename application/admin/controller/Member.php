<?php
/**
 * 	app后台会员控制器
 *  =============================
 * 	后台父类
 * 	=============================
 *  Date: 2017-12-06
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
use think\Collection;
use Think\page;
use think\Session;
require_once (APP_PATH . 'api/FinanceApi.php');
require_once (APP_PATH . 'api/RewardApi.php');
class Member extends AdminBase {
    public function _initialize(){
        parent::_initialize(); 
    }
    /**
     * 随机生成用户名
     * @param unknown $len
     * @param string $chars
     * @return string
     */
    public function getRandomString(){
    	$str = rand('10000000','99999999');
    	$member = M('App_member_relation')->where(array('code'=>$str))->find();
    	if($member){
    		$str = $this->getRandomString();
    	}
    	return $str;
    }
   
    
    public function aaa(){
    	$data['code'] = 0;
    	$where['member_id'] = array('neq',1);
    	$list = M('App_member_relation')->where($where)->update($data);	 
    }
    
    
    public function showw(){
        $phone = I('phone');
        $this->assign('phone',$phone);
        if($phone){
            $where['phone'] = array('eq',$phone);
        }
    	$list = Db::name('Aaaaa')->where($where)->paginate(15,false,['query'=>input()]);
    	
    	$page = $list->render();
    	$list = $list->all();
    	
    	foreach($list as $k=>$v){
    		$user = Db::name('Users')->where(array('loginName'=>$list[$k]['phone']))->find();
    		if(!$user){
    			$list[$k]['name'] = '不存在用户';
    			continue;
    		}
    		$relation = M('app_member_relation')->where('member_id='.$user['userId'])->find();
    		if($relation){
    			$list[$k]['name'] = $relation['name'];
    			$list[$k]['level'] = $relation['level'];
    			if($relation['zhuceid']){
    				$tuijian = M('app_member_relation')->where('member_id='.$relation['zhuceid'])->find();
    				$list[$k]['tuiuser'] = $tuijian['name'];
    			}
    			$list[$k]['idcard'] = $relation['idcard'];
    		}
    	}
    	$this->assign('list',$list);// 赋值数据集
    	$this->assign ( 'page', $page );
    	return $this->fetch();
    }
    
    
    
    /**
     * 会员列表
     */
    public function index(){
    	$status = input('status');
    	
    	
    	$email = input('email');
    	$member_id= input('user_id');
    	$name = input('name');
    	$tuijian = input('tuijian');
    	$level = input('level');
        $this->assign('email',$email);
        $this->assign('user_id',$member_id);
        $this->assign('name',$name);
        $this->assign('tuijian',$tuijian);
    	if(!empty($email)){
    		$where['loginName'] = array('like','%'.$email.'%');
    	}
    	if (!empty($member_id)){
    		$where['userId']=$member_id;
    	}
    	if (!empty($level)){
    		$where1['level']=$level;
    		$list = M('app_member_relation')->where($where1)->select();
    		foreach($list as $k=>$v){
    			$res[] = $list[$k]['member_id'];
    		}
    		$where['userId']=array('in',$res);
    	}
    	//获取姓名
    	if (!empty($name)){
    		$where1['name']=$name;
    		$list = M('app_member_relation')->where($where1)->select();
    		foreach($list as $k=>$v){
    			$res[] = $list[$k]['member_id'];
    		}
    		$where['userId']=array('in',$res);
    	}
    	
    	
    	if (!empty($tuijian)){
    		$where2['loginName']=$tuijian;
    		$list = Db::name('Users')->where($where2)->find();
    		$where3['zhuceid']=$list['userId'];
    		$res = M('app_member_relation')->where($where3)->select();
    		foreach($res as $k=>$v){
    			$re[] = $res[$k]['member_id'];
    		}
    		$where['userId']=array('in',$re);
    	}
    	if(!empty($status)){
    		if($status == '2'){
    			$where['status'] = 0;
    		}else{
    			$where['status'] = 1;
    		}
    		
    	}
    	
        $list = new Collection();
    	$list =  Db::table('wst_users')->alias('a')
									 ->join('wst_app_member_relation w','a.userId = w.member_id')
			    					 ->where($where)
		    						 ->order('userId desc')
		    						 ->paginate(15,false,['query'=>input()]);
        $page = $list->render();
        $list = $list->all();
        foreach($list as $k=>$v){
        		$my_level = Db::name('App_level')->where(array('id'=>$list[$k]['level']))->find();
        		$list[$k]['level_name'] = $my_level['name'];
        		$tuijian = Db::name('Users')->where('userId='.$list[$k]['zhuceid'])->find();
        		$list[$k]['tuiuser'] = $tuijian['loginName'];
        		$jiedian = Db::name('Users')->where('userId='.$list[$k]['jiedianid'])->find();
        		$list[$k]['jiedianuser'] = $jiedian['loginName'];
        		$baodan_name = db('users')->where(['userId'=>$list[$k]['baodan']])->find();
        		$list[$k]['baodan'] = $baodan_name['loginName'];
        		
        		$num = db('line')->where(['member_id'=>$v['userId']])->count();
        		if($num > 0){
        			$list[$k]['num'] = $num-1;
        		}else{
        			$list[$k]['num'] = $num;
        		}
        		
        }
//         foreach($list as $k=>$v){
//             $relation = M('app_member_relation')->where('member_id='.$v['userId'])->find();
//             if($relation){
//                 $list[$k]['name'] = $relation['name'];
//                 $list[$k]['out'] = $relation['out'];
//                 $list[$k]['nick'] = $relation['nick'];
//                 $my_level = Db::name('App_level')->where(array('id'=>$relation['level']))->find();
//                 $list[$k]['level_name'] = $my_level['name'];
//                 $tuijian = Db::name('Users')->where('userId='.$relation['zhuceid'])->find();
//                 $list[$k]['tuiuser'] = $tuijian['loginName'];
//                 $jiedian = Db::name('Users')->where('userId='.$relation['jiedianid'])->find();
//                 $list[$k]['jiedianuser'] = $jiedian['loginName'];
//                 $list[$k]['idcard'] = $relation['idcard'];
//                 $list[$k]['quyu'] = $relation['quyu'];
//                 $list[$k]['status'] = $relation['status'];
//                 $baodan_name = db('users')->where(['userId'=>$relation['baodan']])->find();
//                 $list[$k]['baodan'] = $baodan_name['loginName'];
//                 $list[$k]['is_baodan'] = $relation['is_baodan'];
//                 $num = db('line')->where(['member_id'=>$v['userId']])->find()['num'];
//                 $list[$k]['num'] = $num;
//             }
               
//         }
        //会员等级
        $all_level = Db::name('App_level')->where(array('status'=>1))->select();
        $this->assign('all_level',$all_level);
    	$this->assign('list',$list);// 赋值数据集
    	$this->assign ( 'page', $page );
    	return $this->fetch();
    }
    /**
     * 审核通过
     * 触发奖励
     */
    public function shenheTongGuo(){
    	$member_id = input('member_id');
    	$reward = new \RewardApi();
    	$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->find();
    	$class = $reward->getUserMoneyByLevel($member_relation['level']);
    	$res[] = $this->addChildren($member_relation['zhuceid'],$member_id);
    	//为推荐人添加推荐人数
    	$res[] = Db::name('App_member_relation')->where(array('member_id'=>$member_relation['zhuceid']))->setInc('zhitui_num',1);
    	//修改状态
    	$data['status'] = 1;
    	$res[] = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->update($data);
    	//调用奖励
    	
    	$res[] = $reward->begin($member_id, $class['money']);
    	if($res){
    		$this->success('成功');
    	}else{
    		$this->error('失败');
    	}
    }
    /**
     * 注册之后加左数和右数
     * @param unknown $invite_id
     * @param unknown $id
     * @param unknown $info
     * @return unknown
     */
    public function addChildren($zhuce_id,$member_id){
    	$res = Db::name('App_member_relation')->where(array('member_id' => $zhuce_id))->field('lft')->find()['lft'];
    	$lft['lft'] = array('gt', $res);
    	$rgt['rgt'] = array('gt', $res);
    	Db::name('App_member_relation')->where($lft)->setInc('lft',2);
    	Db::name('App_member_relation')->where($rgt)->setInc('rgt',2);
    	$info['zhuceid'] = $zhuce_id;
    	$info['lft'] = $res + 1;
    	$info['rgt'] = $res + 2;
    	$r = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->update($info);
    	return $r;
    }
    /**
     * 修改会员状态
     */
    public function updateStatus(){
    	$member_id = input('member_id');
    	$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->find();
    	if($member_relation['status'] == 2){
    		$data['status'] = 0;
    	}else{
    		$data['status'] = 2;
    	}
    	$res = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->update($data);
    	if($res){
    		$this->success('修改成功');
    	}else{
    		$this->error('参数错误');
    	}
    }
    
    
    
    
    /**
     * 一键登录
     */
    public function oneLogin(){
    	$member_id = input('member_id');
    	$r = Db::name('Users')->where(array('userId'=>$member_id))->find();
    	if($r){
    		session::set('USER_KEY_ID',$r['userId']);
    		session::set('USER_KEY',$r['loginName']);//用户名
    		$this->redirect('Home/Index/index');
    	}else{
    		$this->error('参数错误');
    	}
    	 
    }
    
	/**
	*获取会员账号
	*/
	public function getzhucename($id){
		if(empty($id)){
			return '没有推荐人';
		}
		$list = M('Member')->where("member_id={$id}")->find();
		return $list['account']."（{$list['member_id']}）";
	}
    /**
     * 添加会员
     */
    public function addMember(){
        if(IS_POST){
            // 接取手机号
            $mobile = input('phone');
            $loginName = input('loginName');
            if(!$mobile){
                $this->error('手机号不能空');

            }else{

               $member = M('users')->where(array('loginName'=>$loginName))->find();
                if($member){
                    $this->error('用户名重复');
                    return;
                }
                $data['loginName'] = $loginName;
                $data['userPhone'] = $mobile;
            }

            //接取登录密码 交易密码 判断是否一直
            if(!$_POST['pwd']){
                $this->error('密码不能空');
                return;
            }
            if(!$_POST['pwdtrade']){
                $this->error('支付密码不能空');
                return;
            }
            if($_POST['pwd']==$_POST['pwdtrade']){
                $this->error('支付密码不能和密码一样');
                return;
            }
            $data['loginPwd']=md5(I('pwd'));
            $data['payPwd']=md5(I('pwdtrade'));
            //添加时间
            $data['createTime'] = time();
            //添加user表
            $re = M('users')->insert($data);
            //接取推荐人姓名
            $tui = input('zhitui');
            if($tui){
                $tuiuser = M('Users')->where(array('loginName'=>$tui))->find();
                if(!$tuiuser){
                	$this->error('没有该推荐人');
                }else{
                	$data1['zhuceid']=$tuiuser['userId'];
                	$data1['code']=$this->getTuiJianCode();//获取推荐码
                }
            }
            //接取真实姓名
            $data1['name'] = input('name');
            //接取身份证号
            $idcard = input('idcard');
            if(!empty($idcard)){
            	$is_idcard = $this->getAttestation($idcard);
            	if(!$is_idcard){
            		$this->error('身份证号已存在');
            	}
            	$data1['idcard']=$idcard;
            }
            $users = Db::name('Users')->where(array('loginName'=>$loginName))->find();
            $data1['member_id'] = $users['userId'];
            $data1['level'] = 1;
            $data1['phone'] = $mobile;
            //后台注册用户为激活状态
            $data1['status'] = 0;
            //添加relation表
            $res1 = M('app_member_relation')->insert($data1);
            //添加app_member_account表
            $arr['member_id'] = $users['userId'];
            $res = M('app_member_account')->insert($arr);
            if($re){
                $this->success('添加成功',U('Member/index')); 
                 return;
            }else{
                $this->error('服务器繁忙,请稍后重试');
                return;
           }
        }else{
            return $this->fetch();
        }
    }
    /**
     * 添加个人信息
     */
    public function saveModify(){
        $member_id = I('get.member_id','','intval');
        $M_member = D('Member');
        if(IS_POST){
            $_POST['status'] = 1;//0=有效但未填写个人信息1=有效并且填写完个人信息2=禁用
            if (!$data=$M_member->create()){ // 创建数据对象
                // 如果创建失败 表示验证没有通过 输出错误提示信息
                $this->error($M_member->getError());
                return;
            }else {
                $where['member_id'] = $_POST['member_id'];
                $r = $M_member->where($where)->save();
                if($r){
                    $this->success('添加成功',U('Member/index'));
                    return;
                }else{
                    $this->error('服务器繁忙,请稍后重试');
                    return;
                }
            }
        }else{
            $where['member_id'] = $member_id;
            $list = $M_member->where($where)->find();
            $this->assign('list',$list);
            return $this->fetch();
        }
    }
	/**
	 * 公排一条线管理
	 */
	public function line(){
// 		echo '123';
		$line = Db::name('Line')->where(array('status'=>0))->order('id asc')->select();
// 		dump($line);
		foreach($line as $k=>$v){
			$user = Db::name('Users')->where(array('userId'=>$line[$k]['member_id']))->find();
			$line[$k]['loginName'] = $user['loginName'];
		}
// 		dump($line);
		$this->assign('line',$line);
		return $this->fetch();
	}




    /**
     * 显示自己推荐列表
     */
    public function show_my_invit(){
        $member_id = $_GET['member_id'];
        if(empty($member_id)){
            $this->error('参数错误');
            return;
        }
        $M_member = M('Member');
        $count      = $M_member->where(array('pid'=>$member_id))->count();// 查询满足要求的总记录数
        $Page       = new Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $my_invit = $M_member
            ->where(array('pid'=>$member_id))
            ->order(" reg_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        if($my_invit){
            $this->assign('my_invit',$my_invit);
            $this->assign('page',$show);// 赋值分页输出
            return $this->fetch();// 输出模板
        }else{
            $this->error('抱歉,您还没有推荐其他人');
            return;
        }
    }
    /**
     * 修改会员
     */
    public function saveMember(){
        $M_member = M('Users');
        if(IS_POST){
//         	$aaa = input();
//         	dump($aaa);
//         	die();
            $member_id = I('member_id');
            $zhitui = I('zhitui');
            
            $data['loginName'] = I('loginName');
            $data['userStatus'] = I('status');
           
			if(I('pwd')){
				$data['loginPwd']=md5(I('pwd'));
			}
            if(I('payPwd')){
            	$data['payPwd']=md5(I('payPwd'));
            }
            if($zhitui){
            	$zhitui_member = M('Users')->where(array('loginName'=>$zhitui))->find();
            	if($zhitui_member){
            		$data1['zhuceid']=$zhitui_member['userId'];
            		$data1['code']=$this->getTuiJianCode();//获取推荐码
            	}else{
            		$this->error('该推荐人不存在!');
            	}
            }
            $data1['name'] = I('name');
       		$idcard = I('idcard','');
            if($idcard){
            	
            	$data1['idcard']=$idcard;
            }else{
            	$data1['idcard']='';
            }
            $data1['idcard'] =  $idcard;
            $data1['status'] = input('status');
            $data1['is_baodan'] = input('is_baodan');
            $data1['is_release_gd'] = input('is_release_gd');
            $data1['is_release_tj'] = input('is_release_tj');
            $data1['is_release_jl'] = input('is_release_jl');
            $data1['level'] = I('level');
            $data1['bank_card'] = I('bank_card');
            $data1['weixin'] = I('weixin');
            $data1['address'] = I('address');
            $data1 ['qu'] = input('area');
            $data1 ['sheng'] = input('pro');
            $data1 ['shi'] = input('city');
            $data1['phone'] = I('phone');
            if($data1['status'] == 2){
            	$token_data['token'] = '';
            	$token_res = Db::name('Token')->where(array('uid'=>$member_id))->update($token_data);
            }
//             $arr['account_type_1'] = floatval(I('account_type_1'));
//             $arr['account_type_2'] = floatval(I('account_type_2'));
//             $arr['account_type_3'] = floatval(I('account_type_3'));
//             $arr['account_type_4'] = floatval(I('account_type_4'));
            
           	//查询没有修改之前的用户资金信息
           	$account = 	db('app_member_account')->where(['member_id'=>$member_id])->find();
//            	$res = M('app_member_account')->where(['member_id'=>$member_id])->update($arr);
           	$finance = new \FinanceApi();
//            	$account_type = Db::name('App_account_type')->select();
//            	foreach($account_type as $m=>$n){
//            		$operation =  input('account_type_'.$account_type[$m]['id']) - $account['account_type_'.$account_type[$m]['id']];
// 				if($operation != 0){
// 					if($operation < 0){
// 						$pre = '减少';
// 						$num = -$operation;
// 						$type = 2;
// 					}else{
// 						$pre = '增加';
// 						$num = $operation;
// 						$type = 1;
// 					}
// 					$content = $account_type[$m]['name'].$pre.$num; 
// 					$res = $finance->addFinance($member_id,4,$content,$type,$operation,$account_type[$m]['id']);
// 				}
//            	}
            $re = M('users')->where(['userId'=>$member_id])->update($data);
            $res1 = M('app_member_relation')->where(['member_id'=>$member_id])->update($data1);
            
            if($re || $res1){
            	$this->success('修改成功',U('Member/index'));
            	return;
            }else{
            	$this->error('服务器繁忙,请稍后重试');
            	return;
            }

        }else{
        	$member_id = (int)I('member_id');
            if($member_id){
                $where['userId'] = $member_id;
                //$where['type'] = array('neq',2);
                $list = Db::table('wst_users')->alias('a')
							                  ->join('app_member_relation w','a.userId = w.member_id')
							                  ->where($where)
							                  ->find();
                	
                $relation = M('app_member_relation')->where(['member_id'=>$list['userId']])->find();
               	$tuijian = M('Users')->where(['userId'=>$relation['zhuceid']])->find();
               	$list['tuijian_name'] = $tuijian['loginName'];
               	$list['user_id'] = $list['userId'];
                $list['name'] = $relation['name'];
               	$list['idcard'] = $relation['idcard'];
               	$account_type = M('app_account_type')->select();
               	for($i=1;$i<count($account_type)+1;$i++){
               		$aa[] = 'account_type_'.$i;
               	}
               	$bb = implode(',',$aa);
               	$list['member_account'] = M('app_member_account')->field($bb)->where(array('member_id'=>$list['user_id']))->find();
               	
               	$level = Db::name('App_level')->select();
               	$my_level = Db::name('App_level')->where(array('id'=>$list['level']))->find();
               	$list['level_name'] = $my_level['name'];
               	
               	
               	$sheng  = Db::name('Areas')->where(array('areaId'=>$list['sheng']))->find();
               	$shi  = Db::name('Areas')->where(array('areaId'=>$list['shi']))->find();
               	$qu = Db::name('Areas')->where(array('areaId'=>$list['qu']))->find();
              
               	$this->assign('sheng',$sheng);
               	$this->assign('shi',$shi);
               	$this->assign('qu',$qu);
               	
               	//三级联动
               	$parent_id['parentId'] = 0;
               	$region = Db::name('Areas')->where($parent_id)->select();
               	$this->assign('region',$region);
               	
               	$this->assign('level',$level);
               	$this->assign('list',$list);
               	
                $this->assign('res',$res);
                return $this->fetch();
            }else{
                $this->error('参数错误');
                return;
            }
        }
    }
    
    public function area(){
    	$parent_id['parentId'] = input('pro_id');
    	$region = M('Areas')->where($parent_id)->select();
    	$opt = '<option value="">--请选择市区--</option>';
    	foreach($region as $key=>$val){
    		$opt .= "<option value='{$val['areaId']}'>{$val['areaName']}</option>";
    	}
    	//dump($opt);die();
    	echo $opt;
    
    }
    
    
    /**
     * 修改收货地址
     */
    public function update(){
    	//三级联动
    	$parent_id['parent_id'] = 1;
    	$region = M('Areas')->where($parent_id)->select();
    	$this->assign('region',$region);
    	//dump($region);die();
    	//三级联动结束
    	$address_id = I('address');
    	$list = M('Address')->where(array('id'=>$address_id))->find();
    	$where['area_id'] = $list['province_id'];
    	$where1['area_id'] = $list['city_id'];
    	$where2['area_id'] = $list['area_id'];
    	$province = M('Areas')->where($where)->select();
    	$city = M('Areas')->where($where1)->select();
    	$area = M('Areas')->where($where2)->select();
    	$this->assign('detailed',$list['detailed']);
    	$this->assign('sheng',$province[0]['area_name']);
    	$this->assign('shi',$city[0]['area_name']);
    	$this->assign('qu',$area[0]['area_name']);
    	$this->assign('province_id',$list['province_id']);
    	$this->assign('city_id',$list['city_id']);
    	$this->assign('area_id',$list['area_id']);
    	$this->assign('member_id',$list['member_id']);
    	$this->assign('phone',$list['phone']);
    	$this->assign('code',$list['code']);
    	$this->assign('linkman',$list['linkman']);
    	$this->assign('id',$list['id']);
    	return $this->fetch();
    }
    
    public function address(){
    	//三级联动
    	$parent_id['parent_id'] = I('post.pro_id','addslashes');
    	$region = M('Areas')->where($parent_id)->select();
    	$opt = '<option>--请选择市区--</option>';
    	foreach($region as $key=>$val){
    		$opt .= "<option value='{$val['area_id']}'>{$val['area_name']}</option>";
    	}
    	//dump($opt);die();
    	echo $opt;
    	//三级联动结束
    }
    public function doupdate(){
    	if (empty ( $_POST ['linkman'] )) {
    		$this->error('请输入收件人');
    	}
    	if (empty ( $_POST ['pro'] )) {
    		$this->error('请选择省份');
    	}
    	if (empty ( $_POST ['city'] )) {
    		$this->error('请选择城市');
    	}
    	if (empty ( $_POST ['area'] )) {
    		$this->error('请选择区域');
    	}
    	if (empty ( $_POST ['phone'] )) {
    		$this->error('请输入手机号');
    	}
    	if (empty ( $_POST ['code'] )) {
    		$this->error('请输入邮编');
    	}
    	if (empty ( $_POST ['address'] )) {
    		$this->error('请输入详细收货地址');
    	}
    	$sheng_id = I('pro');
    	$sh = M('Areas')->where("area_id='{$sheng_id}'")->find();
    	$sheng = $sh['area_name'];
    	//城市名
    	$s = M('Areas')->find(I('city'));
    	$shi = $s['area_name'];

    	$q = M('Areas')->find(I('area'));
    	$qu = $q['area_name'];

    	$res ['id'] =  I('id');
    	$res ['member_id'] = I('member_id');
    	$res ['address'] = $sheng.$shi.$qu.I('address');
    	$res ['phone'] = I('phone');
    	$res ['code'] = I('code');
    	$res ['linkman'] = I('linkman');
    	$res ['province_id'] = I('pro');
    	$res ['city_id'] = I('city');
    	$res ['area_id'] = I('area');
    	$res ['detailed'] = I('address');
    	$list = M('Address')->save($res);
    	if($list){
    	 	$this->success('修改成功',U('Member/index'));
    	}else{
    		$this->error('修改失败');
    	}

    }
    /**
     * 删除收货地址
     */
	public function Del(){
			$id=I('id');
			$address = M('Address')->where(array('id'=>$id))->find();
			if($address['status']!=0){
				$this->error('不要删除默认的收货地址');
			}
			$list ['id'] = $id;
			$list ['type'] = "2";
			$res = M("Address")->save($list);
			if($res){
				$this->success('删除成功');
			}else{
				$this->error('删除失败');
			}
		}
    /**
     * 删除会员
     */
    public function delMember(){
        $member_id = input('member_id');
        //判断还有没有余额
        $where1['userId']= $member_id;
        $where['member_id']= $member_id;
        $member = Db::name('Users')->where($where1)->find();
        $r[] = Db::name('Users')->where($where1)->delete();
        $r[] = Db::name('App_member_account')->where($where)->delete();
        $r[] = Db::name('App_finance')->where($where)->delete();
        $r[] = Db::name('App_member_relation')->where($where)->delete();
        if($r){
        	//改上线直推num 减1
        	//$res = M('Member')->where(array('member_id'=>$member['zhuceid']))->setDec('num',1);
            $this->success('删除成功',U('Member/index'));
            return;
        }else{
            $this->error('删除失败');
            return;
        }
    }
    /**
     * ajax判断邮箱
     * @param $email
     */
    public function ajaxCheckEmail($email){
        $email = urldecode($email);
        $data = array();
        if(!checkEmail($email)){
            $data['status'] = 0;
            $data['msg'] = "邮箱格式错误";
        }else{
            $M_member = M('Member');
            $where['email']  = $email;
            $r = $M_member->where($where)->find();
            if($r){
                $data['status'] = 0;
                $data['msg'] = "邮箱已存在";
            }else{
                $data['status'] = 1;
                $data['msg'] = "";
            }
        }
        $this->ajaxReturn($data);
    }

    /**
     * ajax验证昵称是否存在
     */
    public function ajaxCheckNick($nick){
        $nick = urldecode($nick);
        $data =array();
        $M_member = M('Member');
        $where['nick']  = $nick;
        $r = $M_member->where($where)->find();
        if($r){
            $data['msg'] = "昵称已被占用";
            $data['status'] = 0;
        }else{
            $data['msg'] = "";
            $data['status'] = 1;
        }
        $this->ajaxReturn($data);
    }
    /**
     * ajax手机验证
     */
    function ajaxCheckPhone($phone) {
        $phone = urldecode($phone);
        $data = array();
        if(!checkMobile($phone)){
            $data['msg'] = "手机号不正确！";
            $data['status'] = 0;
        }else{
            $M_member = M('Member');
            $where['phone']  = $phone;
            $r = $M_member->where($where)->find();
            if($r){
                $data['msg'] = "此手机已经绑定过！请更换手机号";
                $data['status'] = 0;
            }else{
                $data['msg'] = "";
                $data['status'] = 1;
            }
        }
        $this->ajaxReturn($data);
    }

    /**
     * 查看个人币种
     */
    public function show(){
    	$currency = M('Currency_user');
    	$member = M('Member');
    	$member_id = I('member_id');
    	if(empty($member_id)){
    		$this->error('参数错误',U('Member/index'));
    	}
    	$where['member_id'] = $member_id;
    	$count = $currency->join(C("DB_PREFIX")."currency ON ".C("DB_PREFIX")."currency_user.currency_id = ".C("DB_PREFIX")."currency.currency_id")
    			->where($where)->count();
    	$Page = new \Think\Page ( $count,20); // 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $Page->show();//分页显示输出性
    	$info = $currency->join(C("DB_PREFIX")."currency ON ".C("DB_PREFIX")."currency_user.currency_id = ".C("DB_PREFIX")."currency.currency_id")
    			->where($where)->limit($Page->firstRow.','.$Page->listRows)
    			->select();
    	$member_info = $member->field('member_id,name,phone,email')->where($where)->find();
    	$this->assign('member_info',$member_info);
    	$this->assign('info',$info);
    	$this->assign('page',$show);
    	return $this->fetch();
    }
    //修改个人币种数量
    public function updateMemberMoney(){
    	$member_id=I('post.member_id');
    	$currency_id=I('post.currency_id'); 
    	$num=I('post.num');
    	$forzen_num=I('post.forzen_num');
    	if(empty($member_id)||empty($member_id)){
    		$data['info']="参数不全";
    		$data['status']=0;
    	}
    	$where['member_id']=$member_id;
    	$where['currency_id']=$currency_id;
    	$r[]=M('Currency_user')->where($where)->setField('num',$num);
    	$r[]=M('Currency_user')->where($where)->setField('forzen_num',$forzen_num);
    	if($r){
    		$data['info']="修改成功";
    		$data['status']=1;
    	}else{
    		$data['info']="修改失败";
    		$data['status']=0;
    	}
    	$this->ajaxReturn($data);
    }
    /**
     * 账户信息修改页面
     */
    public function zhanghu(){
    	$email = input('email'); 
    	$member_id = input('member_id');
    	$tuijian = input('tuijian');
    	$name = input('name');
    	$class = input('class');
    	$num = input('num');
    	if($class && $num){
    		$map[$class] = array('gt',$num);
    		$list = Db::name('App_member_account')->where($map)->select();
    		foreach($list as $m=>$n){
    			$member_id[] = $list[$m]['member_id'];
    		}
    		$where['userId'] = array('in',$member_id);
    	}else{
    		//用户名
    		if($email){
    			$where['loginName'] = $email;
    		}
    		//用户id
    		if($member_id){
    			$where['userId'] = $member_id;
    		}
    		//推荐人电话号
    		if($tuijian){
    			$tuijian_member = Db::name('Users')->where(array('loginName'=>$tuijian))->find();
    			$list = Db::name('App_member_relation')->where(array('zhuceid'=>$tuijian_member['userId']))->select();
    			foreach($list as $k=>$v){
    				$array[] = $list[$k]['member_id'];
    			}
    			$where['userId'] = array('in',$array);
    		}
    		//真实姓名
    		if($name){
    			$member_relation = Db::name('App_member_relation')->where(array('name'=>$name))->select();
    			foreach($member_relation as $k=>$v){
    				$arrayy[] = $member_relation[$k]['member_id'];
    			}
    			$where['userId'] = array('in',$arrayy);
    		}
    	}
    	//查看积分
    	$account_type = Db::name('App_account_type')->select();
    	for($i=1;$i<count($account_type)+1;$i++){
    		$aa[] = 'account_type_'.$i;
    		$arr[] = Db::name('App_member_account')->sum('account_type_'.$i);
    	}
    	
    	$bb = implode(',',$aa);
    	$member = Db::name('Users')->where($where)->paginate(15,false,['query'=>input()]);
        $page = $member->render();
        $member = $member->all();
        foreach($member as $k=>$v){
        	$relation = Db::name('App_member_relation')->where('member_id='.$v['userId'])->find();
        	if($relation){
        		$member[$k]['name'] = $relation['name'];
        		$member[$k]['nick'] = $relation['nick'];
        		$tuijian = Db::name('app_member_relation')->where('member_id='.$relation['zhuceid'])->find();
        		$member[$k]['tuiuser'] = $tuijian['name'];
        		$member[$k]['member_account'] = Db::name('App_member_account')->field($bb)->where(array('member_id'=>$v['userId']))->find();
        	}
        }
		$this->assign('all',$arr);
        $this->assign ('page', $page );
        $this->assign('member',$member);
    	$this->assign('account_type',$account_type);
    	return $this->fetch();
    }
    /**
     * 提交功能
     */
    public function saveZhangHu(){
    	$name = input('name')?input('name'):null;//用户名
    	$type = input('type')?input('type'):null;//币种类型
    	$money = input('money')?input('money'):null;//充值金额
    	if(!is_numeric($money)){
    		$this->error('请填写正确的内容');
    	}
    	$list = M('Users')->where(array('loginName'=>$name))->find();
    	if(empty($type)){
    		$this->error('请选择要添加的货币类型');
    	}
    	if(empty($list)){
    		$this->error('没找到该用户');
    	}
    	$account_type = M('app_account_type')->where(array('id'=>$type))->find();//查看要往哪个币种里面充值
    	$member_account = M('app_member_account')->where(array('member_id'=>$list['userId']))->find();//这里是查询账户所剩余额
    	$currcy = 'account_type_'.$type;//查看用户账户下该币种字段
    	$currcy_name = $account_type['name'];//查看用户账户下该币种name
    	$jifen = $member_account['account_type_'.$type];//查看该用户该币种的余额
//     	$aa = $jifen+$money;
//     	//dump($aa);die();
//     	if($aa<0){
//     		$this->error('余额不足！无法继续扣款');
//     	}
    	if($money>0){
    		$money_type = 1;
    	}else{
    		$money_type = 2;
    	}
    	$res = M('app_member_account')->where(array('member_id'=>$list['userId']))->setInc('account_type_'.$type,$money);
    	//添加余额
    	$bala = $this->balance($list['userId'],$money_type);
    	$finance = new \FinanceApi();
    	$re = $finance->addFinance($list['userId'],4,'管理员操作'.$currcy_name, $money_type, $money, $type);
    	if($res){
    		$this->success("添加成功");
    	}else{
    		$this->error('添加失败');
    	}
    }
  
    /**
     * 推荐关系
     */
    public function tuijian(){
    	if(empty($_POST['username'] )) {
    		$user = M('Users')->where(array('loginName'=>'admin'))->find();
    	}else{
    		$user = M ( 'Users' )->where(array('loginName'=>$_POST['username']))->find();
    	}
		$member = M('App_member_relation')->where(array('member_id'=>$user['userId']))->find();
		$user['yeji'] = $member['yeji'];
    	$user['add_time']=date('Y-m-d H:i:s',$user['add_time']);
    	$count = DB::name('App_member_relation')->where(array('zhuceid'=>$user['userId']))->count();
    	$user['num']=$count;
    	$this->assign ('user',$user);
    	return $this->fetch();
    }
    public function getInivt() {
    	$username = $_POST ['username'];
    	$list = M('Users')->where(array('loginName'=>$username))->find();
    	$r = Db::name('App_member_relation')->where ( array('zhuceid'=>$list['userId']))->select ();
    	foreach ( $r as $k => $v ) {
    		$member = Db::name('Users')->where(array('userId'=>$r[$k]['member_id']))->find();
    	
    		$r[$k]['add_time'] = date('Y-m-d H:i:s',$r[$k]['add_time']);
    		$r[$k]['loginName'] = $member['loginName'];
    		$r[$k]['num'] = DB::name('App_member_relation')->where(array('zhuceid'=>$r[$k]['member_id']))->count();
    	}

    	if(!empty($r)){
    		$data['status']=1;
    		$data['user']=$r;
    		return json($data);
    	}else{
    		$data['status']= 0;
    		return json($data);
    	}
    }

	public  function upgrade(){
		$email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
            $where['name'] = array('like','%'.$email.'%');
        }
        if (!empty($member_id)){
            $where['member_id']=$member_id;
        }
        $count      =  M('Upgrade')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出

        $list =  M('Upgrade')
            ->where($where)
            ->order(" add_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();

		foreach($list as $k=>$v){
			$nameed = memberLevel($v['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			$list[$k]['level'] = $name['name'];
		}
		$this->assign('list',$list);
		return $this->fetch();
	}

	 /**
     * 会员升级记录导出excel文件
     */
    public function upgradeExcel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list= M('Upgrade')
    	->field("id,member_id,name,leveled,level,add_time")
    	->order('add_time desc')
    	->where($where)
    	->select();
    	foreach ($list as $k=>$v){
    		$list[$k]['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
			$nameed = memberLevel($v['leveled']);
			unset($list[$k]['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			unset($list[$k]['level']);
			$list[$k]['level'] = $name['name'];

    	}
    	$title = array(
    			'ID',
    			'会员ID',
    			'会员姓名',
    			'升级时间',
    			'升级前级别',
    			'升级后级别',
    	);
    	$filename= $this->config['name']."会员升级记录-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
	/**
	*会员虚升记录
	*/
	public function upgrade_free(){
		$email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
            $where['name'] = array('like','%'.$email.'%');
        }
        if (!empty($member_id)){
            $where['member_id']=$member_id;
        }
        $count      =  M('Upgrade_free')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出

        $list =  M('Upgrade_free')
            ->where($where)
            ->order(" add_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();

		foreach($list as $k=>$v){
			$nameed = memberLevel($v['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			$list[$k]['level'] = $name['name'];
		}
		$this->assign('list',$list);
		return $this->fetch();
	}

	 /**
     * 会员升级(虚升)记录导出excel文件
     */
    public function upgrade_freeExcel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list= M('Upgrade_free')
    	->field("id,member_id,name,leveled,level,add_time")
    	->order('add_time desc')
    	->where($where)
    	->select();
    	foreach ($list as $k=>$v){
    		$list[$k]['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
			$nameed = memberLevel($v['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			$list[$k]['level'] = $name['name'];
    	}
    	$title = array(
    			'ID',
    			'会员ID',
    			'会员姓名',
    			'升级时间',
    			'升级前级别',
    			'升级后级别',
    	);
    	$filename= $this->config['name']."会员虚升记录-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
  	/**
  	 * 12.3褚天恩
  	 * 艺尊指数提现
  	 */
    public function yizun(){

    	$count =  M('Yizun_tixian')->where()->count();// 查询满足要求的总记录数
    	$Page  = new \Think\Page($count,10);
    	$show  = $Page->show();
    	$list =  M('Yizun_tixian')->where()->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	foreach($list as $k=>$v){
    		$member_id = $list[$k]['member_id'];
    		$member = M('Member')->where(array('member_id'=>$member_id))->find();
    		$list[$k]['name'] = $member['account'];
    		$list[$k]['full_name'] = $member['name'];
    		$list[$k]['phone'] =$member['phone'];
    		$list[$k]['level'] = $member['level'];
    		$list[$k]['yizun'] = $member['yizun'];
    	}
    	$this->assign('list',$list);// 赋值数据集
    	$this->assign('page',$show);// 赋值分页输出

    	return $this->fetch();
    }
    /**
     * 提现处理方法
     */
    public function tongGuo(){
    	$id = I('id');
    	$data['status'] = 1;
    	$res = M('Yizun_tixian')->where(array('id'=>$id))->save($data);
    	if($res){
    		$this->success("已通过");
    	}else{
    		$this->error('系统繁忙');
    	}

    }

     /**
     * 系统充值记录导出excel文件
     */
    public function zhanghu_excel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
		$where['type'] = 25;
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list = M('Finance')->field('member_id,type,content,money,currency_id,add_time')->where($where)->order('add_time desc')->select();
    	foreach($list as $k=>$v){
    		$user = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->find();
    		$list[$k]['member_id'] = $user['account'];
			if($v['currency_id']==1){
				$list[$k]['currency_id']='激活积分';
			}
			if($v['currency_id']==2){
				$list[$k]['currency_id'] = '交易积分';
			}
			if($v['currency_id']==3){
				$list[$k]['currency_id'] = '艺宝积分';
			}
			if($v['currency_id']==2){
				$list[$k]['currency_id'] = '艺尊指数';
			}

    		$list[$k]['add_time']=date('Y-m-d H:i:s',$list[$k]['add_time']);
    	}
    	$title = array(
    			'会员账号',
    			'财务类型',
    			'操作内容',
    			'充值金额',
    			'充值币种',
    			'充值时间',
    	);
    	$filename= $this->config['name']."系统充值记录-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
    /**
     * 通过审核
     */
    public function tong(){
		//通过审核为推荐人num字段加1
		$id = I('member_id');
		$data['status'] = 1;
    	$res = M('Member')->where(array('member_id'=>$id))->save($data);
    	if($res){
    		$member = M('Member')->where(array('member_id'=>$id))->find();
    		$zhuceid = $member['zhuceid'];
//     		$res = M('Member')->where(array('member_id'=>$zhuceid))->setInc('num',1);
    		$this->success('成功');
    	}else{
    		$this->error('系统繁忙');
    	}

    }

	/**
	*审核通过商城注册会员
	*/
	public function shenhe(){
		 $email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
			$me = M('Member')->where("account='{$eamil}'")->find();
            $where['member_id'] = array('like','%'.$me['member_id'].'%');
        }
        if (!empty($member_id)){
            $where['member_id']=$member_id;
        }
        $count      =  M('Shenqing')->where($where)->count();

        $Page       = new Page($count,20);


        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();

        $list =  M('Shenqing')
				->where($where)
				->order(" member_id desc ")
				->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($list as $k=>$v){
        	$list[$k]['accout'] = $this->getzhucename($v['member_id']);
			$list[$k]['tuijian'] = $this->getzhucename($v['tuijian_id']);
        }

        $this->assign('list',$list);
        $this->assign('page',$show);
        return $this->fetch();
	}

	/**
	*执行审核会员申请
	*/
	public function audit(){
		$status = I('get.status');
		$member_id = I('get.member_id');
		if(empty($status) || empty($member_id)){
			$this->error('参数丢失');
		}
		$re = M('Shenqing')->where("member_id={$member_id}")->setField("status",$status);
		if($re){

			M('Member')->where("member_id={$member_id}")->setField("status",$status);

			/***************************通知商城站审核结果**********************************/
			$url = "http://".$_SERVER['SERVER_NAME']."/Login/reqend";

			$post_data = array ("member_id" => "$member_id","status" => "$status");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			// post数据
			curl_setopt($ch, CURLOPT_POST, 1);
			// post的变量
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			$output = curl_exec($ch);
			curl_close($ch);

			/*********************************************************************************/
			$this->success("审核成功",U('Member/shenhe'));
		}else{
			$this->error('审核失败');
		}

	}

	 /**
     * 今日新增会员
     */
    public function xinzeng(){
		$start_time = strtotime(date("Y-m-d 00:00:00",time()));
		$end_time = time();
        $where['add_time'] = array('between',array($start_time,$end_time));
        $count      =  M('Member')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出

        $list =  M('Member')
            ->where($where)
            ->order(" member_id desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($list as $k=>$v){
        	$list[$k]['tuijian']=M('Member')->where(array('member_id'=>$list[$k]['zhuceid']))->find();

			$name = ($v['level'] >= $v['upgrade_free'])?memberLevel($v[level]):memberLevel($v[upgrade_free]);
			$list[$k]['level'] = $name['name'];
        }

        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        return $this->fetch(); // 输出模板
    }
	 /**
     * 今日入金会员
     */
    public function rujin(){
		$start_time = strtotime(date("Y-m-d 00:00:00",time()));
		$end_time = time();
        $where['add_time'] = array('between',array($start_time,$end_time));
        $count      =  M('Upgrade')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出

        $list =  M('Upgrade')
            ->where($where)
            ->order(" add_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($list as $k=>$v){
        	$list[$k]['tuijian']=M('Member')->where(array('member_id'=>$list[$k]['zhuceid']))->find();
        	$list[$k]['leveled'] = memberLevel($v['leveled']);
        	$list[$k]['level'] = memberLevel($v['level']);
        }

        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        return $this->fetch(); // 输出模板
    }

    /**
     * 等待审核页面
     */
    public function chenghao(){
    	$list = M('Chenghaojiang')->order('add_time desc')->select();
    	foreach($list as $k=>$v){
    		$level = $list[$k]['level'];
    		$list[$k]['content'] = $this->config['chenghao_name_'.$level];
    		$list[$k]['user'] = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->find();
    	}
    	$this->assign('list',$list);
    	return $this->fetch();
    }
    public function chenghaoTong(){
    	$map['id'] = I('id');
    	$data['status'] = 2;
    	$res = M('Chenghaojiang')->where($map)->save($data);
    	if($res){
    		$this->success('成功',U('Member/chenghao'));
    	}else{
    		$this->error('失败',U('Member/chenghao'));
    	}
    }








    public function baobiao_xiazai(){
    	return $this->fetch();
    }

    /**
     * 导出excel文件
     */
    public function baobiaoExcel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list= M('Member')
    	->field("add_time,member_id,account,phone,level,forzen,yeji_righ,yeji_left")
    	->order('add_time asc')
    	->where($where)
    	->select();
    	foreach ($list as $k=>$v){
    		//权宜值转入
    		$qyzzr=M("MoneyEmail")->where(['recive_id'=>$v['member_id'],'type'=>'权益值'])->sum("money");
    		$list[$k]['qyzzr']=empty($qyzzr)?0:$qyzzr;
    		//权宜值转出
    		$qyzzc=M("MoneyEmail")->where(['send_id'=>$v['member_id'],'type'=>'权益值'])->sum("money");
    		$list[$k]['qyzzc']=empty($qyzzc)?0:$qyzzc;
    		//权益值使用数量
    		$qyzsysl=M("MemberZichanbao")->where(['member_id'=>$v['member_id']])->sum("integral");
    		$list[$k]['qyzsysl']=empty($qyzsysl)?0:$qyzsysl;
    		//动力奖数量
    		$donglijiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>36])->sum("money");
    		$list[$k]['donglijiang']=empty($donglijiang)?0:$donglijiang;
    		//管理奖数量
    		$guanlijiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>37])->sum("money");
    		$list[$k]['guanlijiang']=empty($guanlijiang)?0:$guanlijiang;
    		//安盈奖励数量
    		$anyingjiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>28])->sum("money");
    		$list[$k]['anyingjiang']=empty($anyingjiang)?0:$anyingjiang;
    		//直推奖数量
    		$zhituijiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>34])->sum("money");
    		$list[$k]['zhituijiang']=empty($zhituijiang)?0:$zhituijiang;

    		//直推会员数量
    		$list[$k]['zhituirenshu']=M("Member")->where(['zhuceid'=>$v['member_id']])->count();
			unset($item);
    		$itme=$this->daishu((string)$v['member_id'], 20);
    		$list[$k]['tuanduirenshu']=count($itme);

    		$list[$k]['maxyeji']=max($v['yeji_righ'],$v['yeji_left']);
    		$list[$k]['minyeji']=min($v['yeji_righ'],$v['yeji_left']);


    		$chj=M("Chenghaojiang")->where(['member_id'=>$v['member_id']])->select();
    		$list[$k]['n1']="无";
    		$list[$k]['n2']="无";
    		$list[$k]['n3']="无";
    		$list[$k]['n4']="无";
    		$list[$k]['n5']="无";
    		$list[$k]['n6']="无";
    		$list[$k]['n7']="无";

    		if(!empty($chj)){
    			foreach ($chj as $c=>$j){
    				 $list[$k]['n'.$j['level']]='已有'.$this->config['chenghao_name_'.$j['level']];
    			}
    		}
    		$list[$k]['add_time']=date("Y-m-d H:i:s",$v['add_time']);

    	$title = array(
    			'会员注册时间',
    			'会员ID',
    			'账号（名称）',
    			'手机号码',
    			'会员等级',
    			'获得冻结权益值的数量',
    			'右区业绩',
    			'左区业绩',
    			'权益值转入数量',
    			'权益值转出数量',
    			'权益值使用数量',

    			'动力奖数量',
    			'管理奖数量',
    			'安盈奖励数量',
    			'直推奖数量',
    			'直推会员数量',
    			'团队人数',
    			'大区业绩',
    			'小区业绩',
    			'有无电子权益证',
    			'有无平台代理商资格',
    			'有无港澳七日游',
    			'有无欧洲维京游轮豪华游',
    			'有无价值30万SUV一辆',
    			'有无价值100豪车一辆',
    			'有无价值300万海景房一套',


    	);

    	$filename= $this->config['name']."报表信息-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }


    }

   
   

    /**
     * 结构图
     */
 public function jiegou(){
    	$member_id = I('id');
    	$account = I('username');
    	$type = I('type');
    	
    	if($account){
    		$member = Db::name('Users')->where(array('loginName'=>$account))->find();
    		$member_id = $member['userId'];
    	}
    	//上一层
    	if($type){
    		$id = I('id');
    		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$id))->find();
    		$member_id = $member_relation['jiedianid'];
    		
    	}
    	$id = $member_id?$member_id:1;
		$where['member_id'] = $id;
		$this->assign('username',$id);
    	$one = Db::table('wst_app_member_relation')
				->field('a.member_id,a.jiedianid,a.quyu,a.level,a.status,a.yeji_left,a.yeji_righ,b.userId,b.loginName as account')
				->alias('a')
				->join('wst_users b','a.member_id = b.userId')
				->where($where)
				->find();
    	//根据用户status判断用户是否购买过资产包
		$one['sclass']=$this->getClassByUserStatus($one);
		$one['level_detail']=$this->getLevel($one['level']);
    	
		
		
    	
    	$two_one=$this->getUperUsername($one['member_id'],left);
    	$two_two=$this->getUperUsername($one['member_id'],righ);
    	//第三层
    	$three_one=$this->getUperUsername($two_one['member_id'],left);
    	$three_two=$this->getUperUsername($two_one['member_id'],righ);
    	$three_three=$this->getUperUsername($two_two['member_id'],left);
    	$three_four=$this->getUperUsername($two_two['member_id'],righ);
    	//第四层
    	$four_one=$this->getUperUsername($three_one['member_id'], left);
    	$four_two=$this->getUperUsername($three_one['member_id'], righ);
    	$four_three=$this->getUperUsername($three_two['member_id'], left);
    	$four_four=$this->getUperUsername($three_two['member_id'], righ);
    	$four_five=$this->getUperUsername($three_three['member_id'], left);
    	$four_six=$this->getUperUsername($three_three['member_id'], righ);
    	$four_seven=$this->getUperUsername($three_four['member_id'], left);
    	$four_eight=$this->getUperUsername($three_four['member_id'], righ);
 		
    	$this->assign('one',$one);
    	$this->assign('two_one',$two_one);
    	$this->assign("two_two",$two_two);;
    	$this->assign("three_one",$three_one);
    	$this->assign("three_two",$three_two);
    	$this->assign("three_three",$three_three);
    	$this->assign("three_four",$three_four);
    	$this->assign("four_one",$four_one);
    	$this->assign("four_two",$four_two);
    	$this->assign("four_three",$four_three);
    	$this->assign("four_four",$four_four);
    	$this->assign("four_five",$four_five);
    	$this->assign("four_six",$four_six);
    	$this->assign("four_seven",$four_seven);
    	$this->assign("four_eight",$four_eight);
    	$this->assign('level',$_POST['level']);
    	return $this->fetch();
    }
    //获取当前人的下线
	private function getUperUsername($member_id,$quyu){
		$where['jiedianid'] = $member_id;
		$where['quyu']=$quyu;
		$list = Db::table('wst_app_member_relation')
				->field('a.member_id,a.jiedianid,a.quyu,a.level,a.status,a.yeji_left,a.yeji_righ,b.userId,b.loginName as account')
				->alias('a')
				->join('wst_users b','a.member_id = b.userId')
				->where($where)
				->find();
		if($list){
			$list['level_detail']=$this->getLevel($list['level']);
			$list['sclass']=$this->getClassByUserStatus($list);
		}
		return $list;
	}
    public function getLevel($level){
    	$class = Db::name('App_level')->where(array('id'=>$level))->find();
    	return $class;
    }
    //获取级别显示样式的class
    public function getClassByUserStatus($member){
    	if($member['status'] == 0){
    		$status = 9;
    	}else{
    		$status = $member['level'];
    	}
    	switch ($status){
    		case 0:$class='xpt_one';break;
			case 1:$class='xpt_zero';break;
			case 2:$class='xpt_two';break;
			case 3:$class='xpt_three';break;
			case 4:$class='xpt_four';break;
			case 5:$class='xpt_five';break;
			case 6:$class='xpt_six';break;
			case 7:$class='xpt_seven';break;
			case 8:$class='xpt_eight';break;
			default:$class='xpt_wjh';
    	}
    	return $class;
    }

    public function yeji(){
	    $email = input('email');
	    $member_id = input('user_id');
	    $name = input('name');
	    $tuijian = input('tuijian');
	    $this->assign('email',$email);
	    $this->assign('user_id',$member_id);
	    $this->assign('name',$name);
	    $this->assign('tuijian',$tuijian);
	    if(!empty($email)){
	        $where['loginName'] = array('like','%'.$email.'%');
	    }
	    if (!empty($member_id)){
	        $where['userId']=$member_id;
	    }
	    //获取姓名
	    if (!empty($name)){
	        $where1['name']=$name;
	        $list = M('app_member_relation')->where($where1)->select();
	        foreach($list as $k=>$v){
	            $res[] = $list[$k]['member_id'];
	        }
	        $where['userId']=array('in',$res);
	    }
	
	
	    if (!empty($tuijian)){
	        $where2['name']=$tuijian;
	        $list = M('app_member_relation')->where($where2)->find();
	        $where3['zhuceid']=$list['member_id'];
	        $res = M('app_member_relation')->where($where3)->select();
	        foreach($res as $k=>$v){
	            $re[] = $res[$k]['member_id'];
	        }
	        $where['userId']=array('in',$re);
	    }
			
	    $list =  Db::name('Users')->where($where)->order('userId desc')->paginate(20,false,['query'=>input()]);
	    $page = $list->render();
	    $list = $list->all();
	
	    foreach($list as $k=>$v){
	        $relation = M('app_member_relation')->where('member_id='.$v['userId'])->find();
	        if($relation){
	            $list[$k]['name'] = $relation['name'];
	            $list[$k]['level'] = $relation['level'];
	            $tuijian = Db::name('Users')->where('userId='.$relation['zhuceid'])->find();
	            $list[$k]['tuiuser'] = $tuijian['loginName'];
	            $list[$k]['idcard'] = $relation['idcard'];
	            $list[$k]['old_yeji'] = $relation['old_yeji'];
	            $list[$k]['new_yeji'] = $relation['new_yeji'];
	            $list[$k]['yeji'] = $relation['yeji'];
	        }
	
	    }
	    $this->assign('list',$list);// 赋值数据集
	    $this->assign ( 'page', $page );
	    return $this->fetch();
	}
}