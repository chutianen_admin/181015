<?php
/**
 * 	app后台首页
 *  =============================
 * 	后台父类
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\walletadmin\controller;
use Think\Db;
class Member extends AdminBase {
    public function _initialize(){
        parent::_initialize();
    }
    /**
     * 会员列表
     */
    public function index(){
    	
        $email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
            $where['account'] = array('like','%'.$email.'%');
        }
        if (!empty($member_id)){
            $where['userId']=$member_id;
        }
        $list =  M('Users')
            ->where($where)
            ->paginate(15);
        $page = $list->render ();
//         //查看积分
//         $account_type = M('Account_type')->select();
//         for($i=1;$i<count($account_type)+1;$i++){
//         	$aa[] = 'account_type_'.$i;
//         }
//         $bb = implode(',',$aa);
//         foreach ($list as $k=>$v){
//         	$list[$k]['tuijian']=M('Users')->field('account')->where(array('member_id'=>$list[$k]['zhuceid']))->find();
//         	$list[$k]['jiedian']=M('Users')->field('account')->where(array('member_id'=>$list[$k]['jiedianid']))->find();
//  			$class = $this->getUserMoneyByLevel($list[$k]['level']);
// 			$list[$k]['level_name'] = $class['name'];
//         	$list[$k]['member_account'] = M('Member_account')->field($bb)->where(array('member_id'=>$list[$k]['member_id']))->find();
//         }
       
//         $this->assign('account_type',$account_type);
        $this->assign('list',$list);// 赋值数据集
        $this->assign ( 'page', $page );
        return $this->fetch();
    }
    
	/**
	*获取会员账号
	*/
	public function getzhucename($id){
		if(empty($id)){
			return '没有推荐人';
		}
		$list = M('Member')->where("member_id={$id}")->find();
		return $list['account']."（{$list['member_id']}）";
	}
    /**
     * 添加会员
     */
    public function addMember(){
        if(IS_POST){
            $zhitui = I('zhitui');
            $zhitui_member = M('Member')->where(array('account'=>$zhitui))->find();
            $data['phone'] = I('phone');
           	$data['account'] = I('phone');
            $data['zhuceid']=$zhitui_member['member_id'];
            $data['add_time'] = time();
            $member = $this->getMemberByUsername($data['phone']);
            if($member){
                $this->error('用户名重复');
                return;
            }
            if($_POST['pwd']==$_POST['pwdtrade']){
                $this->error('支付密码不能和密码一样');
                return;
            }
            if(empty($_POST['pwd'])||empty($_POST['pwdtrade'])){
                $this->error('密码不能为空');
            }
            $data['password']=md5(I('pwd'));
            $data['pwdtrade']=md5(I('pwdtrade'));
            $data['name'] = I('name');
            $data['idcard'] = I('idcard');
            $re = M('Member')->add($data);
            $arr['account_type_1'] = I('account_type_1');
            $arr['account_type_2'] = I('account_type_2');
            $arr['account_type_3'] = I('account_type_3');
            $arr['account_type_4'] = I('account_type_4');
            $arr['member_id'] = $re;
            $res = M('Member_account')->add($arr);
            if($re){
                $this->success('添加成功',U('Member/index'));
                 return;
            }else{
                $this->error('服务器繁忙,请稍后重试');
                return;
           }
        }else{
            $this->display();
        }
    }
    /**
     * 添加个人信息
     */
    public function saveModify(){
        $member_id = I('get.member_id','','intval');
        $M_member = D('Member');
        if(IS_POST){
            $_POST['status'] = 1;//0=有效但未填写个人信息1=有效并且填写完个人信息2=禁用
            if (!$data=$M_member->create()){ // 创建数据对象
                // 如果创建失败 表示验证没有通过 输出错误提示信息
                $this->error($M_member->getError());
                return;
            }else {
                $where['member_id'] = $_POST['member_id'];
                $r = $M_member->where($where)->save();
                if($r){
                    $this->success('添加成功',U('Member/index'));
                    return;
                }else{
                    $this->error('服务器繁忙,请稍后重试');
                    return;
                }
            }
        }else{
            $where['member_id'] = $member_id;
            $list = $M_member->where($where)->find();
            $this->assign('list',$list);
            $this->display();
        }
    }
    /**
     * 开通空单功能
     */
    public function kongdan(){
    	$member_id = I('member_id');
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	$level = M('Level')->where(array('status'=>1))->select();
    	$this->assign('level',$level);
    	$this->assign('list',$member);
    	$this->display();
    }
    /**
     * 空单提交
     */
    public function dansave(){
    	$id = I('member_id');
    	$data['level'] = I('level');
    	if(empty($data['level'])){
    		$this->error('请选择等级');
    	}
    	$jihuo = M('Member')->where(array('member_id'=>$id))->find();
    	if($data['level']<=$jihuo['level']){
    		$this->error('无法降低等级');
    	}
    	$class = $this->getUserMoneyByLevel($data['level']);
    	//判断是升级还是第一次空单
    	if($jihuo['kongdan'] == 1){
    		$class_old = $this->getUserMoneyByLevel($jihuo['level']);
    		//补差价
    		$money = $class['money']-$class_old['money'];
    	}else{
    		$money = $class['money'];
    	}
    	$data['kongdan'] = 1;
    	$res = M('Member')->where(array('member_id'=>$id))->save($data);
    	 
    	 
    	 
    	$res=$this->jihuoOver($jihuo,$money);
    	 
    	$arr_id = explode(',', $class['account_type_id']);
    	$arr_money = explode(',', $class['start_money']);
    	$r[] = $this->addFinance($jihuo['member_id'],15, '开通空单会员',-$money,1,2);
    	for($i=0;$i<count($arr_id);$i++){
    		$jifen = M('Account_type')->where(array('id'=>$arr_id[$i]))->find();
    		if(!empty($arr_money[$i])){
    			$r[] = M('Member_account')->where(array('member_id'=>$jihuo['member_id']))->setInc('account_type_'.$arr_id[$i],$arr_money[$i]);
    			$r[] = $this->addFinance($jihuo['member_id'],5, '激活'.$class['name'].'会员，赠送'.$jifen['name'].$arr_money[$i],$arr_money[$i],1,2);
    		}
    	}
    	if($r){
    		$this->success('开通成功',U('Member/index'));
    	}else{
    		$this->error('开通失败');
    	}
    }
    
    
    
    
    
    
    
    
    
    /**
     * 显示自己推荐列表
     */
    public function show_my_invit(){
        $member_id = $_GET['member_id'];
        if(empty($member_id)){
            $this->error('参数错误');
            return;
        }
        $M_member = M('Member');
        $count      = $M_member->where(array('pid'=>$member_id))->count();// 查询满足要求的总记录数
        $Page       = new Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $my_invit = $M_member
            ->where(array('pid'=>$member_id))
            ->order(" reg_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        if($my_invit){
            $this->assign('my_invit',$my_invit);
            $this->assign('page',$show);// 赋值分页输出
            $this->display(); // 输出模板
        }else{
            $this->error('抱歉,您还没有推荐其他人');
            return;
        }
    }
    /**
     * 修改会员
     */
    public function saveMember(){
        $member_id = I('get.member_id','','intval');
        $M_member = M('Member');
        if(IS_POST){
            $member_id = I('post.member_id','','intval');
            $zhitui = I('zhitui');
            $zhitui_member = M('Member')->where(array('account'=>$zhitui))->find();
            $data['phone'] = I('phone');
            $data['account'] = I('phone');
            $data['zhuceid']=$zhitui_member['member_id'];
            $data['add_time'] = time();
			if(I('pwd')){
				$data['password']=md5(I('pwd'));
			}
            if(I('pwdtrade')){
            	$data['pwdtrade']=md5(I('pwdtrade'));
            }
            $data['name'] = I('name');
            $data['idcard'] = I('idcard');
            $arr['account_type_1'] = (float)I('account_type_1');
            $arr['account_type_2'] = (float)I('account_type_2');
            $arr['account_type_3'] = (float)I('account_type_3');
            $arr['account_type_4'] = (float)I('account_type_4');
            $re = M('Member')->where(['member_id'=>$member_id])->save($data);
            $res = M('Member_account')->where(['member_id'=>$member_id])->save($arr);
            if($re){
            	$this->success('修改成功',U('Member/index'));
            	return;
            }else{
            	$this->error('服务器繁忙,请稍后重试');
            	return;
            }            
            
        }else{
        	
            if($member_id){
                $where['member_id'] = $member_id;
                $where['type'] = array('neq',2);
                $list = $M_member->where($where)->find();
                $res = M('Address')->where($where)->select();
                
               	$tuijian = M('Member')->where(['member_id'=>$list['zhuceid']])->find();
               	$list['tuijian_name'] = $tuijian['account'];
               	
               	$account_type = M('Account_type')->select();
               	for($i=1;$i<count($account_type)+1;$i++){
               		$aa[] = 'account_type_'.$i;
               	}
               	$bb = implode(',',$aa);
               	$list['member_account'] = M('Member_account')->field($bb)->where(array('member_id'=>$list['member_id']))->find();
               
               	$this->assign('list',$list);
                $this->assign('res',$res);
                $this->display();
            }else{
                $this->error('参数错误');
                return;
            }
        }
    }
    /**
     * 修改收货地址
     */
    public function update(){
    	//三级联动
    	$parent_id['parent_id'] = 1;
    	$region = M('Areas')->where($parent_id)->select();
    	$this->assign('region',$region);
    	//dump($region);die();
    	//三级联动结束
    	$address_id = I('address');
    	$list = M('Address')->where(array('id'=>$address_id))->find();
    	$where['area_id'] = $list['province_id'];
    	$where1['area_id'] = $list['city_id'];
    	$where2['area_id'] = $list['area_id'];
    	$province = M('Areas')->where($where)->select();
    	$city = M('Areas')->where($where1)->select();
    	$area = M('Areas')->where($where2)->select();
    	$this->assign('detailed',$list['detailed']);
    	$this->assign('sheng',$province[0]['area_name']);
    	$this->assign('shi',$city[0]['area_name']);
    	$this->assign('qu',$area[0]['area_name']);
    	$this->assign('province_id',$list['province_id']);
    	$this->assign('city_id',$list['city_id']);
    	$this->assign('area_id',$list['area_id']);
    	$this->assign('member_id',$list['member_id']);
    	$this->assign('phone',$list['phone']);
    	$this->assign('code',$list['code']);
    	$this->assign('linkman',$list['linkman']);
    	$this->assign('id',$list['id']);
    	$this->display();
    }
    public function address(){
    	//三级联动
    	$parent_id['parent_id'] = I('post.pro_id','addslashes');
    	$region = M('Areas')->where($parent_id)->select();
    	$opt = '<option>--请选择市区--</option>';
    	foreach($region as $key=>$val){
    		$opt .= "<option value='{$val['area_id']}'>{$val['area_name']}</option>";
    	}
    	//dump($opt);die();
    	echo $opt;
    	//三级联动结束
    }
    public function doupdate(){
    	if (empty ( $_POST ['linkman'] )) {
    		$this->error('请输入收件人');
    	}
    	if (empty ( $_POST ['pro'] )) {
    		$this->error('请选择省份');
    	}
    	if (empty ( $_POST ['city'] )) {
    		$this->error('请选择城市');
    	}
    	if (empty ( $_POST ['area'] )) {
    		$this->error('请选择区域');
    	}
    	if (empty ( $_POST ['phone'] )) {
    		$this->error('请输入手机号');
    	}
    	if (empty ( $_POST ['code'] )) {
    		$this->error('请输入邮编');
    	}
    	if (empty ( $_POST ['address'] )) {
    		$this->error('请输入详细收货地址');
    	}
    	$sheng_id = I('pro');
    	$sh = M('Areas')->where("area_id='{$sheng_id}'")->find();
    	$sheng = $sh['area_name'];
    	//城市名
    	$s = M('Areas')->find(I('city'));
    	$shi = $s['area_name'];
    	
    	$q = M('Areas')->find(I('area'));
    	$qu = $q['area_name'];
    	
    	$res ['id'] =  I('id');
    	$res ['member_id'] = I('member_id');
    	$res ['address'] = $sheng.$shi.$qu.I('address');
    	$res ['phone'] = I('phone');
    	$res ['code'] = I('code');
    	$res ['linkman'] = I('linkman');
    	$res ['province_id'] = I('pro');
    	$res ['city_id'] = I('city');
    	$res ['area_id'] = I('area');
    	$res ['detailed'] = I('address');
    	$list = M('Address')->save($res);
    	if($list){
    	 	$this->success('修改成功',U('Member/index'));
    	}else{
    		$this->error('修改失败');
    	}
    
    }
    /**
     * 删除收货地址
     */
	public function Del(){
			$id=I('id');
			$address = M('Address')->where(array('id'=>$id))->find();
			if($address['status']!=0){
				$this->error('不要删除默认的收货地址');
			}
			$list ['id'] = $id;
			$list ['type'] = "2";
			$res = M("Address")->save($list);
			if($res){
				$this->success('删除成功');
			}else{
				$this->error('删除失败');
			}
		}
    /**
     * 删除会员
     */
    public function delMember(){
        $member_id = I('get.member_id','','intval');
        $M_member = M('Member');
        //判断还有没有余额
        $where['member_id']= $member_id;
        $member = $M_member->where($where)->find();
		
        $r[] = $M_member->delete($member_id);
        $r[] = M('Member_account')->where($where)->delete();
        $r[] = M('Finance')->where($where)->delete();
        $r[] = M('Orders')->where($where)->delete();
        $r[] = M('Trade')->where($where)->delete();
        $r[] = M('Withdraw')->where('uid='.$member_id)->delete();
        $r[] = M('Pay')->where($where)->delete();
        if($r){
        	//改上线直推num 减1
        	$res = M('Member')->where(array('member_id'=>$member['zhuceid']))->setDec('num',1);
        	
            $this->success('删除成功',U('Member/index'));
            return;
        }else{
            $this->error('删除失败');
            return;
        }
    }
    /**
     * ajax判断邮箱
     * @param $email
     */
    public function ajaxCheckEmail($email){
        $email = urldecode($email);
        $data = array();
        if(!checkEmail($email)){
            $data['status'] = 0;
            $data['msg'] = "邮箱格式错误";
        }else{
            $M_member = M('Member');
            $where['email']  = $email;
            $r = $M_member->where($where)->find();
            if($r){
                $data['status'] = 0;
                $data['msg'] = "邮箱已存在";
            }else{
                $data['status'] = 1;
                $data['msg'] = "";
            }
        }
        $this->ajaxReturn($data);
    }

    /**
     * ajax验证昵称是否存在
     */
    public function ajaxCheckNick($nick){
        $nick = urldecode($nick);
        $data =array();
        $M_member = M('Member');
        $where['nick']  = $nick;
        $r = $M_member->where($where)->find();
        if($r){
            $data['msg'] = "昵称已被占用";
            $data['status'] = 0;
        }else{
            $data['msg'] = "";
            $data['status'] = 1;
        }
        $this->ajaxReturn($data);
    }
    /**
     * ajax手机验证
     */
    function ajaxCheckPhone($phone) {
        $phone = urldecode($phone);
        $data = array();
        if(!checkMobile($phone)){
            $data['msg'] = "手机号不正确！";
            $data['status'] = 0;
        }else{
            $M_member = M('Member');
            $where['phone']  = $phone;
            $r = $M_member->where($where)->find();
            if($r){
                $data['msg'] = "此手机已经绑定过！请更换手机号";
                $data['status'] = 0;
            }else{
                $data['msg'] = "";
                $data['status'] = 1;
            }
        }
        $this->ajaxReturn($data);
    }
    
    /**
     * 查看个人币种
     */
    public function show(){
    	$currency = M('Currency_user');
    	$member = M('Member');
    	$member_id = I('member_id');
    	if(empty($member_id)){
    		$this->error('参数错误',U('Member/index'));
    	}
    	$where['member_id'] = $member_id;
    	$count = $currency->join(C("DB_PREFIX")."currency ON ".C("DB_PREFIX")."currency_user.currency_id = ".C("DB_PREFIX")."currency.currency_id")
    			->where($where)->count();
    	$Page = new \Think\Page ( $count,20); // 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $Page->show();//分页显示输出性
    	$info = $currency->join(C("DB_PREFIX")."currency ON ".C("DB_PREFIX")."currency_user.currency_id = ".C("DB_PREFIX")."currency.currency_id")
    			->where($where)->limit($Page->firstRow.','.$Page->listRows)
    			->select();
    	$member_info = $member->field('member_id,name,phone,email')->where($where)->find();
    	$this->assign('member_info',$member_info);
    	$this->assign('info',$info);
    	$this->assign('page',$show);
    	$this->display();
    }
    //修改个人币种数量
    public function updateMemberMoney(){
    	$member_id=I('post.member_id');
    	$currency_id=I('post.currency_id');
    	$num=I('post.num');
    	$forzen_num=I('post.forzen_num');
    	if(empty($member_id)||empty($member_id)){
    		$data['info']="参数不全";
    		$data['status']=0;
    	}
    	$where['member_id']=$member_id;
    	$where['currency_id']=$currency_id;
    	$r[]=M('Currency_user')->where($where)->setField('num',$num);
    	$r[]=M('Currency_user')->where($where)->setField('forzen_num',$forzen_num);
    	if($r){
    		$data['info']="修改成功";
    		$data['status']=1;
    	}else{
    		$data['info']="修改失败";
    		$data['status']=0;
    	}
    	$this->ajaxReturn($data);
    }
    //一键登录
    public function oneLogin(){
    	$member_id=I('get.member_id');
    	$r=M('Member')->where(array('member_id'=>$member_id))->find();
    	if($r){
    		session('USER_KEY_ID',$r['member_id']);
    		session('USER_KEY',$r['nick']);//用户名
    		$this->redirect('Home/Index/index');
    	}else{
    		$this->error('参数错误');
    	}
    	
    } 
    /***************************************************修改用户账户信息***************************************/
    /**
     * 账户信息修改页面
     */
    public function zhanghu(){
    	$map['type'] = 9;
    	
    	$count = M('Finance')->where($map)->count();
    	$Page = new Page ( $count, 10 );
    	$show = $Page->show ();

    	$list = M('Finance')->where($map)->limit($Page->firstRow.','.$Page->listRows)->order('add_time desc')->select();
    	foreach($list as $k=>$v){
    		$user = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->find();
    		$list[$k]['name'] = $user['account'];
    		$account_type = M('Account_type')->where(array('id'=>$list[$k]['currency_id']))->find();
    		$list[$k]['currency_name'] =$account_type['name'];
    	}
		
    	//查看积分种类表
    	$account_type = M('Account_type')->select();
    	
    	
    	$this->assign('account_type',$account_type);
		$this->assign('end_time',time());
    	$this->assign('list',$list);
    	$this->assign ( 'page', $show );
    	$this->display();
    }
    /**
     * 提交功能
     */
    public function saveZhangHu(){
    	$name = I('name')?I('name'):null;
    	$type = I('type')?I('type'):null;
		$money = I('money')?I('money'):null;
		if(!is_numeric($money)){
			$this->error('请填写正确的内容');
		}
    	$list = M('Member')->where(array('account'=>$name))->find();
    	
    	if(empty($type)){
    		$this->error('请选择要添加的货币类型');
    	}
    	if(empty($list)){
    		$this->error('没找到该用户');
    	}
    	$account_type = M('Account_type')->where(array('id'=>$type))->find();//查看要往哪个币种里面充值   	
    	$member_account = M('Member_account')->where(array('member_id'=>$list['member_id']))->find();//这里是查询账户所剩余额
    	$currcy = 'account_type_'.$type;//查看用户账户下该币种字段
    	$currcy_name = $account_type['name'];//查看用户账户下该币种name
    	$jifen = $member_account['account_type_'.$type];//查看该用户该币种的余额
    	$aa = $jifen+$money;
    	//dump($aa);die();
    	if($aa<0){
    		$this->error('余额不足！无法继续扣款');
    	}
    	if($money>0){
    		$money_type = 1;
    	}else{
    		$money_type = 2;
    	}
    	$res = M('Member_account')->where(array('member_id'=>$list['member_id']))->setInc('account_type_'.$type,$money);
    	//添加余额
    	$bala = $this->balance($list['member_id'],$money_type);
    	$re = $this->addFinance($list['member_id'],9,'管理员操作'.$currcy_name,$money,$type,$money_type,$bala);
    	if($res){
    		$this->success("添加成功");
    	}else{
    		$this->error('添加失败');
    	}
    }
    /**
     * 推荐关系
     */
    public function tuijian(){
    	if (empty ( $_POST ['username'] )) {
    		$user = M('Member')->where("account='admin'")->find();
    	
    	} else {
    		$user = M ( 'Member' )->where ( "account='{$_POST['username']}'" )->find ();
    	}
//     	dump($user);
//     	die();
//     	$user ['utypename'] = $this->getTypeNameById ($user ['utype'] );
    	$user['regtime']=date('Y-m-d',$user['regtime']);
    	$this->assign ( 'user', $user );
    	$this->display ();
    }
    public function getInivt() {
    	$username = $_POST ['username'];
    	$list = M('Member')->where(array('account'=>$username))->find();
    	$U = M ( 'Member' );
    	$r = $U->where ( "zhuceid='{$list['member_id']}'" )->select ();
    	foreach ( $r as $k => $v ) {
    		$r[$k]['add_time']=date('Y-m-d H:i:s',$v['add_time']);
    	}
    	if (! empty ( $r )) {
    		$data ['status'] = 1;
    		$data ['user'] = $r;
    		$this->ajaxReturn ( $data );
    	} else {
    		$data ['status'] = 0;
    		$this->ajaxReturn ( $data );
    	}
    }
    
	public  function upgrade(){
		$email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
            $where['name'] = array('like','%'.$email.'%');
        }
        if (!empty($member_id)){
            $where['member_id']=$member_id;
        }
        $count      =  M('Upgrade')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出
        
        $list =  M('Upgrade')
            ->where($where)
            ->order(" add_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();

		foreach($list as $k=>$v){
			$nameed = memberLevel($v['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			$list[$k]['level'] = $name['name'];
		}
		$this->assign('list',$list);
		$this->display();
	}
	
	 /**
     * 会员升级记录导出excel文件
     */
    public function upgradeExcel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list= M('Upgrade')
    	->field("id,member_id,name,leveled,level,add_time")
    	->order('add_time desc')
    	->where($where)
    	->select();
    	foreach ($list as $k=>$v){
    		$list[$k]['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
			$nameed = memberLevel($v['leveled']);
			unset($list[$k]['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			unset($list[$k]['level']);
			$list[$k]['level'] = $name['name'];
			
    	}
    	$title = array(
    			'ID',
    			'会员ID',
    			'会员姓名',
    			'升级时间',
    			'升级前级别',
    			'升级后级别',
    	);
    	$filename= $this->config['name']."会员升级记录-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
	/**
	*会员虚升记录
	*/
	public function upgrade_free(){
		$email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
            $where['name'] = array('like','%'.$email.'%');
        }
        if (!empty($member_id)){
            $where['member_id']=$member_id;
        }
        $count      =  M('Upgrade_free')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出
        
        $list =  M('Upgrade_free')
            ->where($where)
            ->order(" add_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();

		foreach($list as $k=>$v){
			$nameed = memberLevel($v['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			$list[$k]['level'] = $name['name'];
		}
		$this->assign('list',$list);
		$this->display();
	}
	
	 /**
     * 会员升级(虚升)记录导出excel文件
     */
    public function upgrade_freeExcel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list= M('Upgrade_free')
    	->field("id,member_id,name,leveled,level,add_time")
    	->order('add_time desc')
    	->where($where)
    	->select();
    	foreach ($list as $k=>$v){
    		$list[$k]['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
			$nameed = memberLevel($v['leveled']);
			$list[$k]['leveled'] = $nameed['name'];
			$name = memberLevel($v['level']);
			$list[$k]['level'] = $name['name'];
    	}
    	$title = array(
    			'ID',
    			'会员ID',
    			'会员姓名',
    			'升级时间',
    			'升级前级别',
    			'升级后级别',
    	);
    	$filename= $this->config['name']."会员虚升记录-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
  	/**
  	 * 12.3褚天恩
  	 * 艺尊指数提现
  	 */
    public function yizun(){
    	
    	$count =  M('Yizun_tixian')->where()->count();// 查询满足要求的总记录数
    	$Page  = new \Think\Page($count,10);
    	$show  = $Page->show();
    	$list =  M('Yizun_tixian')->where()->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	foreach($list as $k=>$v){
    		$member_id = $list[$k]['member_id'];
    		$member = M('Member')->where(array('member_id'=>$member_id))->find();
    		$list[$k]['name'] = $member['account'];
    		$list[$k]['full_name'] = $member['name'];
    		$list[$k]['phone'] =$member['phone'];
    		$list[$k]['level'] = $member['level'];
    		$list[$k]['yizun'] = $member['yizun'];
    	}
    	$this->assign('list',$list);// 赋值数据集
    	$this->assign('page',$show);// 赋值分页输出
    	
    	$this->display();
    }
    /**
     * 提现处理方法
     */
    public function tongGuo(){
    	$id = I('id');
    	$data['status'] = 1;
    	$res = M('Yizun_tixian')->where(array('id'=>$id))->save($data);
    	if($res){ 
    		$this->success("已通过");
    	}else{
    		$this->error('系统繁忙');
    	}
    	
    }
    
     /**
     * 系统充值记录导出excel文件
     */
    public function zhanghu_excel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
		$where['type'] = 25;
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list = M('Finance')->field('member_id,type,content,money,currency_id,add_time')->where($where)->order('add_time desc')->select();
    	foreach($list as $k=>$v){
    		$user = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->find();
    		$list[$k]['member_id'] = $user['account'];
			if($v['currency_id']==1){
				$list[$k]['currency_id']='激活积分';
			}
			if($v['currency_id']==2){
				$list[$k]['currency_id'] = '交易积分';
			}
			if($v['currency_id']==3){
				$list[$k]['currency_id'] = '艺宝积分';
			}
			if($v['currency_id']==2){
				$list[$k]['currency_id'] = '艺尊指数';
			}
    		
    		$list[$k]['add_time']=date('Y-m-d H:i:s',$list[$k]['add_time']);
    	}
    	$title = array(
    			'会员账号',
    			'财务类型',
    			'操作内容',
    			'充值金额',
    			'充值币种',
    			'充值时间',
    	);
    	$filename= $this->config['name']."系统充值记录-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
    /**
     * 通过审核
     */
    public function tong(){
		//通过审核为推荐人num字段加1
		$id = I('member_id');
		$data['status'] = 1;
    	$res = M('Member')->where(array('member_id'=>$id))->save($data);
    	if($res){
    		$member = M('Member')->where(array('member_id'=>$id))->find();
    		$zhuceid = $member['zhuceid'];
//     		$res = M('Member')->where(array('member_id'=>$zhuceid))->setInc('num',1);
    		$this->success('成功');
    	}else{
    		$this->error('系统繁忙');
    	}
    	
    }

	/**
	*审核通过商城注册会员
	*/
	public function shenhe(){
		 $email = I('email');
        $member_id=I('member_id');
        if(!empty($email)){
			$me = M('Member')->where("account='{$eamil}'")->find();
            $where['member_id'] = array('like','%'.$me['member_id'].'%');
        }
        if (!empty($member_id)){
            $where['member_id']=$member_id;
        }
        $count      =  M('Shenqing')->where($where)->count();

        $Page       = new Page($count,20);

        
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();
        
        $list =  M('Shenqing')
				->where($where)
				->order(" member_id desc ")
				->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($list as $k=>$v){
        	$list[$k]['accout'] = $this->getzhucename($v['member_id']);
			$list[$k]['tuijian'] = $this->getzhucename($v['tuijian_id']);
        }

        $this->assign('list',$list);
        $this->assign('page',$show);
        $this->display(); 
	}
	
	/**
	*执行审核会员申请
	*/
	public function audit(){
		$status = I('get.status');
		$member_id = I('get.member_id');
		if(empty($status) || empty($member_id)){
			$this->error('参数丢失');
		}
		$re = M('Shenqing')->where("member_id={$member_id}")->setField("status",$status);
		if($re){
			
			M('Member')->where("member_id={$member_id}")->setField("status",$status);
			
			/***************************通知商城站审核结果**********************************/
			$url = "http://".$_SERVER['SERVER_NAME']."/Login/reqend";
			
			$post_data = array ("member_id" => "$member_id","status" => "$status");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			// post数据
			curl_setopt($ch, CURLOPT_POST, 1);
			// post的变量
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			$output = curl_exec($ch);
			curl_close($ch);
			
			/*********************************************************************************/
			$this->success("审核成功",U('Member/shenhe'));
		}else{
			$this->error('审核失败');
		}
		
	}
	
	 /**
     * 今日新增会员
     */
    public function xinzeng(){
		$start_time = strtotime(date("Y-m-d 00:00:00",time()));
		$end_time = time();
        $where['add_time'] = array('between',array($start_time,$end_time));
        $count      =  M('Member')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出
        
        $list =  M('Member')
            ->where($where)
            ->order(" member_id desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($list as $k=>$v){
        	$list[$k]['tuijian']=M('Member')->where(array('member_id'=>$list[$k]['zhuceid']))->find();
        	
			$name = ($v['level'] >= $v['upgrade_free'])?memberLevel($v[level]):memberLevel($v[upgrade_free]);
			$list[$k]['level'] = $name['name'];
        }

        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }
	 /**
     * 今日入金会员
     */
    public function rujin(){
		$start_time = strtotime(date("Y-m-d 00:00:00",time()));
		$end_time = time();
        $where['add_time'] = array('between',array($start_time,$end_time));
        $count      =  M('Upgrade')->where($where)->count();// 查询满足要求的总记录数

        $Page       = new Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)

        //给分页传参数
        setPageParameter($Page, array('email'=>$email,'member_id'=>$member_id));

        $show       = $Page->show();// 分页显示输出
        
        $list =  M('Upgrade')
            ->where($where)
            ->order(" add_time desc ")
            ->limit($Page->firstRow.','.$Page->listRows)->select();
        foreach ($list as $k=>$v){
        	$list[$k]['tuijian']=M('Member')->where(array('member_id'=>$list[$k]['zhuceid']))->find();
        	$list[$k]['leveled'] = memberLevel($v['leveled']);
        	$list[$k]['level'] = memberLevel($v['level']);
        }
		
        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出
        $this->display(); // 输出模板
    }
  
    /**
     * 等待审核页面
     */
    public function chenghao(){
    	$list = M('Chenghaojiang')->order('add_time desc')->select();
    	foreach($list as $k=>$v){
    		$level = $list[$k]['level'];
    		$list[$k]['content'] = $this->config['chenghao_name_'.$level];
    		$list[$k]['user'] = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->find();
    	}
    	$this->assign('list',$list);
    	$this->display();
    }
    public function chenghaoTong(){
    	$map['id'] = I('id');
    	$data['status'] = 2;
    	$res = M('Chenghaojiang')->where($map)->save($data);
    	if($res){
    		$this->success('成功',U('Member/chenghao'));
    	}else{
    		$this->error('失败',U('Member/chenghao'));
    	}
    }
    
    
    
    
    
    
    
    
    public function baobiao_xiazai(){
    	$this->display();
    }
    
    /**
     * 导出excel文件
     */
    public function baobiaoExcel(){
    	//时间筛选
    	$add_time=I('get.add_time');
    	$end_time=I('get.end_time');
    	$add_time = I('add_time')?strtotime(I('add_time')):'';
    	$end_time = I('end_time')?strtotime(I('end_time')):'';
    	$where['add_time'] = array('between',array($add_time,$end_time));
    	$list= M('Member')
    	->field("add_time,member_id,account,phone,level,forzen,yeji_righ,yeji_left")
    	->order('add_time asc')
    	->where($where)
    	->select();
    	foreach ($list as $k=>$v){
    		//权宜值转入
    		$qyzzr=M("MoneyEmail")->where(['recive_id'=>$v['member_id'],'type'=>'权益值'])->sum("money");
    		$list[$k]['qyzzr']=empty($qyzzr)?0:$qyzzr;
    		//权宜值转出
    		$qyzzc=M("MoneyEmail")->where(['send_id'=>$v['member_id'],'type'=>'权益值'])->sum("money");
    		$list[$k]['qyzzc']=empty($qyzzc)?0:$qyzzc;
    		//权益值使用数量
    		$qyzsysl=M("MemberZichanbao")->where(['member_id'=>$v['member_id']])->sum("integral");
    		$list[$k]['qyzsysl']=empty($qyzsysl)?0:$qyzsysl;
    		//动力奖数量
    		$donglijiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>36])->sum("money");
    		$list[$k]['donglijiang']=empty($donglijiang)?0:$donglijiang;
    		//管理奖数量
    		$guanlijiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>37])->sum("money");
    		$list[$k]['guanlijiang']=empty($guanlijiang)?0:$guanlijiang;
    		//安盈奖励数量
    		$anyingjiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>28])->sum("money");
    		$list[$k]['anyingjiang']=empty($anyingjiang)?0:$anyingjiang;
    		//直推奖数量
    		$zhituijiang=M("Finance")->where(['member_id'=>$v['member_id'],'type'=>34])->sum("money");
    		$list[$k]['zhituijiang']=empty($zhituijiang)?0:$zhituijiang;
    		
    		//直推会员数量
    		$list[$k]['zhituirenshu']=M("Member")->where(['zhuceid'=>$v['member_id']])->count();
			unset($item);
    		$itme=$this->daishu((string)$v['member_id'], 20);
    		$list[$k]['tuanduirenshu']=count($itme);
    		
    		$list[$k]['maxyeji']=max($v['yeji_righ'],$v['yeji_left']);
    		$list[$k]['minyeji']=min($v['yeji_righ'],$v['yeji_left']);
    		
    	
    		$chj=M("Chenghaojiang")->where(['member_id'=>$v['member_id']])->select();
    		$list[$k]['n1']="无";
    		$list[$k]['n2']="无";
    		$list[$k]['n3']="无";
    		$list[$k]['n4']="无";
    		$list[$k]['n5']="无";
    		$list[$k]['n6']="无";
    		$list[$k]['n7']="无";

    		if(!empty($chj)){
    			foreach ($chj as $c=>$j){
    				 $list[$k]['n'.$j['level']]='已有'.$this->config['chenghao_name_'.$j['level']];
    			}
    		}
    		$list[$k]['add_time']=date("Y-m-d H:i:s",$v['add_time']);

    	$title = array(
    			'会员注册时间',
    			'会员ID',
    			'账号（名称）',
    			'手机号码',
    			'会员等级',
    			'获得冻结权益值的数量',
    			'右区业绩',
    			'左区业绩',
    			'权益值转入数量',
    			'权益值转出数量',
    			'权益值使用数量',
    	
    			'动力奖数量',
    			'管理奖数量',
    			'安盈奖励数量',
    			'直推奖数量',
    			'直推会员数量',
    			'团队人数',
    			'大区业绩',
    			'小区业绩',
    			'有无电子权益证',
    			'有无平台代理商资格',
    			'有无港澳七日游',
    			'有无欧洲维京游轮豪华游',
    			'有无价值30万SUV一辆',
    			'有无价值100豪车一辆',
    			'有无价值300万海景房一套',
    			
    			
    	);
		
    	$filename= $this->config['name']."报表信息-".date('Y-m-d',time());
    	$r = exportexcel($list,$title,$filename);
    }
    

    }
    
    /**
     * 报单申请页面
     */
    public function baodan_list(){
    	$Baodanshenqing = M('Baodanshenqing'); // 实例化User对象
    	$count      = $Baodanshenqing->count();// 查询满足要求的总记录数
    	$Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
    	$show       = $Page->show();// 分页显示输出
    	// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
    	$list = $Baodanshenqing->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->assign('list',$list);// 赋值数据集
    	$this->assign('page',$show);// 赋值分页输出
    	$this->display();
    }
    
    /**
     * 报单中心通过方法
     */
    public function baodan_pass(){
    	$id=I('id',"");
    	if(empty($id)){
    		$this->error("无效参数，请核实后在操作");
    	}
    	$baodan=M("Baodanshenqing")->where(['id'=>$id])->find();
    	if(empty($baodan)){
    		$this->error("无效数据");
    	}
    	if($baodan['status']!=1){
    		$this->error("已经审核，无法重复审核");
    	}
    	$re=M("Baodanshenqing")->where(['id'=>$id])->setField("status",2);
    	if($re){
    		M("Member")->where(['member_id'=>$baodan['member_id']])->setField("is_baodan",1);
    		$this->success("审核通过");
    	}else{
    		$this->error("操作出错");
    	}
    }
    
    /**
     * 报单中心驳回方法
     */
    public function baodan_no_pass(){
    	$id=I('id',"");
    	if(empty($id)){
    		$this->error("无效参数，请核实后在操作");
    	}
    	$baodan=M("Baodanshenqing")->where(['id'=>$id])->find();
    	if(empty($baodan)){
    		$this->error("无效数据");
    	}
    	if($baodan['status']!=1){
    		$this->error("已经审核，无法重复审核");
    	}
    	$re=M("Baodanshenqing")->where(['id'=>$id])->setField("status",-1);
    	if($re){
    		$this->success("申请被驳回");
    	}else{
    		$this->error("操作出错");
    	}
    	
    	
    }
    
    /**
     * 结构图
     */
 public function jiegou(){
    	$member_id = I('myuser');
    	$account = I('account');
    	$type = I('type');
    	if($account){
    		$member = M('Member')->where(array('account'=>$account))->find();
    		$member_id = $member['member_id'];
    	}
    	//上一层
    	if($type){
    		$id = I('id');
    		$member = M('Member')->where(array('member_id'=>$id))->find();
    		$member_id = $member['jiedianid'];
    		
    	}
    
    	$id = $member_id?$member_id:1;
    
    	$this->assign('username',$id);
    	$one=M('Member')->where("member_id='$id'")->find();
    	//根据用户status判断用户是否购买过资产包
    	$one['sclass']=$this->getClassByUserStatus($one['level']);
    	$one['level_detail']=$this->getLevel($one['level']);
    	$one['left_all_money']=intval($one['left_all_money']/$this->jihuo);
    	$one['right_all_money']=intval($one['right_all_money']/$this->jihuo);
    	$one['left_money']=intval($one['left_money']/$this->jihuo);
    	$one['right_money']=intval($one['right_money']/$this->jihuo);
    	$two_one=$this->getUperUsername($one['member_id'],left);//A区
    	// 		$two_two=$this->getUperUsername($one['uname'],cent);//B区
    	// 		$two_three=$this->getUperUsername($one['uname'],righ);//C区
    	$two_two=$this->getUperUsername($one['member_id'],righ);//B区
    	$three_one=$this->getUperUsername($two_one['member_id'],left);//A区
    	// 		$three_two=$this->getUperUsername($two_one['uname'],cent);//B区
    	// 		$three_three=$this->getUperUsername($two_one['uname'],righ);//C区
    	$three_two=$this->getUperUsername($two_one['member_id'],righ);//B区
    	$three_three=$this->getUperUsername($two_two['member_id'],left);//A区
    	// 		$three_five=$this->getUperUsername($two_two['uname'],cent);//B区
    	// 		$three_six=$this->getUperUsername($two_two['uname'],righ);//C区
    	$three_four=$this->getUperUsername($two_two['member_id'],righ);//B区
    	// 		$three_five=$this->getUperUsername($two_three['uname'],left);//A区
    	// 		$three_eight=$this->getUperUsername($two_three['uname'],cent);//B区
    	// 		$three_nine=$this->getUperUsername($two_three['uname'],righ);//C区
    	// 		$three_six=$this->getUperUsername($two_three['uname'],righ);//B区
    	if ($_POST['level']==4) {
    		$four_one=$this->getUperUsername($three_one['member_id'], left);
    		$four_two=$this->getUperUsername($three_one['member_id'], righ);
    		$four_three=$this->getUperUsername($three_two['member_id'], left);
    		$four_four=$this->getUperUsername($three_two['member_id'], righ);
    		$four_five=$this->getUperUsername($three_three['member_id'], left);
    		$four_six=$this->getUperUsername($three_three['member_id'], righ);
    		$four_seven=$this->getUperUsername($three_four['member_id'], left);
    		$four_eignt=$this->getUperUsername($three_four['member_id'], righ);
    	}
    	$this->assign('one',$one);
    	$this->assign('two_one',$two_one);
    	$this->assign("two_two",$two_two);
    	// 		$this->assign("two_three",$two_three);
    	$this->assign("three_one",$three_one);
    	$this->assign("three_two",$three_two);
    	$this->assign("three_three",$three_three);
    	$this->assign("three_four",$three_four);
    	$this->assign("four_one",$four_one);
    	$this->assign("four_two",$four_two);
    	$this->assign("four_three",$four_three);
    	$this->assign("four_four",$four_four);
    	$this->assign("four_five",$four_five);
    	$this->assign("four_six",$four_six);
    	$this->assign("four_seven",$four_seven);
    	$this->assign("four_eignt",$four_eignt);
    	$this->assign('level',$_POST['level']);
    	$this->display();
    }
    //获取当前人的下线
	private function getUperUsername($username,$quyu){
		$where['jiedianid']=$username;
		$where['quyu']=$quyu;
		$list=M('Member')->where($where)->order("quyu asc")->find();
		if ($list) {
			$list['level_detail']=$this->getLevel($list['level']);
			$list['sclass']=$this->getClassByUserStatus($list['level']);
		}
		return $list;
	}
    public function getLevel($level){
    	$class = M('Level')->where(array('id'=>$level))->find();
    	return $class;
    }
    //获取级别显示样式的class
    public function getClassByUserStatus($status){
    	switch ($status){
    		case 0:$class='xpt_zero';break;
			case 1:$class='xpt_one';break;
			case 2:$class='xpt_two';break;
			case 3:$class='xpt_three';break;
			case 4:$class='xpt_four';break;
			case 5:$class='xpt_five';break;
			case 6:$class='xpt_six';break;
			case 7:$class='xpt_seven';break;
			case 8:$class='xpt_eight';break;
			default:$class='xpt_wjh';
    	}
    	return $class;
    }
    
    
    
    
    
}