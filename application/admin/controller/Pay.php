<?php
namespace app\admin\controller;
use Think\Db;
require_once (APP_PATH .'api/FinanceApi.php');
require_once (APP_PATH .'api/RewardApi.php');
class Pay extends AdminBase {
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	
     //人工充值审核页面
    public function payByMan(){
    
    		$status=I('status');
    		$member_name=I('member_name');
    		if(!empty($status)||$status==="0"){
    			$where[C("DB_PREFIX")."pay.status"]=$status;
    		}
    		if(!empty($member_name)){
    			$where[C("DB_PREFIX")."pay.member_name"]=array('like',"%".$member_name."%");
    		}
    	$count =  M('Pay')->where($where)->count();// 查询满足要求的总记录数
    	$Page  = new \Think\Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)
    	//给分页传参数
    	setPageParameter($Page, array('status'=>$status,'member_name'=>$member_name));
    	
    	$show       = $Page->show();// 分页显示输出

    	$list= M('Pay')
    	->field(C("DB_PREFIX")."pay.*,".C("DB_PREFIX")."member.email")
    	->join("left join ".C("DB_PREFIX")."member on ".C("DB_PREFIX")."member.name=".C("DB_PREFIX")."pay.member_name")

    	->where($where)
    	->limit($Page->firstRow.','.$Page->listRows)
    	->order('add_time desc')
    	->select();
//     	echo M("Pay")->getLastSql();die;
    	foreach ($list as $k=>$v){
    		$list[$k]['status']=payStatus($v['status']);
    		$list[$k]['pay_reward']=$v['money']*$this->config['pay_reward']/100;
    	}
    	$this->assign('page',$show);
    	$this->assign('list',$list);
    	$this->assign('empty','暂无数据');
     	return $this->fetch();
     }
     //人工充值审核处理
     public function payUpdate(){
     	$pay=M('Pay');
     	$where['pay_id']=$_POST['pay_id'];
     	$list=$pay->where($where)->find();
     	if($list['status']!=0){
     		$data['status'] = -1;
     		$data['info'] = "请不要重复操作";
     		$this->ajaxReturn($data);
     	}
     	$member_id=M('Member')->where("member_id='".$list['member_id']."'")->find();
     	if($_POST['status']==1){
     		$pay->where($where)->setField('status',1);
     		if($list['money']>=$this->congif['pay_reward_limit']){
     			$list['count']=$list['count']+$list['money']*$this->config['pay_reward']/100;
     		}
     		//修改member表钱数
     		$rs=M('Member')->where("member_id='".$list['member_id']."'")->setInc('rmb',$list['count']);
     		//添加财务日志
     		$this->addFinance($member_id['member_id'],6,"线下充值".$list['count']."。",$list['count'],1,0);
     		//添加信息表
     		$this->addMessage_all($member_id['member_id'], -2, '人工充值成功', '您申请的人工充值已成功，充值金额为'.$xnb);
     	}elseif($_POST['status']==2){
     		$rs=$pay->where($where)->setField('status',2);
     		//添加信息表
      		$this->addMessage_all($member_id['member_id'], -2, '人工充值审核未通过', '您申请的人工充值审核未通过,请重新处理');
     	}else{
     		$data['status'] = 0;
     		$data['info'] = "操作有误";
     		$this->ajaxReturn($data);
     	}
     	if($rs){
     		$data['status'] = 1;
     		$data['info'] = "修改成功";
     		$this->ajaxReturn($data);
     	}else{
     		$data['status'] = 2;
     		$data['info'] = "修改失败";
     		$this->ajaxReturn($data);
     	}
     }
     /**
      * 添加管理员充值
      */
     public function admRecharge(){
     	if(IS_POST){
     		$admin=M("Admin")->where("admin_id='{$_SESSION['admin_userid']}'")->find();
     		if(empty($_POST['password'])){
     			$this->error("请输入管理员密码");
     		}
     		if(md5($_POST['password'])!=$admin['password']){
     			$this->error("您输入的管理员密码错误");
     		}
     		if(empty($_POST['email'])){
     			$this->error('请输入充值人员');
     		}
     		if(!isset($_POST['currency_id'])){
     			$this->error('请输入币种');
     		}
     		if(empty($_POST['money'])){
     			$this->error('请输入充值金额');
     		}
     		$email=I('email');
     		$member=M('Member')->where("email='".$email."'")->find();
     		if(!$member){
     			$this->error('用户不存在');
     		}
     		$data['member_id'] = $member['member_id'];
     		$data['member_name'] = $member['email'];
     		$data['currency_id'] = I('currency_id');
     		$data['money'] = I('money');
     		$data['status'] = 1;
     		$data['add_time']  = time();
     		$data['type'] = 3;//管理员充值类型
     		M()->startTrans();//开启事务
     		$r[] = M('Pay')->add($data);
     		if($data['currency_id']==0){
     			$r[] = M('Member')->where(array('member_id'=>$data['member_id']))->setInc('rmb',$data['money']);
     		}
     		if($data['currency_id']==-1){
     			$r[] = M('Member')->where(array('member_id'=>$data['member_id']))->setInc('xnb',$data['money']);
     		}
     		if($data['currency_id']==1){
     			$r[] = M('Member')->where(array('member_id'=>$data['member_id']))->setInc('jihuobi',$data['money']);
     		}
     		$r[] = addKFinanceLog($data['member_id'], 3, "管理员充值", $data['money'], 1, $data['currency_id']);
     		if(!in_array(false,$r)){
     			M()->commit();
     			$this->success('添加成功');
     			
     		}else{
     			M()->rollback();
     			$this->error('添加失败');
     		}
     	}else{
	     	$type_id=I('type_id');
			$email=I('email');
			$member_id=I('member_id');
			if(!empty($email)){
				$uid=M('Member')->where("email like '%{$email}%'")->find();
				$where["member_id"]=$uid['member_id'];
			}
	        if(!empty($member_id)){
	            $where["member_id"]=$member_id;
	        }
	        $where['type']=3;
     		$count =  M('Pay')->where($where)->count();// 查询满足要求的总记录数
     		$Page  = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
     		//给分页传参数
     		setPageParameter($Page, array('type_id'=>$type_id,'email'=>$email,'member_id'=>$member_id));
     		
     		$show       = $Page->show();// 分页显示输出
     		
     		$list= M('Pay')->where($where)->limit($Page->firstRow.','.$Page->listRows)->order('add_time desc')->select();
     		$this->assign('page',$show);
     		$this->assign('list',$list);
     		return $this->fetch();
     	}
     }
	  /**
      * 导出excel文件
      */
     public function derivedExcel(){

     	//时间筛选
     	$add_time=I('get.add_time');
     	$end_time=I('get.end_time');
     	$add_time=empty($add_time)?0:strtotime($add_time);
     	$end_time=empty($end_time)?0:strtotime($end_time);

     	$where[C("DB_PREFIX").'pay.add_time'] = array('lt',$end_time);
     	$list= M('Pay')
    	->field(C("DB_PREFIX")."pay.pay_id,"
		.C("DB_PREFIX")."member.email,"
		.C("DB_PREFIX")."member.name,"
		.C("DB_PREFIX")."pay.account,"
		.C("DB_PREFIX")."pay.money,"
		.C("DB_PREFIX")."pay.status,"
		.C("DB_PREFIX")."pay.add_time")
    	->join("left join ".C("DB_PREFIX")."member on ".C("DB_PREFIX")."member.name=".C("DB_PREFIX")."pay.member_name")

    	->where($where)
    	->where(C("DB_PREFIX").'pay.add_time>'.$add_time)
    	->order('add_time desc')
    	->select();
//     	echo M("Pay")->getLastSql();die;
    	foreach ($list as $k=>$v){
    		$list[$k]['status']=payStatus($v['status']);
    		$list[$k]['add_time']=date('Y-m-d H:i:s',$list[$k]['add_time']);
    	}
     	$title = array(
     		'订单号',
     		'汇款人账号',
     		'汇款人',
     		'银行卡号',
     		'充值钱数',
     		'实际打款',
     		'状态',
     		'时间',
     	);
     	$filename= $this->config['name']."人工充值日志-".date('Y-m-d',time());
     	$r = exportexcel($list,$title,$filename);
     }
	  //人工充值审核页面
    public function fill(){
     		$count =  M('Fill')->count();// 查询满足要求的总记录数
     		$Page  = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
     		$show       = $Page->show();// 分页显示输出
     		$list= M('Fill')->where()->limit($Page->firstRow.','.$Page->listRows)->order('id desc')->select();
     		$this->assign('page',$show);
     		$this->assign('list',$list);
     		return $this->fetch();
     }
     
     
     
     
     
     
     
     
     /**
      * 提现列表
      */
     public function withdraw_list(){
     	$account = input('account');
        if(!empty($account)){
            $member = Db::name("Users")->where(array('loginName'=>$account))->find();
            $where['member_id']=$member['userId'];
            $this->assign("account",$account);
        }
        $list= Db::name('Withdraw')->where($where)->order('add_time desc')->paginate(15,false,['query'=>input()]);
        $page = $list->render();
        $list = $list->all();
        foreach($list as $k=>$v){
        	$user = Db::name('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
            $member = Db::name('App_member_relation')->where(array('member_id'=>$list[$k]['member_id']))->find();
            $list[$k]['account'] = $user['loginName'];
            $list[$k]['kaihuren'] = $member['kaihuren'];
            $list[$k]['kaihu_address'] = $member['kaihu_address'];
            $list[$k]['bank_card'] = $member['bank_card'];
            $list[$k]['zhifubao'] = $member['zhifubao'];
        }
        $this->assign('page',$page);
        $this->assign('list',$list);
        return $this->fetch();
     }
     
     
     /**
      * 审核通过方法
      */
     public function withdraw_pass(){
        $id=I('id',"");
        if(empty($id)){
            $this->error("无效参数");
        }
        $withdraw=M('Withdraw')->where(array('id'=>$id))->find();
        if(empty($withdraw)){
            $this->error("无效数据");
        }
       // dump($withdraw['status']);die;
        if($withdraw['status'] !='0'){
            $this->error("已经审核，请勿重复审核");
        }
        $data['status']=2;
        $data['admin_id']=$_SESSION['admin_userid'];
        $result=M('Withdraw')->where(array('id'=>$id))->save($data);
        if($result){
            $this->success("审核成功");
        }else{
            $this->error("审核出错，请核实");
        }
     }
     
     
     /**
      * 审核驳回方法
      */
     public function withdraw_no_pass(){
        $id = input('id');
        if(empty($id)){
            $this->error("无效参数");
        }
        $withdraw = Db::name('Withdraw')->where(array('id'=>$id))->find();
        if(empty($withdraw)){
            $this->error("无效数据");
        }
        if($withdraw['status']!=0){
            $this->error("已经审核，请勿重复审核");
        }
        $data['status']=-1;
        $data['admin_id']=$_SESSION['admin_userid'];
        $result=M('Withdraw')->where(array('id'=>$id))->save($data);
        if($result){
            M("Member")->where(['member_id'=>$withdraw['uid']])->setInc("account_type_".$withdraw[''],$withdraw['money']);
            $this->success("驳回成功");
        }else{
            $this->error("审核出错，请核实");
        }
     }
     
     
     /**
      * 
      */
     public function withdraw_trans_excel(){
     	//时间筛选
     	$add_time=I('get.add_time');
     	$end_time=I('get.end_time');
     	$add_time=empty($add_time)?0:strtotime($add_time);
     	$end_time=empty($end_time)?time():strtotime($end_time);
     	
     	$where['add_time'] = array('between',array($add_time,$end_time));
     	$field="withdraw_id,uid,money,name,cardname,cardnum,status,add_time";
     	$list= M('Withdraw')
     			->field($field)
     			->select();
     	foreach ($list as $k=>$v){
     		$list[$k]['uid']=getMemberNameByMemberid($v['uid']);
     		$list[$k]['status']=withdrawstatus($v['status']);
     		$list[$k]['add_time']=date('Y-m-d H:i:s',$list[$k]['add_time']);
     	}
 
     	$title = array(
     			'ID号',
     			'用户账号',
     			'提现金额',
     			'名字',
     			'提现信息',
     			'卡号',
     			'状态',
     			'时间',
     	);
     	$filename= $this->config['name']."提现日志-".date('Y-m-d',time());
     	$r = exportexcel($list,$title,$filename);
     }
     
     
}