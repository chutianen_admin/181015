<?php
/**
 *  =============================
 * 	后台父类
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Phone extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
    /**
     * 首页文章
     */
   	public function index(){
   		$list_ing = Db::name('App_phone_pay')->where(array('game_state'=>0))->select();
   		foreach($list_ing as $m=>$n){
   			$data['game_state'] = $this->getOrderStatus($list_ing[$m]['id']);

   			$res[] = Db::name('App_phone_pay')->where(array('id'=>$list_ing[$m]['id']))->update($data);
   			
   		}
        $list = Db::name('App_phone_pay')->order('add_time desc')->paginate(20,false,['query'=>input()]);
    	$page = $list->render();
    	$list = $list->all();
        foreach($list as $k=>$v){
        	$member = Db::name('App_member_relation')->where(array('member_id'=>$list[$k]['member_id']))->find();
        	$list[$k]['name'] = $member['name'];
        	
        }
        
       $this->assign('page',$page);
       $this->assign('info',$list);
       return $this->fetch();
    }
    /**
     * 获得订单状态
     * @param unknown $order_id
     * /*状态 1:成功 9:失败 0：充值中*
     */
    public function getOrderStatus($id){
    	$order = Db::name('App_phone_pay')->where(array('id'=>$id))->find();
    	$order_web = $this->payOrder($order['uorderid']);
    	$order_array = json_decode($order_web,true);
    	$data['game_state'] = $order_array['game_state'];
    	$res = Db::name('App_phone_pay')->where(array('id'=>$id))->update($data);
    	return $res;
    }
    /**
     * 订单状态查询
     * @param unknown $number
     * @param unknown $price
     * @return mixed
     */
    public	function payOrder($sporder_id){
    	$url="http://op.juhe.cn/ofpay/mobile/ordersta?key=8f4c6d95dc57b7cb456798e4720fcbc6&orderid=".$sporder_id;
    	$curl = curl_init (); // 启动一个CURL会话
    	curl_setopt ( $curl, CURLOPT_URL, $url ); // 要访问的地址
    	curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, 0 ); // 对认证证书来源的检查
    	@curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, 1 ); // 使用自动跳转
    	curl_setopt ( $curl, CURLOPT_AUTOREFERER, 1 ); // 自动设置Referer
    	curl_setopt ( $curl, CURLOPT_TIMEOUT, 120 ); // 设置超时限制防止死循环
    	curl_setopt ( $curl, CURLOPT_HEADER, 0 ); // 显示返回的Header区域内容
    	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 ); // 获取的信息以文件流的形式返回
    	$tmpInfo = curl_exec ( $curl ); // 执行操作
    	if (curl_errno ( $curl )) {
    		echo 'Errno' . curl_error ( $curl );
    	}
    	curl_close ( $curl ); // 关键CURL会话
    	return $tmpInfo; // 返回数据
    }
    
    
   
}