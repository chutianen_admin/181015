<?php
/**
 * 	app后台返利管理
 *  =============================
 * 	后台父类
 * 	=============================
 *	Author: 李宏宇
 *  Date: 2017-10-31
 * @author Administrator
 *
 */
namespace app\admin\controller;
use Think\Db;
class Rebate extends AdminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
    
   	public function index(){
   		$phone = I('phone');
   		$member_id= I('member_id');
   		$start_time = I('start_time');
   		$end_time = I('end_time');
   		
   		if(!empty($phone)){
   			$user = db('Users')->where(array('loginName'=>$phone))->find();
   			$where['member_id'] = $user['userId'];
   		}
   		if(!empty($member_id)){
   			$where['member_id'] = $member_id;
   		}
   		if(!empty($start_time)&&!empty($end_time)){
   			$where['add_time'] = array('BETWEEN',array(strtotime($start_time),strtotime($end_time)));
   		}
       $list=db('app_fanli')->where($where)->order('add_time desc')->paginate(20);
       $page = $list->render();
       $list = $list->all();
       foreach ($list as $k=>$v){
	       	$member = db('Users')->where(array('userId'=>$v['member_id']))->find();
	       	$list[$k]['loginName'] =$member['loginName'];
	       	$list[$k]['release'] = $list[$k]['all_jifen']-$list[$k]['remain_jifen'];
	       	$list[$k]['release_day'] = $list[$k]['all_day']-$list[$k]['remain_day'];
       }
       $this->assign('page',$page);
       $this->assign('list',$list);
       return $this->fetch();
    }
    
    
    public function status(){
    	$type = I('type');
    	$id = I('id');
    	if(empty($id) || empty($type)){
    		$this->error('参数错误');
    	}
    	if($type == 1){
    		$data['status']=1;
    	}else{
    		$data['status']=0;
    	}
    	
    	$list = db('app_fanli')->where(array("id"=>$id))->update($data);
    	if($list){
    		$this->success('操作成功');
    	}else{
    		$this->error('操作失败');
    	}
    	
    	
    }
   public function details(){
   		$id = I('id','');
   		$phone = I('phone','');
   		$add_time=strtotime(I('start_time',''));
   		$end_time=strtotime(I('end_time',''));
   		if(!empty($phone)){
   			$member = db('Users')->where(array('loginName'=>$phone))->find();
   			if ($member) {
   				$where['member_id']=$member['userId'];
   			}
   		}
   		if(!empty($add_time) && !empty($end_time)){
   			$where['add_time'] = array('between',array($add_time,$end_time));
   		}
   		if(empty($id)){
   			$this->error('参数错误');
   		}else{
   			$where['fanli_id'] = $id;
   		}
   		$list = db('app_fanli_finance')->where($where)->order('add_time desc')->paginate(15,false,['query'=>request()->param(),]);
   		$page = $list->render();
   		$list = $list->all();
   		foreach($list as $k=>$v){
   			$user = db('Users')->where(array('userId'=>$list[$k]['member_id']))->find();
   			$list[$k]['userName'] = $user['loginName'];
   			$account = db('app_account_type')->where(array('id'=>$list[$k]['account_type']))->find();
   			$list[$k]['account'] = $account['name'];
   		}
   		$this->assign('id',$id);
   		$this->assign('page',$page);
   		$this->assign('list',$list);
   		return $this->fetch();
   }
    
    
   
  
}