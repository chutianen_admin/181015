<?php
/**
 *  =============================
 * 	店铺申请 2
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\walletadmin\controller;
use Think\Db;
require_once (APP_PATH . 'api/FinanceApi.php');
class Shop extends WalletadminBase{
	//空操作
	public function _initialize(){
		parent::_initialize();
	}
    /**
     * 首页文章
     */
   	public function index(){
		
        $list = Db::name('App_shop')->order('add_time desc')->paginate(20,false,['query'=>input()]);
    	$page = $list->render();
    	$list = $list->all();
        foreach($list as $k=>$v){
        	$member = Db::name('App_member_relation')->where(array('member_id'=>$list[$k]['member_id']))->find();
        	$list[$k]['name'] = $member['name'];
        }
       $this->assign('page',$page);
       $this->assign('info',$list);
       
       
       return $this->fetch();
    }
    /**
     * 通过
     */
	public function doYes(){
		$id = input('id');
		$data['status'] = 1;
		$res = Db::name('App_shop')->where(array('id'=>$id))->update($data);
		$shop = Db::name('App_shop')->where(array('id'=>$id))->find();
		if($res){
			$data['shopStatus'] = 1;
			$re[] = Db::name('Shops')->where(array('userId'=>$shop['member_id']))->update($data);
			$arr['level'] = 2;
			$re[] = Db::name('App_member_relation')->where(array('member_id'=>$shop['member_id']))->update($arr);
			//店铺推荐奖励
			$re[] = $this->shopZhiTui($shop['member_id'],$shop);
			
			$this->success('成功',U('Shop/index'));
			return;
		}else{
			$this->error('失败',U('Shop/index'));
			return;
		}
	}
	/**
	 * 
	 * @param unknown $member_id
	 */
	public function shopZhiTui($member_id,$shop){
		$list = $this->getUpUserByTuiJianId($member_id,3);
		// 		dump($list);
		// 		die();
		for($i=1;$i<count($list);$i++){
			$class = $this->getUserMoneyByLevel($list[$i]['level']);
			//店铺才会获得奖励
			if($list[$i]['level'] == 2){
				$jiangli = $shop['all_money']*$class['shop_zhitui_bili_'.$i]*0.01;
				if($jiangli > 0){
					$status = $this->addUserMoney($list[$i]['member_id'],$jiangli,$shop);
				}
			}
		}
		return $status;
	}
	
	/**
	 * 给用户钱包加钱
	 * @param unknown $userId
	 * @param unknown $money
	 */
	public function addUserMoney($member_id,$money,$shop){
		//获得会员信息
		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->find();
		//获得会员等级比例
		$class = $this->getUserMoneyByLevel($member_relation['level']);
		//实例化日志api
		$finance = new \FinanceApi();
		//发放奖励
		$r[] = Db::name('App_member_account')->where(array('member_id'=>$member_id))->setInc('account_type_'.$shop['pay_type'],$money);
		//添加日志
		$r[] = $finance->addFinance($member_id,18,'店铺申请，直推奖',1,$money,$shop['pay_type']);
		//税及手续费
		$fee =$this->fee($money,$class);
		if($fee['fee']){
			$r[] = $finance->addFinance($member_id,6,'店铺申请推荐奖励，扣除手续费'.$fee['fee'],2,$fee['fee'],'');
		}
		if($fee['fuwu']){
			$r[] = $finance->addFinance($member_id,7,'店铺申请推荐奖励，扣除服务费'.$fee['fuwu'],2,$fee['fuwu'],'');
		}
		return $r;
	}
	/**
	 * 手续费扶贫基金
	 * @param unknown $money
	 */
	public function fee($money,$class){
		$fee_bili = $class['shop_fee'];
		$fuwu_bili = $class['shop_fuwu'];
		$fee['fee'] = $money*$fee_bili*0.01;
		$fee['fuwu'] = $money*$fuwu_bili*0.01;
		return $fee;
	}
	
	/**
	 * 递归查找上级推荐会员
	 * @param unknown $id
	 * @param unknown $lv
	 * @param number $k
	 * @return unknown
	 */
	public function getUpUserByTuiJianId($id,$lv,$k=0){
		$member_relation = Db::name("App_member_relation")->where(array('member_id'=>$id))->find();
		$k++;
		if (!empty($member_relation)) {
			$item[]=$member_relation;
			if ($k<$lv) {
				$list = $this->getUpUserByTuiJianId($member_relation['zhuceid'],$lv,$k);
				for($i = 0;$i<count($list);$i++){
					if(!empty($list[$i])){
						$item[] = $list[$i];
					}
				}
			}
		}
		return $item;
	}
	
	/**
	 * 拒绝
	 */
    public function doNot(){
    	$id = input('id');
    	$list = Db::name('App_shop')->where(array('id'=>$id))->find();
    	$data['status'] = 2;
    	$res = Db::name('App_shop')->where(array('id'=>$id))->update($data);
    	if($res){
    		$r[] = Db::name('App_member_account')->where(array('member_id'=>$list['member_id']))->setInc('account_type_'.$list['pay_type'],$list['all_money']);
    		$finance = new \FinanceApi();
    		$r[] = $finance->addFinance($list['member_id'], 17, '店铺申请，被驳回', 1, $list['all_money'], $list['pay_type']);
    		$this->success('驳回成功',U('Shop/index'));
    		return;
    	}else{
    		$this->error('驳回失败',U('Shop/index'));
    		return;
    	}
    }
    
    
    
   
}