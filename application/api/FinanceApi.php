<?php
use think\Session;
use think\Db;
use think\Config;
/**
 * @description  	日志接口
 * @author      	褚天恩 <chutianen@163.com>
 * @date 2017-10-16 时间
 * @version 1.0.0	版本
 * @copyright
 */
class FinanceApi {
	public $finance_month_name;
	
	/**
	 * 构造函数
	 * 判断是否有当前月份日志表
	 */
	function __construct(){
// 		$finance_month_name = $this->judgeNowMonthFinance();
// 		$this->finance_month_name = $finance_month_name;
	}
	/**
	 * 判断是否存在当前月份日志表
	 * @return string
	 */
	public function judgeNowMonthFinance(){
		$date = date('Y_m');   							//2017_10
		$finance_month_name = 'app_finance_'.$date;
		//获得数据库表前缀
		$prefix = Config::get('database.prefix');
		$all_databases_name = $prefix.$finance_month_name;
		$exist = Db::query('show tables like '.'"'.$all_databases_name.'"');
		if(empty($exist)){
			$sql =
			"CREATE TABLE `$all_databases_name` (
			`finance_id` int(32) NOT NULL AUTO_INCREMENT COMMENT '财务日志表',
			`member_id` int(32) NOT NULL COMMENT '用户id',
			`finance_type` tinyint(4) NOT NULL COMMENT '类型',
			`content` text NOT NULL COMMENT '内容',
			`money_type` tinyint(4) NOT NULL COMMENT '收入=1/支出=2',
			`money` decimal(10,2) NOT NULL COMMENT '金额',
			`add_time` int(10) NOT NULL COMMENT '添加时间',
			`account_type` int(10) NOT NULL COMMENT '币种',
			`ip` varchar(64) NOT NULL,
			`balance` decimal(10,4) NOT NULL COMMENT '余额',
			`belong` varchar(32) NOT NULL COMMENT '所属',
			PRIMARY KEY (`finance_id`)
			) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
			Db::execute($sql);
		}
		return $finance_month_name;
	}
	
	
	/**
	 * 日志添加方法
	 * @param unknown $member_id 		用户id
	 * @param unknown $finance_type 	日志类型id
	 * @param unknown $content			日志内容
	 * @param unknown $money_type 		收入/支出  收入=1/支出=2
	 * @param unknown $money			金额   
	 * @param unknown $account_type		积分种类   单积分（1）
	 * @return multitype:number string
	 */
	public function addFinance($member_id,$finance_type,$content,$money_type,$money,$account_type){
		
		$config = $this->configApp();//获取配置项
		
// 		$db = $this->finance_month_name;
		$data['member_id'] = $member_id;
		$data['finance_type'] = $finance_type;
		$data['money_type'] = $money_type;
		$data['add_time'] = time();
		$data['ip'] = request()->ip();
// 		$data['belong'] = $db;
		$data['content'] = $content;
		$data['money'] = $money;
		$data['account_type'] = $account_type;
		
		if($account_type){
			$data['balance'] = $this->getBlanceByMemberIdAndAccountType($member_id,$account_type);
			
		}
		
		$res = Db::name('App_finance')->insert($data);
		
		return $res;
	}	
	/**
	 * 获得账户余额（根据用户id和积分类别）
	 * @param unknown $member_id  用户id
	 * @param unknown $account_type  积分id
	 */
	public function getBlanceByMemberIdAndAccountType($member_id,$account_type){
		$member_account = Db::name('App_member_account')->field('account_type_'.$account_type)->where(array('member_id'=>$member_id))->find();
		return $member_account['account_type_'.$account_type];
	}
	/**
	 * 格式化时间
	 * @param unknown $date
	 */
	public function formatDate($date){
		//判断传过来的条件是不是数组
		if(is_array($date)){
			//开始时间
			$arr_start = explode("-",$date[0]);
			//结束时间
			$arr_end = explode("-",$date[1]);
			//判断是否属于同一年
			if($arr_start[0] == $arr_end[0]){
				//相差几个月(同一年)
				$j = $arr_end[1] - $arr_start[1];
			}else{
				//相差几个月（不同年）
				$j = $arr_end[1] - $arr_start[1] + 12;
			}
			//起始 月份时间戳
			$finance_month_name[0] = strtotime($arr_start[0].'-'.$arr_start[1]);
			for($i = 1;$i < $j+1;$i++){
				// 时间戳加一个月
				$finance_month_name[$i] = strtotime("+1 Month",$finance_month_name[$i-1]);
			}
			foreach($finance_month_name as $k=>$v){
				//转换成时间格式   2017_10  拼接字符串
				$finance_month_name[$k] = 'app_finance_'.date("Y_m",$finance_month_name[$k]);
			}
		}else{
			//2017-10
			$arr = explode("-",$date);
			//2017_10
			$new = implode("_", $arr);
			$finance_month_name = 'app_finance_'.$new;
		}
		return $finance_month_name;
	}
	/**
	 * 获得配置项
	 * @return multitype:
	 */
	public function configApp(){
		//获取配置信息
		$config = Db::name('App_config')->select();
		foreach($config as $k=>$v){
			$list[$v['key']]=$v['value'];
		}
		return $list;
	}
	
	
	
	
}