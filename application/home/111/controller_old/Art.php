<?php
namespace app\wallet\controller;
use think\Controller;
use think\Request;
use think\Db;
// require_once (APP_PATH . 'api/SmsApi.php');
class Art extends Base
{
	/**
	 * 获取公告列表信息
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
   public function getNoticeList(){
   		$where['type'] = 1;
   		$where['article_id'] = array('neq',333);
	   	//获取信息
	   	 $list = M('App_article')->where($where)->order('sort desc')->select();
	   	 //判断存在传值
	   	 if(empty($list)){
	   	 	//不存在返回空
	   	 	$data['data'] = array('article'=>'');
			$data['code']= -1;
			$data['msg']= '暂无相关数据';
			return json($data);
	   	 }else{
	   	 	//存在返回数据
	   	 	$data['data'] = array('article'=>$list);
	   	 	$data['code']= 1;
	   	 	$data['msg']= '成功';
	   	 	return json($data);
	   	 }
   }
   /**
    * 获取新闻列表信息
    * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
    */
   public function getNewList(){
	   	$where['type'] = 2;
	   	$where['article_id'] = array('neq',333);
	   	$list = M('App_article')->where($where)->order('sort desc')->select();
	   	
	   	foreach($list as $k=>$v){
	   		//$v['content'] = mb_convert_encoding($v['content'], 'UTF-8', 'GB2312');
	   		preg_match_all('/[\x{4e00}-\x{9fff}]+/u', $v['content'], $matches);
	   		$v['content'] = join('', $matches[0]);
	   		$str = mb_convert_encoding($v['content'], 'GB2312', 'UTF-8');
	   		$list[$k]['title']=mb_substr((strip_tags(html_entity_decode($v['title']))),0,10,'utf-8');
	   		$list[$k]['content']=mb_substr((strip_tags(html_entity_decode(htmlspecialchars($v['content'])))),0,20,'utf-8');
	   	}
	   	
	   	//判断存在传值
	   	if(empty($list)){
	   		//不存在返回空
	   		$data['data'] = array('article'=>'');
	   		$data['code']= -1;
	   		$data['msg']= '暂无相关数据';
	   		return json($data);
	   	}else{
	   		//存在返回数据
	   		$data['data'] = array('article'=>$list);
	   		$data['code']= 1;
	   		$data['msg']= '成功';
	   		return json($data);
	   	}
   }
   
   /**
    * 文章详细信息
    * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
    */
   public function getDetails(){
   		$article_id = input('article_id');
   		$url = 'http://'.$_SERVER['SERVER_NAME'].'/wallet/Art/details/article_id/'.$article_id;
   		//判断文章id是否存在
   		if(!empty($article_id)){
   			//存在返回数据
   			$data['data'] = array('url'=>$url);
   			$data['code']= 1;
   			$data['msg']= '成功';
   			return json($data);
   		}else{
   			//不存在返回空
   			$data['data'] = array('url'=>'');
   			$data['code']= -1;
   			$data['msg']= '参数错误';
   			return json($data);
   		}
   }
   
   
   public function details(){
   		$article_id = input('article_id');
   		if(!empty($article_id)){
   			db('App_article')->where(array('article_id'=>$article_id))->setInc('num',1);
   			$list = db('App_article')->where(array('article_id'=>$article_id))->find();
   		}
   		$this->assign('list',$list);
   		return $this->fetch();
   		
   }
   /**
    * 关于我们
    */
   public function getAbout(){
   		$id = 333;
   		$list = Db::name('App_article')->where(array('article_id'=>$id))->find();
   		if($list){
   			$data['data'] = array('list'=>$list);
   			$data['code']= 1;
   			$data['msg']= '成功';
   			return json($data);
   		}else{
   			$data['data'] = array('list'=>array());
   			$data['code']= -1;
   			$data['msg']= '失败';
   			return json($data);
   			
   		}
   }
   /**
    * 联系我们
    */
   public function getConnect(){
   		$config = $this->getConfig();
   		//公众号
   		$arr['accounts'] = $config['accounts'];
   		//公司名
   		$arr['company'] = $config['gname'];
   		//版权所有
   		$arr['copyright'] = $config['copyright'];
   		$data['data'] = array('arr'=>$arr);
   		$data['code']= 1;
   		$data['msg']= '成功';
   		return json($data);
   		
   } 
   
   
   
   
}