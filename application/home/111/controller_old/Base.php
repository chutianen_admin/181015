<?php

namespace app\wallet\controller;
use think\Db;
use think\Controller;
use think\Request;
require_once (APP_PATH . 'api/SmsApi.php');
require_once (APP_PATH .'api/FinanceApi.php');
class Base extends Controller {
	static $member = '';
	static $sys = '';
	public function _initialize() {
		header("Content-type: text/html; charset=utf-8");
		parent::_initialize();
		if(empty($_SESSION['USER_KEY_ID'])){
			$this->redirect("Login/index");
		}
		$this->member = M('Member')->where('member_id ='.$_SESSION['USER_KEY_ID'])->find();
		$this->assign('member',$this->member);
	}
	/**
	 * 发送短信
	 */
	public function smsSend(){
		$phone = input('phone');
		$A_Sms = new \SmsApi();
		$res = $A_Sms->send($phone);
		return json (['data'=>'','code'=>$res['status'],'msg'=>$res['info']]);
	}
	/**
	 * 获取配置项
	 * 
	 * @return 配置项
	 */
	public function getConfig() {
		$config = db ( 'app_config' )->select ();
		foreach ( $config as $k => $v ) {
			$list [$v ['key']] = $v ['value'];
		}
		return $list;
	}
	/**
	 * 获取验证吗时间
	 */
	public function getCodeTime(){
		$phone = input('phone');
		$A_Sms = new \SmsApi();
		$re = $A_Sms ->getCodeTime($phone);
		$time = $A_Sms->time;
		if(!$re){
			return json(['code'=>1,'msg'=>'没有发送过','time'=>0]);
		}
		$name = 'time_quyum_'.$phone;
		$new_time = $re->$name;
		$a = time() -$new_time;
		if($a > $time){
			return json(['code'=>1,'msg'=>'没有发送过','time'=>0]);
		}
		return json(['code'=>1,'msg'=>'没有发送过','time'=>$time-$a]);		
	}
	
	
	
	
	
	/**
	 * 获得推荐人
	 * @param unknown $id
	 * @param unknown $lv
	 * @param number $k
	 * @return unknown
	 */
	public function getUpUserByTuiJianId($id,$lv,$k=0){
		$member_relation = $this->getRelation($id);
		$k++;
		if($member_relation){
			$arr[] = $member_relation;
			if($k < $lv){
				if($member_relation['zhuceid']){
					$arr[] = $this->getRelation($member_relation['zhuceid']);
				}
			}
		}
		return $arr;
	}
	public function getRelation($member_id){
		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->find();
		return $member_relation;
	}
	
}