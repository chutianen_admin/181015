<?php
/**
 * 	app 日志
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-18
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
use think\Request;
class Fanli extends Base {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    
    /**
     * 返利记录
     */
    public function getFanLi(){
    	//用户id
    	$member_id = Base::$member['user_id'];
    	$point  = input('date');//年月
    	$times[] = strtotime(input('start'));//开始时间
    	$times[] = strtotime(input('end'));//结束时间
    	if($point){
    		$arr[] = strtotime($point);//指定月份月初时间戳
    		$arr[] = mktime(23, 59, 59, date('m', strtotime($point))+1, 00);
    		$where['add_time'] = array('between',$arr);
    	}else{
    		if(input('start') && input('end')){
    			//时间段where查询条件
    			$where['add_time'] = array('between',$times);
    		}
    	}
    	$where['member_id'] = $member_id;
    	$where['status'] = input('status');//1已完成  0未完成
    	$array = Db::name('App_fanli')->where($where)->order('add_time desc')->select();
    	foreach($array as $k=>$v){
    		$row = db('Goods')->where(array('goodsId'=>$array[$k]['goods_id']))->find();
    		$array[$k]['account'] = "购买".$row['goodsName']."赠送积分";
    	}
    	//调用根据相同日期进行分组统计
    	$list = $this->groupVisit($array);
    	if($list){
    		$data['data'] = $list;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array();
    		$data['code']= -1;
    		$data['msg']= '无数据';
    		return json($data);
    	}
    }
    /**
     * 获得返利记录详情
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function getFanLiDetial(){
    	$id = input('fanli_id');
    	$list = Db::name('App_fanli')->where(array('id'=>$id))->find();
    	if($list){
    		$data['data'] = $list;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array();
    		$data['code']= -1;
    		$data['msg']= '无数据';
    		return json($data);
    	}
    }
    /**
     *	详情页面获得每天详细的返利记录
     */
   	public function getEveryDayFanLi(){
   		$fanli_id = input('fanli_id');
   		$id = input('id');
   		if($id){
   			$where['id'] = array('lt',$id);
   			$where['fanli_id'] = $fanli_id;
   			$list = Db::name('App_fanli_finance')->where($where)->order('id desc')->limit(30)->select();
   		}else{
   			//第一次访问展示10条数据
   			$list = Db::name('App_fanli_finance')->where(array('fanli_id'=>$fanli_id))->order('id desc')->limit(30)->select();
   		}
   		if($list){
   			$data['data'] = $list;
   			$data['code']= 1;
   			$data['msg']= '成功';
   			return json($data);
   		}else{
   			$data['data'] = array();
   			$data['code']= -1;
   			$data['msg']= '无数据';
   			return json($data);
   		}
   	}
 	/**
 	 * 
 	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
 	 */
   	public function getCount(){
   		$member_id = Base::$member['user_id'];
   		$member = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->find();
   		if(empty($member)){
   			$data['data'] = array();
   			$data['code']= -1;
   			$data['msg']= '无效token';
   			return json($data);
   		}
   		//购买返利商品数量
   		$list = db('App_fanli')->where(array('member_id'=>$member_id))->select();
   		
   		
   		foreach($list as $k=>$v){
   			//返利总额
   			$arr['all_jifen'] += $list[$k]['all_jifen'];
   			//返利未到账
   			$arr['remail_jifen'] += $list[$k]['remain_jifen'];
   		}
   		//返利已到账
   		$arr['get_jifen'] = $arr['all_jifen']-$arr['remail_jifen']; 
   		$arr['count'] = count($list);
   		$data['data'] = $arr;
   		$data['code']= 1;
   		$data['msg']= '成功';
   		return json($data);
   	}
   	
   	/**
   	 * 2017-11-08 9:38
   	 * 李宏宇
   	 * 根据相同时期进行分组统计
   	 */
   	public function groupVisit($visit){
   		//$curyear = date('Y');
   		$visit_list = [];
   		foreach ($visit as $k=>$v) {
   			$date = date('Y年m月d日', $v['add_time']);
   			$visit_list[$k]['date']=$date;
   			$arr[$k][] = $v;
   			$visit_list[$k]['list']=$arr[$k];
   		}
   		return $visit_list;
   	}
    
}