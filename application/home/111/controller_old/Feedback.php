<?php
namespace app\wallet\controller;
use think\Request;
use think\Db;
// require_once (APP_PATH . 'api/SmsApi.php');
class Feedback extends Base
{
	
	/**
	 * 添加问题反馈
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
   public function getFeedbackAdd(){
   		$user_id = Base::$member['user_id'];
   		$insert['name'] = input('name');//获取姓名
   		$insert['sex'] = input('sex');//获取性别
   		$insert['phone'] = input('phone');//获取电话
   	 	$insert['content'] = input('content');//获取内容
   	 	$insert['add_time'] = time();//添加时间
   	 	$insert['member_id'] = $user_id;//用户id
   	 	$insert['status'] = 1;//用户id
		if(!empty($user_id)){
			$list = db('App_feedback')->insert($insert);
		}
   	 	if($list){
   	 		$data['data'] = array('user_id'=>'');
	   		$data['code']= 1;
	   		$data['msg']= '成功';
	   		return json($data);
   	 	}else{
   	 		$data['data'] = array('user_id'=>'');
   	 		$data['code']= -1;
   	 		$data['msg']= '失败';
   	 		return json($data);
   	 	}
   }
   
   
   /**
    * 返回用户已提交的反馈信息
    * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
    */
   public function getListFeedback(){
   		$user_id = Base::$member['user_id'];
   		$where['member_id'] = $user_id;
   		$list = db('App_feedback')->where($where)->select();
   		if($list){
   			$data['data'] = array('user_id'=>$user_id,'content'=>$list);
   			$data['code']= 1;
   			$data['msg']= '成功';
   			return json($data);
   		}else{
   			$data['data'] = array('user_id'=>'','content'=>array());
   			$data['code']= -1;
   			$data['msg']= '失败';
   			return json($data);
   		}
   }
   
   
   
   
}