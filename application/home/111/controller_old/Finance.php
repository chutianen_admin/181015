<?php
/**
 * 	app 日志
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-18
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
use think\Request;
class Finance extends Base {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    /**
     * 财务日志分类
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function getType(){
    	$where['status'] = 1; 
    	$finance_type = Db::name('App_finance_type')->where($where)->select();
    	if($finance_type){
    		$data['data'] = array('type'=>$finance_type);;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array('type'=>$finance_type);;
    		$data['code']= -1;
    		$data['msg']= '失败';
    		return json($data);
    	}
    }

    public function aa(){
//     	$a1=array("red","green");
//     	$a2=array("blue","yellow");
//     	print_r(array_merge($a1,$a2));
		$aa = $this->getFinance();
    }
    /**
     * 获得财务日志
     * 
     */
    public function getFinance(){
    	//用户id
    	$user_id = Base::$member['user_id'];
    	//筛选
    	$type = input('type');//类型
    	$point  = input('date');//年月
    	$times[] = input('start');//开始时间
    	$times[] = input('end');//结束时间
    	//默认查询本月账单
    	$finance = new \FinanceApi();
    	//判断使用表
    	if($point){
    		//获得时间点需要使用的数据表
    		$finance_month_name = $finance->formatDate($point);
    	}else{
    		if(input('start') && input('end')){
    			//获得时间段需要使用的数据表
    			$finance_month_name = $finance->formatDate($times);
    			//时间段where查询条件
//     			$where['add_time'] = array('between',$times);
    		}else{
    			//获得当前月份需要调用的数据表
    			$finance_month_name = $finance->judgeNowMonthFinance();
    		}
    	}
    	//前台不展示管理员操作
    	$finance_type = Db::name('App_finance_type')->where(array('status'=>1))->select();
    	foreach ($finance_type as $k=>$v){
    		$arr[$k] = $v['id'];
    	}
    	if($type){
    		//日志类型
    		$where['finance_type'] = $type;
    	}else{
    		$where['finance_type'] = array('in',$arr);
    	}
    	$where['member_id'] = $user_id;
    	
    	if(is_array($finance_month_name)){
    		//循环查询日志表内容
    		for($i=0;$i<count($finance_month_name);$i++){
    			$arr[$i] = Db::name($finance_month_name[$i])->where($where)->order('add_time desc')->select();
    		}
    		//转换成同同一个数组中
    		foreach($arr as $k=>$v){
    			for($m=0;$m<count($v);$m++){
    				$list[] = $v[$m];
    			}
    		}
    	}else{
    		$list = Db::name($finance_month_name)->where($where)->order('add_time desc')->select();
    	}
    	foreach($list as $m=>$n){
    		$now_type = Db::name('App_finance_type')->where(array('id'=>$list[$m]['finance_type']))->find();
    		$list[$m]['pic'] = $now_type['pic'];
    	}
    	if($list){
    		$data['data'] = $list;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array();
    		$data['code']= -1;
    		$data['msg']= '无数据';
    		return json($data);
    	}
    }
 	/**
 	 * 日志详情
 	 */
    public function getDetials(){
    	$finance_id = input('finance_id');
    	$belong = input('belong');
    	$db = $belong;
    	$finance = Db::name($db)->where(array('finance_id'=>$finance_id))->find();
    	$type = Db::name('App_finance_type')->where(array('id'=>$finance['finance_type']))->find();
    	$finance['type_name'] = $type['name'];
    	$account_type = Db::name('App_account_type')->where(array('id'=>$finance['account_type']))->find();
    	$finance['account_type_name'] = $account_type['name'];
    	
    	if($finance){
    		$data['data'] = array('finance'=>$finance);
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array('finance'=>'');
    		$data['code']= -1;
    		$data['msg']= '失败';
    		return json($data);
    	}	
    }
    /**
     * 获得手机充值日志
     */
    public function getPhonePayFinance(){
    	$member_id = Base::$member['user_id'];
    	$member = Db::name('App_member_relation')->where(array('member_id'=>$member_id))->find();
    	if(empty($member)){
    		$data['data'] = array();
    		$data['code']= -1;
    		$data['msg']= '无效token';
    		return json($data);
    	}
    	$where['member_id'] = $member_id;
    	$where['finance_type'] = '11';
    	$times[] = '2017-10';
    	$times[] = date('Y-m');
    	$finance = new \FinanceApi();
    	$finance_month_name = $finance->formatDate($times);
    	for($i=0;$i<count($finance_month_name);$i++){
    		$arr[$i] = Db::name($finance_month_name[$i])->where($where)->order('add_time desc')->select();
    	}
    	//转换成同同一个数组中
    	foreach($arr as $k=>$v){
    		for($m=0;$m<count($v);$m++){
    			$list[] = $v[$m];
    		}
    	}
    	if($list){
    		$data['data'] = $list;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array();
    		$data['code']= -1;
    		$data['msg']= '暂无内容';
    		return json($data);
    	}
    }
 
    
}