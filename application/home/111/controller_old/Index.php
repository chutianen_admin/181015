<?php
/**
 * 	app首页
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
class Index extends Base {
	  public function _initialize() {
    	parent::_initialize();
	  }
	/**
	 * app首页
	 * @return Ambigous <\think\mixed, string>
	 */
    public function index(){
    	
    	
        return $this->fetch();
    }

    /**
     * 首页轮播
     */
	public function flash(){
		$list = M('app_flash')->limit(5)->select();	
		$where['top']=1;
		$where['type']=1;
		$res = M('App_article')->where($where)->order('sort desc')->find();
		$notice = $res['title'];
		$url ='http://'.$_SERVER['SERVER_NAME'].'/wallet/Art/details/article_id/'.$res['article_id'];
		$advertising = db('App_advertising')->where(array('status'=>1))->order('sort desc')->limit(3)->select();
		
		if(empty($list)){
			$list='';
		}
		
		if(empty($notice)){
			$notice='';
		}
		
		if(empty($advertising)){
			$advertising='';
		}
		
		if($list ||$notice || $advertising){
			$data['data'] = $list;
			$data['notice']=$notice;
			$data['url']=$url;
			$data['advertising']=$advertising;
			$data['code']= 1;
			$data['msg']= '成功';
			return json($data);
		}else{
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['notice']='';
			$data['url']='';
			$data['advertising']='';
			$data['code']= -1;
			$data['msg']= '失败';
			return json($data);
		}	
	}
    
    public function getDetectionSave(){
    	$list['version'] = Base::$sys['version']?Base::$sys['version']:'';
    	$list['version_name'] = Base::$sys['version_name']?Base::$sys['version_name']:'';
    	$list['renewal_text'] = Base::$sys['renewal_text']?Base::$sys['renewal_text']:'';
    	$list['download_url'] = Base::$sys['download_url']?Base::$sys['download_url']:'';
    	if($list){
    		$data['data'] = $list;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = $list;
    		$data['code']= -1;
    		$data['msg']= '失败';
    		return json($data);
    	}
    }
    
    /**
     * 商城开关   1开  0关
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function shopStatus(){
    	$config = $this->getConfig();
    	
    	$data['data'] = $config['shop_kaiguan'];
    	$data['code']= 1;
    	$data['msg']= '成功';
    	return json($data);
    }
    
    /**
     * 手机充值开关   1开  0关
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function phonePayStatus(){
    	$config = $this->getConfig();
    	$data['data'] = $config['phone_pay_kaiguan'];
    	$data['code']= 1;
    	$data['msg']= '成功';
    	return json($data);
    }
    
    
    
    
    
    
    
       
}