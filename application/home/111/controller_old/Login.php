<?php
namespace app\wallet\controller;
use think\Controller;
use think\Request;
use think\Db;
// require_once (APP_PATH . 'api/SmsApi.php');
class Login extends Base
{
	
	
	
    /*
     * 登录(手机号+密码)
     */
    public function phonePassLogin(){
//     	$data['data'] = array('student_id'=>0,'token'=>'','name'=>'');
//     	$data['code']= -1;
//     	$data['msg']= '系统升级中请耐心等待';
//     	return json($data);
    	
    	
    	//获取数据
       	$phone = input('phone');
       	$pass = input('pass');	
       	$member = M('Users')->where(array('loginName'=>$phone))->find();
       	//禁用用户不能登录
       	if($member['userStatus']==2){
       		$data['data'] = array('student_id'=>0,'token'=>'','name'=>'');
       		$data['code']= -8;
       		$data['msg']= '该用户被禁用';
       		return json($data);
       	}
		$pass = md5($pass);
		//用户名错误	//比对密码
		
		if(!$member){
			$data['data'] = array('student_id'=>0,'token'=>'','name'=>'');
			$data['code']= -2;
			$data['msg']= '用户名不存在';
			return json($data);
		}
		
		if($pass != $member['loginPwd']){
			$data['data'] = array('student_id'=>0,'token'=>'','name'=>'');
			$data['code']= -1;
			$data['msg']= '密码不正确';
			return json($data);
		}
// 		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$member['userId']))->find();
// 		if($member_relation['status'] == 0){
// 			$data['data'] = array('student_id'=>0,'token'=>'','name'=>'');
// 			$data['code']= -9;
// 			$data['msg']= '账号已冻结';
// 			return json($data);
// 		}
		//获取token
		$token  = $this ->getToKen($member['userId']);
		$tk['token'] = $token;
		//$tk['uid'] = $member['userId'];
		db('token')->where(['uid'=>$member['userId']])->update($tk);
		//获取登录用户的昵称
		$account = M('App_member_relation')->where(array('member_id'=>$member['userId']))->find();
		if(empty($account)){
			$account['name']='';
		}
		//返回信息
		$data['data'] = array('user_id'=>$member['userId'],'token'=>$token,'name'=>$account['name']);
		$data['code']= 1;
		$data['msg']= '登录成功';
		return json($data);
    }
	
    /**
	 * 验证码登陆
	*/
	public function codeRegister(){
		$channelid = Request::instance()->param('channelid');
		$type = Request::instance()->param('type');
		$data['data'] = array('student_id'=>0,'token'=>'');
		//手机号
		$phone = Request::instance()->param('phone');
		//验证码
		$code = Request::instance()->param('code');
		//验证信息
		if(empty($phone)){
			$data['code'] = -1;
			$data['msg'] = '手机号不能为空';
			return json ($data);
		}
		//验证验证码
		$A_Sms= new \SmsApi();
		$verifyPhone = $A_Sms ->checkAppCode($phone, $code);
		if(!$verifyPhone){
			$data['code'] = -2;
			$data['msg'] = '手机验证码输入有误';
			//return json ($data);
		}
		$M_Student = new StudentModel();
		$student = $M_Student->getStudentOneByField('phone',$phone);
		$funame = 'sd_'.rand(1000,9999);
		if(!$student){
			//获取级别
			$M_Level = new StudentLevelModel();
			$level = $M_Level ->order('id')->find();
			$res = $M_Student ->insertStudent($phone, '', $level['id'], '',$funame, '', '', '', '');
			if(!$res){
				$data['code'] = -2;
				$data['msg'] = '登陆失败';
				return json ($data);
			}
			$student = $M_Student->getStudentOneByField('phone',$phone);
		}
		$token = md5(uniqid());
		$M_Student ->updata((int)$student['student_id'],['token'=>$token,'ispush'=>1,'channelid'=>$channelid,'device'=>$type]);
		//删除验证码
		$A_Sms ->deleteSendSmsApp($phone);
		$data['data'] = array('student_id'=>$student['student_id'],'token'=>$token);
		$data['code'] = 1;
		$data['msg'] = '登陆成功';
		return json ($data);
	}
    /**
     * 获取token
     * @param int $member_id	会员id
     * @return string token		
     */
    private  function getToKen($member_id){
    	return md5($member_id.time());
    } 
   /**
    * 退出
    * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
    */
    public function signOut(){
    	$user_id = Base::$member['user_id'];
    	$token = Db::name('Token')->where(array('uid'=>$user_id))->find();
    	if($token['token']){
    		$r = Db::name('Token')->where(array('uid'=>$user_id))->setField('token','');
    		if(empty($r)){
    			return json(['data'=>'','code'=>-2,'msg'=>'操作失败']);
    		}
    	}
    	return json(['data'=>'','code'=>1,'msg'=>'操作完成']);
    }
   /**
	*	h5注册页面
	*/
   public function login(){
   		
	   $user_id = input('user_id');
	   $name = input('name');
	   $user = Db::name('Users')->where(array('userId'=>$user_id))->find();
	   $this->assign('user_id',$user_id);
	   $this->assign('name',$user['loginName']);
	   return $this->fetch();
   }
   
   public function ewm(){

   	return $this->fetch();
   }
   
}