<?php
/**
 * 	app 会员Api接口
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
use think\Request;
require_once (APP_PATH . 'api/SmsApi.php');
class Member extends Base {
    public function _initialize(){
        parent::_initialize();
       
    }
    /**
     * 获得用户账户余额信息
     * @param unknown $user_id
     */
    public function getMemberAccountByUserId(){
    	$user_id = Base::$member['user_id'];
    	$type = input('type')?input('type'):0;
    	$account_type = input('type')?'account_type_'.input('type'):null;
    	
    	$member_account = M('app_member_account')->field($account_type)->where(array('member_id'=>$user_id))->find();
    	if($member_account){
    		$data['data'] = $member_account;
    		$data['code']= 1;
    		$data['msg']= '成功';
    		return json($data);
    	}else{
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -1;
    		$data['msg']= '失败';
    		return json($data);
    	}
    }
    /**
     * 获得用户信息
     * @param token $user_id
     * @return string
     */
	public function getMember(){
		$user_id = Base::$member['user_id'];
		$list = M('Users')->where(array('userId'=>$user_id))->find();
		if(empty($list)){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= 1;
			$data['msg']= '用户不存在';
			return json($data);
		}
		$data['data'] = $list;
		$data['code']= 1;
		$data['msg']= '成功';
		return json($data);
	}
	/**
	 * 修改二级密码
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function savePayPwd(){
		//手机号
		$phone = input('phone');
		//身份证号
		$idcard = input('idcard');
		//验证码
		$code = input('code');
		//密码
		$pass = input('paypwd');
		$token = input('token');
		//获取数据
		if(empty($phone)){
			return json (['data'=>'','code'=>-1,'msg'=>'手机号不能为空']);
		}
		if(empty($pass)){
			return json (['data'=>'','code'=>-4,'msg'=>'密码不能为空']);
		}
		if(empty($idcard)){
			return json (['data'=>'','code'=>-4,'msg'=>'身份证号不能为空']);
		}
		$A_Sms= new \SmsApi();
		$verifyPhone = $A_Sms ->checkAppCode($phone, $code);
		
		if($verifyPhone){
			$sendtime = json_decode($A_Sms ->getSms($phone));
			$name = 'time_quyum_' . $phone;
			$time = $sendtime ->$name;
			$ctime = time();
			$chatime = $ctime-$time;
			if($chatime>600){
				//删除验证码
				$A_Sms ->deleteSendSmsApp($phone);
				$array['data'] = '';
				$array['code']= -10;
				$array['msg']= '验证码失效(有效时间10分钟)';
				return json($array);
			}
		}
		
		if(!$verifyPhone){
			return json (['data'=>'','code'=>-5,'msg'=>'手机验证码输入有误']);
		}
		
		
		//判断是否有用户
		$member = M('Users')->where(array('loginName'=>$phone))->find();
		$member_relation = M('App_member_relation')->where(array('member_id'=>$member['userId']))->find();
		if(!$member){
			return json (['data'=>'','code'=>-6,'msg'=>'当前手机号未注册']);
		}
		//验证当前token是否为修改密码用户token
// 		if($member['token'] != $token){
// 			return json (['data'=>'','code'=>-7,'msg'=>'手机号和当前用户token不符']);
// 		}
		//判断预留身份证号
		if($member_relation['idcard'] != $idcard){
			return json (['data'=>'','code'=>-8,'msg'=>'输入身份证号与预留信息不符']);
		}
		if($member['loginPwd'] == md5($pass)){
			return json (['data'=>'','code'=>-9,'msg'=>'新密码和原密码一致']);
		}
		$data['payPwd'] = md5($pass);
		//修改密码
		$r = M('Users')->where(array('loginName'=>$phone))->update($data);
		if(!$r){
			return json (['data'=>'','code'=>-7,'msg'=>'修改失败']);
		}
		$A_Sms ->deleteSendSmsApp($phone);
		return json (['data'=>'','code'=>1,'msg'=>'修改成功']);	
	}
	/**
	 * 实名认证
	 */
	public function judgeAuthentication(){
		$user_id = Base::$member['user_id'];
		$member = M('Users')->where(array('userId'=>$user_id))->find();
		$member_relation = M('App_member_relation')->where(array('member_id'=>$user_id))->find();
		if(empty($member_relation['idcard'])){
			$data['data'] = array('idcard'=>'','name'=>'');
			$data['code']= -1;
			$data['msg']= '未实名认证';
			return json($data);
		}else{
			if($member_relation['status'] == 0){
				$data['data'] = array('idcard'=>'','name'=>'');
				$data['code']= -2;
				$data['msg']= '等待审核';
				return json($data);
			}
			$data['data'] = array('idcard'=>$member_relation['idcard'],'name'=>$member_relation['name']);
			$data['code']= 1;
			$data['msg']= '已实名认证';
			return json($data);
		}	
	}
	/**
	 * 实名验证上传
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function authentication(){
		$data['name'] = input('name');
		$data['idcard'] = input('idcard');
		if(empty($data['name']) || empty($data['idcard'])){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= -1;
			$data['msg']= '请补全数据';
			return json($data);
		}
		$res = $this->isCreditNo($data['idcard']);
		if(!$res){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= -2;
			$data['msg']= '请填写正确的身份证号码';
			return json($data);
		}
		$user_id = Base::$member['user_id'];
        $where['idcard']=$data['idcard'];
        $where['member_id']=array('neq',$user_id);
        $list = db('App_member_relation')->where($where)->select();
        if($list){
            $data['data'] = array('user_id'=>'','token'=>'');
            $data['code']= -2;
            $data['msg']= '身份证信息已重复！';
            return json($data);
        }
		$res = M('App_member_relation')->where(array('member_id'=>$user_id))->update($data);
		if($res){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= 1;
			$data['msg']= '成功';
			return json($data);
		}else{
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= -3;
			$data['msg']= '失败';
			return json($data);
		}
	}
	/**
	 * 判断是否头推荐人
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function judgeTuijian(){
		$user_id = Base::$member['user_id'];
		$member_relation = M('App_member_relation')->where(array('member_id'=>$user_id))->find();
		if(empty($member_relation['zhuceid'])){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= -1;
			$data['msg']= '无推荐人';
			return json($data);
		}else{
			$data['data'] = array('zhuceid'=>$member_relation['zhuceid'],'token'=>'');
			$data['code']= 1;
			$data['msg']= '有推荐人';
			return json($data);
		}
	}
	
	/**
	 * 获得推荐人信息
	 * @return Ambigous <\think\response\Json                    , \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 * 有推荐人情况下  查找推荐人信息
	 */
	public function getTuiJianDetails(){
		$user_id = Base::$member['user_id'];
		$member = M('Users')->where(array('userId'=>$user_id))->find();
		if(empty($member)){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= -2;
			$data['msg']= '无效token';
			return json($data);
		}
		$member_relation = M('App_member_relation')->where(array('member_id'=>$user_id))->find();
		if(empty($member_relation)){
			$data['data'] = array('user_id'=>'','token'=>'');
			$data['code']= -1;
			$data['msg']= 'relation无数据';
			return json($data);
		}
		$re = $member_relation = M('App_member_relation')->where(array('member_id'=>$user_id))->find();
		//dump($re);
		if(!$re['zhuceid']){
			$arr['name'] = '';
			$arr['phone'] = '';
			$data['data'] = $arr;
			$data['code']= 1;
			$data['msg']= '失败';
			return json($data);
		}
		$zhuce_relation = M('App_member_relation')->where(array('member_id'=>$member_relation['zhuceid']))->find();
		$zhuce_user = M('Users')->where(array('userId'=>$member_relation['zhuceid']))->find();
		$arr['name'] = $zhuce_relation['nick'];
		$arr['phone'] = $zhuce_user['loginName'];
		$data['data'] = $arr;
		$data['code']= 1;
		$data['msg']= '成功';
		return json($data);
	}
	/**
	 * 根据邀请码获得推推荐人信息
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function getTuiJianDetailsByTuiJianNum(){
		$user_id = Base::$member['user_id'];
		//查询自己的推荐code
		$my_member_relation = M('App_member_relation')->where(array('member_id'=>$user_id))->find();
		$code = input('code');
		if($code == $my_member_relation['code']){
			$data['data'] = array('name'=>'','phone'=>'');
			$data['code']= -1;
			$data['msg']= '请不要填写自己的推荐码';
			return json($data);
		}
		$member_relation = M('App_member_relation')->where(array('code'=>$code))->find();
		$member = M('Users')->where(array('userId'=>$member_relation['member_id']))->find();
		if($member_relation){
			$data['data'] = array('name'=>$member_relation['nick'],'phone'=>$member['loginName']);
			$data['code']= 1;
			$data['msg']= '成功';
			return json($data);
		}else{
			$data['data'] = array('name'=>'','phone'=>'');
			$data['code']= -2;
			$data['msg']= '无数据';
			return json($data);
		}
	}
	
	
	
	
	
	
	
	
	/**
	 * 修改头像,修改昵称,修改姓名,修改手机号
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function editMember(){
		$M_Student = New StudentModel();
		$student_id = input('student_id');
		if(empty($student_id)){
			return json(['data'=>'','code'=>-1,'msg'=>'学生ID不能为空']);
		}
		//type  1 修改头像    2修改昵称    3修改姓名    4修改手机号	5修改密码
		$type = input('type/d');
		if(empty($type)){
			return json(['data'=>'','code'=>-2,'msg'=>'状态不能为空']);
		}
		if($type == 1){
			$file = input( 'header_img' );
			if(empty($file)){
				return json(['data'=>'','code'=>-3,'msg'=>'上传头像不能为空']);
			}
			$info = $file->move ( ROOT_PATH . 'public' . DS . 'uploads' );
			$header_img = '/uploads/' . str_replace('\\', '/', $info->getsaveName ());
			$data['pic'] = $header_img;
		}
		if($type == 2){
			$data['full_name'] = input('full_name');
			$student = $M_Student->getStudentOne($student_id);
			if(empty($data['full_name'])){
				return json(['data'=>'','code'=>-4,'msg'=>'昵称不能为空']);
			}
			if($data['full_name'] == $student['full_name']){
				return json(['data'=>'','code'=>-18,'msg'=>'操作成功']);
			}
		}
		if($type == 3){
			$data['name'] = input('name');
			$student = $M_Student->getStudentOne($student_id);
			//   			if($student['name']){
			//   				return json(['data'=>'','code'=>-20,'msg'=>'请勿重复修改真实姓名']);
			//   			}
			if(empty($data['name'])){
				return json(['data'=>'','code'=>-5,'msg'=>'真实姓名不能为空']);
			}
			if($data['name'] == $student['name']){
				return json(['data'=>'','code'=>-19,'msg'=>'操作成功']);
			}
				
		}
		if($type == 4){
			$data['phone'] = input('phone');
			$code = input('code');
			if(empty($data['phone'])){
				return json(['data'=>'','code'=>-6,'msg'=>'手机号不能为空']);
			}
			$res = $M_Student->getStudentOneByField('phone',$data['phone']);
			if($res){
				return json(['data'=>'','code'=>-7,'msg'=>'当前手机号已注册']);
			}
			if(empty($code)){
				return json(['data'=>'','code'=>-8,'msg'=>'验证码不能为空']);
			}
			$A_Sms= new \SmsApi();
			$verifyPhone = $A_Sms ->checkAppCode($data['phone'], $code);
			if(!$verifyPhone){
				return json (['data'=>'','code'=>-9,'msg'=>'手机验证码输入有误']);
			}
			$A_Sms ->deleteSendSmsApp($data['phone']);
		}
		if($type == 5){
			//原密码
			$pass = input('pass');
			if(empty($pass)){
				return json (['data'=>'','code'=>-10,'msg'=>'原密码不能为空']);
			}
			//新密码
			$newpass = input('newpass');
			if(empty($newpass)){
				return json (['data'=>'','code'=>-11,'msg'=>'新密码不能为空']);
			}
			//再次输入密码
			$againnewpass = input('againnewpass');
			if(empty($againnewpass)){
				return json (['data'=>'','code'=>-12,'msg'=>'再次输入密码不能为空']);
			}
			$res = Db::name('Student')->where(array('student_id'=>$student_id,'pass'=>md5($pass)))->find();
			if(empty($res)){
				return json (['data'=>'','code'=>-13,'msg'=>'原密码输入不正确']);
			}
			if($pass == $newpass){
				return json (['data'=>'','code'=>-14,'msg'=>'原密码和新密码设置不能重复']);
			}
			if($newpass !== $againnewpass){
				return json (['data'=>'','code'=>-15,'msg'=>'两次密码输入不一致']);
			}
			if(!preg_match('/^[_0-9a-z]{6,16}$/i',$newpass)){
				return json (['data'=>'','code'=>-16,'msg'=>'密码位数6-16位']);
			}
			$data['pass'] = md5($newpass);
		}
		//修改操作
		$r = $M_Student->updata($student_id, $data);
		if(empty($r)){
			return json(['data'=>'','code'=>-17,'msg'=>'操作失败']);
		}
		return json(['data'=>'','code'=>1,'msg'=>'操作成功']);
	}

  
     /*
    	个人资料
    	参数 token string 用户token;
    	参数 head_pic file 头像; 存 users 存relation
    	参数 nick string 昵称;	存 relation
    	参数 sex int 性别		存 relation
    	参数 mobile string 手机号  存users
     */ 
   	public function addUser(){
    	//头像
    	$head_pic = $_FILES["head_pic"]["tmp_name"];
		 if($head_pic){
// 			$info = $head_pic->move ( ROOT_PATH . 'public' . DS . 'pic' );
// 			//头像
// 			$pic = '/pic/' . str_replace('\\', '/', $info->getsaveName ());
    		$file = request()->file('head_pic');
			$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
			$pic = '/public/uploads/' . str_replace('\\', '/', $info->getsaveName ());
		}

    	$nick = input('nick');
    	//判断是否有昵称
    	if(empty($nick)){
    		return json(['data'=>'','code'=>-2,'msg'=>'没有昵称']);
    	}
    	$sex = input('sex');
    	//判断是否有性别
    	if(is_null($sex)){
    		return json(['data'=>'','code'=>-3,'msg'=>'没有性别']);
    	}
//     	$mobile = input('mobile');
//     	//判断是否有手机号
//     	if(empty($mobile)){
//     		return json(['data'=>'','code'=>-4,'msg'=>'没有手机号']);
//     	}
    	//获取用户id
    	$user_id = Base::$member['user_id'];
    	$list = db('users')->where(['userId'=>$user_id])->find();
    	if(!$list){
    		return json(['data'=>'','code'=>-5,'msg'=>'用户不存在']);
    	}
    	//增加relation表
    	$data['nick']=$nick;
    	$data['sex']=$sex;
    	$data['pic']=$pic;
    	$res = db('app_member_relation')->where(['member_id'=>$list['userId']])->update($data);
    	//增加users表
    	//$data1['mobile']=$mobile;
    	$data1['userPhoto']=$pic;
    	$res1 = db('users')->where(['userId'=>$list['userId']])->update($data1);
    	if($res || $res1){
    		return json(['data'=>'','code'=>1,'msg'=>'成功']);
    	}else{
    		return json(['data'=>'','code'=>-6,'msg'=>'失败']);
    	}

   	}
   	/*
    	个人头像修改
    	参数 token string 用户token;
    	参数 head_pic file 头像; 存 users 存relation
     */ 
    public function editPhoto(){
    	$user_id = Base::$member['user_id'];
    	$list = db('users')->where(['userId'=>$user_id])->find();
    	if(!$list){
    		return json(['data'=>'','code'=>-1,'msg'=>'用户不存在']);
    	}
    	//头像
//     	$head_pic = input('head_pic');
// 		if(empty($head_pic)){
// 			return json(['data'=>'','code'=>-3,'msg'=>'头像不能为空']);
// 		}else{
// 			$info = $head_pic->move ( ROOT_PATH . 'public' . DS . 'uploads' );
// 			//头像
// 			$pic = '/uploads/' . str_replace('\\', '/', $info->getsaveName ());
// 		}
    	$head_pic = $_FILES["head_pic"]["tmp_name"];
    	if($head_pic){
    		$file = request()->file('head_pic');
    		$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
    		$pic = '/public/uploads/' . str_replace('\\', '/', $info->getsaveName ());
    		
    	}
    	$data['pic']=$pic;
		//修改relation表
		
		$res = db('app_member_relation')->where(['member_id'=>$list['userId']])->update($data);
		//修改users表
		$data1['userPhone']=$pic;
    	$res1 = db('users')->where(['userId'=>$list['userId']])->update($data1);
    	if($res || $res1){
    		return json(['data'=>'','code'=>1,'msg'=>'成功']);
    	}else{
    		return json(['data'=>'','code'=>-2,'msg'=>'失败']);
    	}

    }
    
    
    
    /*
    	绑定银行卡
    	参数 token string 用户token;
    	参数 head_pic file 头像; 存 users 存relation
    	刘轩林  
     */ 
    public function bandCard(){
    	
    	//验证手机号验证码
    	$A_Sms= new \SmsApi();
		$verifyPhone = $A_Sms ->checkAppCode($phone, $code);
		if(!$verifyPhone){
			return json (['data'=>'','code'=>-5,'msg'=>'手机验证码输入有误']);
		}

    }

    /*
    	个人资料展示
    	参数 token string 用户token;
     */ 
    public function userList(){
    	$user_id = Base::$member['user_id'];
    	
    	$list = db('users')->where(['userId'=>$user_id])->find();
    	
    	if(empty($list)){
    		$data =[
				'pic'=>'',
				'nick'=>'',
				'sex'=>'',
				'mobile'=>'',
				'code'=>'',
				'tuijian_name'=>'',
				'tuijian_phone'=>''
    		];
    		return json(['data'=>$data,'code'=>-1,'msg'=>'用户不存在']);
    	}
    	$arr = db('app_member_relation')->where(['member_id'=>$list['userId']])->find();
    	//头像
    	if(!empty($arr['pic'])){
    		$data['pic'] = $arr['pic'];
    	}else{
    		$data['pic']='';
    	}
    	//昵称
    	if(!empty($arr['nick'])){
    		$data['nick'] = $arr['nick'];
    	}else{
    		$data['nick']='';
    	}
    	//性别
    	if($arr['sex']==1){
    		$data['sex'] ='男';
    	}elseif($arr['sex']==0){
    		$data['sex']='女';
    	}else{
    		$data['sex']='';
    	}
    	//手机号
    	if(!empty($list['loginName'])){
    		$data['mobile'] = $list['loginName'];
    	}else{
    		$data['mobile']='';
    	}
    	
    	//推荐码
    	if(!empty($arr['code']) && !empty($arr['zhuceid'])){
    		$data['code'] = $arr['code'];
    	}else{
    		$data['code'] = '';
    	}
    	
    	//姓名
    	if(!empty($arr['name'])){
    		$data['name'] = $arr['name'];
    	}else{
    		$data['name'] = '';
    	}
    	
    	//身份证号
    	if(!empty($arr['idcard'])){
    		$data['idcard'] = $arr['idcard'];
    	}else{
    		$data['idcard'] = '';
    	}
    	
    	
    	$zhuce_relation = M('App_member_relation')->where(array('member_id'=>$arr['zhuceid']))->find();
    	
    	$zhuce_user = M('Users')->where(array('userId'=>$arr['zhuceid']))->find();
    	
    	if($zhuce_relation['nick']){
    		$data['tuijian_name'] = $zhuce_relation['nick'];
    	}else{
    		$data['tuijian_name'] = '';
    	}
    	
    	if($zhuce_user['loginName']){
    		$data['tuijian_phone'] = $zhuce_user['loginName'];
    	}else{
    		$data['tuijian_phone'] = '';
    	}
    	
    	return json (['data'=>$data,'code'=>1,'msg'=>'成功']);
    }
    
    
	/**
	 * 添加推荐人信息
	 */
	public function addTuiJian(){
		$user_id = Base::$member['user_id'];
		$code = input('code');
		$member_relation = M('App_member_relation')->where(array('code'=>$code))->find();
		if(!$member_relation){
			$data['data'] = array('user_id'=>'','phone'=>'');
			$data['code']= -3;
			$data['msg']= '无效推荐码';
			return json($data);
		}
		$res = Db::name('App_member_relation')->where(array('code'=>$code))->setInc('zhitui_num',1);
		$list = $this->getUpUserByTuiJianId($member_relation['member_id'],9999);
		for($i=0;$i<count($list);$i++){
			if($list[$i]['member_id']){
				$res = Db::name('App_member_relation')->where(array('member_id'=>$list[$i]['member_id']))->setInc('tream_num',1);
			}
		}
		$where['zhuceid'] = $member_relation['member_id'];
		$where['code'] = $this->getTuiJianCode();
		$member = M('App_member_relation')->where(array('member_id'=>$user_id))->update($where);
		if($member){
			$data['data'] = array('user_id'=>'','phone'=>'');
			$data['code']= 1;
			$data['msg']= '成功';
			return json($data);
		}else{
			$data['data'] = array('user_id'=>'','phone'=>'');
			$data['code']= -2;
			$data['msg']= '失败';
			return json($data);
		}
	}
	/**
	 * 获得一个6位的数字字母（唯一）
	 */
	public function getTuiJianCode(){
		$str = rand('10000000','99999999');
		$member = M('App_member_relation')->where(array('code'=>$str))->find();
		if($member){
			$str = $this->getTuiJianCode();
		}
		return $str;
	}
	/**
	 * 生成二维码地址
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
   public  function evUrl(){
   		$user_id = Base::$member['user_id'];
   		
   		$re = $member_relation = M('App_member_relation')->where(array('member_id'=>$user_id))->find();
   		//dump($re);
   		if(!$re['zhuceid']){
   			$data['data'] = array('user_id'=>'','url'=>'');
   			$data['code']= 1;
   			$data['msg']= '失败';
   			return json($data);
   		}
   		$member = M('Users')->where(array('userId'=>$user_id))->find();
   		$url = 'http://'.$_SERVER['SERVER_NAME'].'/wallet/Login/login/user_id/'.$user_id.'/name/'.$member['email'];
   		//判断user_id是否存在
   		if(!empty($member)){
   			/*存在返回地址*/
   			$data['data'] = array('user_id'=>$user_id,'url'=>$url);
   			$data['code']= 1;
   			$data['msg']= '成功';
   			return json($data);
   		}else{                   
   			$data['data'] = array('user_id'=>'','phone'=>'');
   			$data['code']= -1;
   			$data['msg']= '参数错误';
   			return json($data);
   		}
   		
   }
   
   
   /**
    * 获取用户的所有火车票信息
    */
  	public function getCarOrder(){
  		$user_id = Base::$member['user_id'];//获取用户id
  		$list = db('Ticket')->where(array('uid'=>$user_id))->select();//查询用户的所有订单
  		if(!empty($list)){
  			$data['data'] = array('list'=>$list);
  			$data['code']= 1;
  			$data['msg']= '成功';
  			return json($data);
  		}else{
  			$data['data'] = array('list'=>'');
  			$data['code']= 1;
  			$data['msg']= '失败';
  			return json($data);
  		}
  		
  	}
   
    
  	public function getPlaneOrder(){
  		$user_id = Base::$member['user_id'];//获取用户id
  		$list = db('Ticket_plane')->where(array('uid'=>$user_id))->select();//查询用户的所有订单
  		if(!empty($list)){
  			$data['data'] = array('list'=>$list);
  			$data['code']= 1;
  			$data['msg']= '成功';
  			return json($data);
  		}else{
  			$data['data'] = array('list'=>'');
  			$data['code']= 1;
  			$data['msg']= '失败';
  			return json($data);
  		}
  	
  	}
    
    
}