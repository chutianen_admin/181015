<?php
/**
 * 	app后台会员控制器
 *  =============================
 * 	后台父类
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
class Payphone extends Base {
    public function _initialize(){
        parent::_initialize();
       
    }
//     array(3) {
//     	["reason"] => string(33) "订单提交成功，等待充值"
//     	["result"] => array(8) {
//     		["cardid"] => string(5) "10091"
//     		["cardnum"] => string(1) "1"
//     		["ordercash"] => float(1.06)
//     		["cardname"] => string(22) "广东移动话费1元"
//     		["sporder_id"] => string(24) "J17120115293831944924905"
//     		["uorderid"] => string(10) "Ua3qk8IUud"
//     		["game_userid"] => string(11) "13433331670"
//     		["game_state"] => string(1) "0"
//     	}
//     	["error_code"] => int(0)
//     }
    public function aaa(){
    	$phone = 18640182276;
    	$num = 1;
    	$res = $this->setPhonePrice($phone, $num);
    	$rs = json_decode($res,true);	
    	
// // 		$order_id = 'J17120115070895435682955';
//     	$order_id = Ua3qk8IUud;
//     	$res = $this->payOrder($order_id);
//     	$rs = json_decode($res,true);
    	dump($rs);
//     	die();
    	if($rs['error_code']){
    		$data['data'] = array('user_id'=>1,'token'=>'');
    		$data['status']=$rs['error_code'];
    		$data['info']=$rs['reason'];
//     		$rs = M('app_member_account')->where(array('member_id'=>$user_id))->setInc('account_type_1',$money);
    		return json($data);
    	}
    	
    	$ressulet = $this->addPhonePayOrder(2, $phone, $num, 1.02, $rs);
    	echo '成功';
    	
    }
    /**
     * 手机充值
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function judgeMoney(){
//     	$data['data'] = array('user_id'=>'','token'=>'');
//     	$data['code']= -6;
//     	$data['msg']= '该服务暂停使用';
//     	return json($data);
//     	$token = $_POST['token'];//token 值
    	$user_id = Base::$member['user_id'];
    	$phone = input('phone');
    	$num = input('num');//充值金额
    	$pawpwd = input('pawpwd');//交易密码
    	if(empty($phone)||empty($num)){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -1;
    		$data['msg']= '手机号码或金额不能为空';
    		return json($data);
    	}
    	if(empty($user_id)){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -2;
    		$data['msg']= '请先登录';
    		return json($data);
    	}
    	$member_account = M('app_member_account')->where(array('member_id'=>$user_id))->find();
    	$member = M('Users')->where(array('userId'=>$user_id))->find();
    	//交易密码
    	if(md5($pawpwd)!=$member['payPwd']){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -4;
    		$data['msg']= '交易密码错误';
    		return json($data);
    	}
    	//判断是不是为自己充值
    	$user = Db::name('Users')->where(array('userId'=>$user_id))->find();
    	if($user['loginName'] != $phone){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -7;
    		$data['msg']= '请为自己充值';
    		return json($data);
    	}
    	
    	$finance = new \FinanceApi();
    	$db = $finance->judgeNowMonthFinance();
    	//求当天的充值金额
    	$time = $this->getNowDay();
    	$map2['add_time'] = array('between',$time);
    	$map2['member_id'] = $user_id;
    	$map2['finance_type'] = 11;
    	$pay_money_day = Db::name($db)->where($map2)->sum('money');
    	$all_money_day = $num+$pay_money_day;
    	$config = $this->getConfig();
    	//如果不为空说明有限定
    	if(!empty($config['phone_pay_top_day'])){
    		//判断是否超出每日上线
    		if($all_money_day > $config['phone_pay_top_day']){
    			$data['data'] = array('user_id'=>'','token'=>'');
    			$data['code']= -5;
    			
    			$data['msg']= '超出每日限额';
    			return json($data);
    		}
    	}
    	
    
    	//判断是否超出限额
    	$map['member_id'] = $user_id;
    	$map['finance_type'] = 11;
    	$pay_money = Db::name($db)->where($map)->sum('money');
    	$all_money = $num+$pay_money;
    	if($all_money > $config['phone_pay_top']){
    		$data['data'] = array('user_id'=>'','token'=>'');
    		$data['code']= -5;
    		$data['msg']= '超出每月限额';
    		return json($data);
    	}
    	//检测手机是否能充值
    	$json = $this->phonetest($phone,$num);
    	$rs = json_decode($json,true);

    	if($rs['error_code']){
    		$data['data'] = array('user_id'=>'','token'=>'');
    		$data['code']=$rs['error_code'];
    		$data['msg']=$rs['reason'];
    		return json($data);
    	}
    	$bili = $config['phone_pay_fee']*0.01;
    	$fee = $num*$bili;
    	$money = $num+$fee;
    	//对比金额
    	if($member_account['account_type_1'] < $money){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -3;
    		$data['msg']= '您的消费积分余额不足';
    		return json($data);
    	}
    	//用户减钱
    	$rs = M('app_member_account')->where(array('member_id'=>$user_id))->setDec('account_type_1',$money);
    	if(!$rs){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['code']= -6;
    		$data['msg']= '充值失败';
    		$rs = M('app_member_account')->where(array('member_id'=>$user_id))->setInc('account_type_1',$money);
    		return json($data);
    	}
    	$res = $this->setPhonePrice($phone, $num);
    	$rs_pay = json_decode($res,true);
    	if($rs['error_code']){
    		$data['data'] = array('user_id'=>$user_id,'token'=>'');
    		$data['status']=$rs['error_code'];
    		$data['info']=$rs['reason'];
    		$rs = M('app_member_account')->where(array('member_id'=>$user_id))->setInc('account_type_1',$money);
    		return json($data);
    	}
    	$ressulet = $this->addPhonePayOrder($user_id, $phone, $num, $money, $rs_pay);
    	
    	$finance->addFinance($user_id,11,$phone."话费充值-金额:".$num,2,$num,1);
    	$finance->addFinance($user_id, 6, "手机充值手续费",2, $fee,1);
    	$data['data'] = array('user_id'=>$user_id,'token'=>'');
    	$data['code']= 1;
    	$data['msg']= '成功';
    	return json($data);
    }
    
    /**
     * 添加手机充值订单
     * @param unknown $member_id
     * @param unknown $phone
     * @param unknown $num
     * @param unknown $money
     * @param unknown $order_id
     * @return unknown
     * 
     */
    public function addPhonePayOrder($member_id,$phone,$num,$money,$order){
    	$data['member_id'] = $member_id;
    	$data['phone'] = $phone;
    	$data['num'] = $num;
    	$data['all_num'] = $money;
    	$data['add_time'] = time();
    	$data['cardnum'] = $order['result']['cardnum'];
    	$data['uorderid'] = $order['result']['uorderid'];
    	$data['game_state'] = $order['result']['game_state'];
    	$data['cardname'] = $order['result']['cardname'];
    	$data['ordercash'] = $order['result']['ordercash'];
    	$data['reason'] = $order['reason'];
    	$res = Db::name('App_phone_pay')->insert($data);
    	return $res;
    }
    /**
     * 获得订单状态
     * @param unknown $order_id
     * /*状态 1:成功 9:失败 0：充值中*
     */
    public function getOrderStatus(){
    	$id = input('id');
    	$order = Db::name('App_phone_pay')->where(array('id'=>$id))->find();
    	$order_web = $this->payOrder($order['uorderid']);
    	$order_array = json_decode($order_web,true);
    	$data['game_state'] = $order_array['game_state'];
    	$res = Db::name('App_phone_pay')->where(array('id'=>$id))->update($data);
		if($res){
			$data['data'] = array();
			$data['code']= 1;
			$data['msg']= '成功';
			return json($data);
		}else{
			$data['data'] = array();
			$data['code']= -1;
			$data['msg']= '失败';
			return json($data);
		}
    }
    
    /**
     * 获得当天时间戳
     */
    public function getNowDay(){
    	$start = strtotime(date('Y-m-d',time()));
    	$time[] = $start;
    	$time[] = $start+84600;
    	return $time;
    }
    
    
    /**
     * 检测手机号码是否可以充值
     * @param unknown $number
     * @param unknown $price
     * @return mixed
     */
    public	function phonetest($number,$price){
    	$url="http://op.juhe.cn/ofpay/mobile/telcheck?phoneno=".$number."&cardnum=".$price."&key=8f4c6d95dc57b7cb456798e4720fcbc6";
    	$curl = curl_init (); // 启动一个CURL会话
    	curl_setopt ( $curl, CURLOPT_URL, $url ); // 要访问的地址
    	curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, 0 ); // 对认证证书来源的检查
    	@curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, 1 ); // 使用自动跳转
    	curl_setopt ( $curl, CURLOPT_AUTOREFERER, 1 ); // 自动设置Referer
    	curl_setopt ( $curl, CURLOPT_TIMEOUT, 120 ); // 设置超时限制防止死循环
    	curl_setopt ( $curl, CURLOPT_HEADER, 0 ); // 显示返回的Header区域内容
    	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 ); // 获取的信息以文件流的形式返回
    	$tmpInfo = curl_exec ( $curl ); // 执行操作
    	if (curl_errno ( $curl )) {
    		echo 'Errno' . curl_error ( $curl );
    	}
    	curl_close ( $curl ); // 关键CURL会话
    	return $tmpInfo; // 返回数据
    }
    /**
     * 订单状态查询
     * @param unknown $number
     * @param unknown $price
     * @return mixed
     */
    public	function payOrder($sporder_id){
    	$url="http://op.juhe.cn/ofpay/mobile/ordersta?key=8f4c6d95dc57b7cb456798e4720fcbc6&orderid=".$sporder_id;
    	$curl = curl_init (); // 启动一个CURL会话
    	curl_setopt ( $curl, CURLOPT_URL, $url ); // 要访问的地址
    	curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, 0 ); // 对认证证书来源的检查
    	@curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, 1 ); // 使用自动跳转
    	curl_setopt ( $curl, CURLOPT_AUTOREFERER, 1 ); // 自动设置Referer
    	curl_setopt ( $curl, CURLOPT_TIMEOUT, 120 ); // 设置超时限制防止死循环
    	curl_setopt ( $curl, CURLOPT_HEADER, 0 ); // 显示返回的Header区域内容
    	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 ); // 获取的信息以文件流的形式返回
    	$tmpInfo = curl_exec ( $curl ); // 执行操作
    	if (curl_errno ( $curl )) {
    		echo 'Errno' . curl_error ( $curl );
    	}
    	curl_close ( $curl ); // 关键CURL会话
    	return $tmpInfo; // 返回数据
    }
    
    
    //手机充值方法
    public	function setPhonePrice($number,$price){
    	$num=$this->getRandomString(10);
    	$sign=md5("JH1b586144410bf84cf9427770680278df8f4c6d95dc57b7cb456798e4720fcbc6".$number.$price.$num);
    	$url="http://op.juhe.cn/ofpay/mobile/onlineorder?phoneno=".$number."&cardnum=".$price."&orderid=".$num."&sign=".$sign."&key=8f4c6d95dc57b7cb456798e4720fcbc6";
    	$curl = curl_init (); // 启动一个CURL会话
    	curl_setopt ( $curl, CURLOPT_URL, $url ); // 要访问的地址
    	curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, 0 ); // 对认证证书来源的检查
    	@curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, 1 ); // 使用自动跳转
    	curl_setopt ( $curl, CURLOPT_AUTOREFERER, 1 ); // 自动设置Referer
    	curl_setopt ( $curl, CURLOPT_TIMEOUT, 120 ); // 设置超时限制防止死循环
    	curl_setopt ( $curl, CURLOPT_HEADER, 0 ); // 显示返回的Header区域内容
    	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 ); // 获取的信息以文件流的形式返回
    	$tmpInfo = curl_exec ( $curl ); // 执行操作
    	if (curl_errno ( $curl )) {
    		echo 'Errno' . curl_error ( $curl );
    	}
    	curl_close ( $curl ); // 关键CURL会话
    	return $tmpInfo; // 返回数据
    } 
    //生成随机数
    public function getRandomString($len, $chars=null){
    	if (is_null($chars)){
    		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	}
    	mt_srand(10000000*(double)microtime());
    	for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++){
    		$str .= $chars[mt_rand(0, $lc)];
    	}
    	return $str;
    }
    /**
     * 手机充值手续费比例
     */
    public function payFeeBili(){
    	$bili = $this->config['phone_pay_fee']*0.01;
    	$data['data'] = array('bili'=>$bili);
    	$data['code']= 1;
    	$data['msg']= '成功';
    	return json($data);
    }
    
    
 
}