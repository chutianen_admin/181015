<?php
namespace app\wallet\controller;
use think\Controller;
use think\Request;
require_once (APP_PATH . 'api/SmsApi.php');
class Register extends Controller
{
	/**
	 * 发送短信
	 */
	public function smsSend(){
		$phone = input('phone');
		$A_Sms = new \SmsApi();
		$res = $A_Sms->send($phone);
		return json (['data'=>'','code'=>$res['status'],'msg'=>$res['info']]);
	}
    /*
     * 注册密码
     */
	public function register(){
		//手机号
		$phone = input('phone');
		//验证码
		$code = input('code');
		//密码
		$pass = input('pass');
		//推荐人
		$tuijian = input('tuijian');
		//判断手机是否唯一
		$member = M('Users')->where(array('loginName'=>$phone))->find();
		if($member){
			$array['data'] = array('user_id'=>'','token'=>'');
			$array['code']= -6;
			$array['msg']= '手机号已注册！请登录';
			return json($array);
		}
		//验证信息
		if(empty($phone)){
			$array['data'] = array('user_id'=>'','token'=>'');
			$array['code']= -1;
			$array['msg']= '手机号不能为空';
			return json($array);
		}
		if(empty($pass)){
			$array['data'] = array('user_id'=>'','token'=>'');
			$array['code']= -4;
			$array['msg']= '密码不能为空';
			return json($array);
		}
		//验证验证码
		$A_Sms= new \SmsApi();
		$verifyPhone = $A_Sms ->checkAppCode($phone, $code);
		if($verifyPhone){
			$sendtime = json_decode($A_Sms ->getSms($phone));
			$name = 'time_quyum_' . $phone;
			$time = $sendtime ->$name;
			$ctime = time();
			$chatime = $ctime-$time;
			if($chatime>600){
				//删除验证码
				$A_Sms ->deleteSendSmsApp($phone);
				$array['data'] = array('user_id'=>'','token'=>'');
				$array['code']= -10;
				$array['msg']= '验证码失效(有效时间10分钟)';
				return json($array);
			}
		}
		
		if(!$verifyPhone){
			$array['data'] = array('user_id'=>'','token'=>'');
			$array['code']= -5;
			$array['msg']= '手机验证码输入有误';
			return json($array);
		}
		
		$data['loginName'] = $phone;
		$data['userPhoto'] = $phone;
		$data['loginPwd'] = md5($pass);
		$data['createTime'] = date('Y-m-d H:i:s',time());
		$res = M('Users')->insert($data);
		$id = M('Users')->getLastInsID();
		if(!$res){
			$array['data'] = array('user_id'=>'','token'=>'');
			$array['code']= -7;
			$array['msg']= '注册失败';
			return json($array);
		}
		//删除验证码
		$A_Sms ->deleteSendSmsApp($phone);
		//注册成功操作
		$re = $this->afterRegister($id,$tuijian);
		
		//注册成功生成token
		$token = md5($id.time());
		$arr['token'] = $token;
		$arr['uid'] = $id;
		M('token')->add($arr);
		//$after_res = M('Users')->where(array('userId'=>$id))->update($arr);
		$member = M('App_member_relation')->where(array('member_id'=>$id))->find();
		$A_Sms ->deleteSendSmsApp($phone);
		$array['data'] = array('user_id'=>$id,'token'=>$token,'account'=>$member['account']);
		$array['code']= 1;
		$array['msg']= '注册成功';
		return json($array);
	}
	/**
	 * 注册成功添加数据
	 * $member_id  Users表主键user_id
	 */
	public function afterRegister($member_id,$tuijian){
		$data['member_id'] = $member_id;
		//个人积分表
		$re[] = M('App_member_account')->insert($data);
		//结构表 （及其他用户信息）
		$tuijianren = M('Users')->where(array('loginName'=>$tuijian))->find();
		if($tuijianren['zhuceid']){
			$data['zhuceid'] = $tuijianren['userId'];
		}
		//注册生成推荐码
// 		$data['code'] = $this->getTuiJianCode();
		$data['nick']= 'jf_'.$this->getRandomString(6); 
		$data['level'] = 1;
		$re[] = M('App_member_relation')->insert($data);
		return $re;
	}
	
	public function aaa(){
		$aa = $this->getTuiJianCode(6);
		dump($aa);
	}
	
	/**
	 * 修改密码
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function resetPass(){
		//手机号
		$phone = input('phone');
		//验证码
		$code = input('code');
		//密码
		$pass = input('password');
		//获取数据
		if(empty($phone)){
			return json (['data'=>'','code'=>-1,'msg'=>'手机号不能为空']);
		}
		if(empty($pass)){
			return json (['data'=>'','code'=>-4,'msg'=>'密码不能为空']);
		}
		$A_Sms= new \SmsApi();
		$verifyPhone = $A_Sms ->checkAppCode($phone, $code);
		if(!$verifyPhone){
			return json (['data'=>'','code'=>-5,'msg'=>'手机验证码输入有误']);
		}
		$member = M('Users')->where(array('loginName'=>$phone))->find();
		if(!$member){
			return json (['data'=>'','code'=>-6,'msg'=>'当前手机号未注册']);
		}
		if($member['loginPwd'] == md5($pass)){
			return json (['data'=>'','code'=>-6,'msg'=>'新密码和原密码一致']);
		}
		$data['loginPwd'] = md5($pass);
		//修改密码
		$r = M('Users')->where(array('loginName'=>$phone))->update($data);
		
		if(!$r){
			return json (['data'=>'','code'=>-7,'msg'=>'修改失败']);
		}
		$A_Sms ->deleteSendSmsApp($phone);
		return json (['data'=>'','code'=>1,'msg'=>'修改成功']);
	}
   	public function createUserName() {
     	$charArr = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
     	$charArr = explode(",",$charArr);
     	do {
     		$num = '';
     		//取两个字母
     		$chars[0] = mt_rand(0,25);
     		$chars[1] = mt_rand(0,25);
     		foreach($chars as $v){
     			$num.=$charArr[$v];
     		}
     		//使用空字符串连接数组元素，也就是将数组转为字符串
     	} while ($this->userName_unique($num));//如果生成的编号在数据库中存在,则继续循环,否则输出到前台
     	return $num;
    }
    public function userName_unique($username){
    	$M_Student = new StudentModel();
    	$data = $M_Student->getStudentOneByField('full_name',$username);
     	return $data;
    }
    
    /**
     * 随机生成用户名
     * @param unknown $len
     * @param string $chars
     * @return string
     */
    public function getRandomString($len, $chars=null){
    	if (is_null($chars)){
    		$chars = "0123456789";
    	}
    	mt_srand(10000000*(double)microtime());
    	for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++){
    		$str .= $chars[mt_rand(0, $lc)];
    	}
    	return $str;
    }
}