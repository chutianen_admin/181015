<?php
/**
 * 	app 会员Api接口
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-09
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
use think\Request;
require_once (APP_PATH . 'api/FinanceApi.php');
class Shop extends Base {
    public function _initialize(){
        parent::_initialize();
       
    }
	
	/**
	 * 店铺申请
	 * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
	 */
	public function apply(){
		$phone 	= input('phone');
		$pay_type = input('pay_type');
		$num = input('num');
		$pay_pwd = input('pay_pwd');
// 		$shop_account = input('shop_account');
		$shop_pwd = input('shop_pwd');
		$shop_pwd_two = input('shop_pwd_two');
		$user_id = Base::$member['user_id'];
		$shop_type = input('goods_type');
		
		$user = Db::name('Users')->where(array('userId'=>$user_id))->find();
		if(empty($user)){
			$data['data'] = array();
			$data['code']= -1;
			$data['msg']= '无效token';
			return json($data);
		}
		if(empty($pay_type) || empty($pay_pwd) || empty($shop_pwd)){
			$data['data'] = array();
			$data['code']= -2;
			$data['msg']= '请补全信息';
			return json($data);
		}
		//电话号码
		if($phone != $user['loginName']){
			$data['data'] = array();
			$data['code']= -3;
			$data['msg']= '联系方式与预留不符';
			return json($data);
		}
		//判断是不是第二次申请
		$list = Db::name('App_shop')->where(array('member_id'=>$user_id))->select();
		if($list){
			if($list[0]['shop_pwd'] != $shop_pwd){
				$data['data'] = array();
				$data['code']= -6;
				$data['msg']= '请保持登录密码一致';
				return json($data);
			}
		}
		//判断金额
		$config = $this->getConfig();
		$price = $config['shop_'.$shop_type.'_'.$pay_type];
		$all_money = $price*$num;
		$member_account = Db::name('App_member_account')->where(array('member_id'=>$user_id))->find();
		if($all_money > $member_account['account_type_'.$pay_type]){
			$data['data'] = array();
			$data['code']= -4;
			$data['msg']= '余额不足';
			return json($data);
		}
		if(!$list){
			$arr['shopSn'] = $phone;
			$arr['userId'] = $user_id;
			$res[] = Db::name('Shops')->insert($arr);
		}
		$dataa['shopSn'] = $phone;
		$dataa['member_id'] = $user_id;
		$dataa['shop_account'] = $phone;
		$dataa['shop_pwd'] = $shop_pwd;
		$dataa['num'] = $num;
		$dataa['add_time'] = time();
		$dataa['status'] = 0;
		$dataa['goods_type'] = $shop_type;
		$dataa['pay_type'] = $pay_type;
		$dataa['all_money'] = $all_money;
		$res[] = Db::name('App_shop')->insert($dataa);
		
		$array['shopPwd'] = md5($shop_pwd);
		$array['userType'] = 1;
		$array['login'] = 
		$res[] = Db::name('Users')->where(array('userId'=>$user_id))->update($array);
		if($res){
			$r[] = Db::name('App_member_account')->where(array('member_id'=>$user_id))->setDec('account_type_'.$pay_type,$all_money);
			$finance = new \FinanceApi();
			$r[] = $finance->addFinance($user_id, 17, '店铺申请', 2, $all_money, $pay_type);
			$data['data'] = array();
			$data['code']= 1;
			$data['msg']= '申请成功，请等待审核';
			return json($data);
		}else{
			$data['data'] = array();
			$data['code']= -9;
			$data['msg']= '申请失败';
			return json($data);
		}
	}
	/**
	 * 申请店铺提示信息
	 */
	public function getPrompt(){
		$user_id = Base::$member['user_id'];
		$config = $this->getConfig();
		$shop = Db::name('App_shop')->where(array('member_id'=>$user_id))->find();
		if($shop){
			$status = 1;
		}else{
			$status = 0;	
		}
		if(!empty($config['shop_prompt'])){
			$data['data'] = $config['shop_prompt'];
			$data['code']= 1;
			$data['status'] = $status;
			$data['msg']= '成功';
			return json($data);
		}else{
			$data['data'] = '';
			$data['code']= -1;
			$data['status'] = $status;
			$data['msg']= '无';
			return json($data);
		}
	}
	/**
	 * 获得开店所需积分
	 */
	public function getAllMoney(){
		$account_type = input('account_type');
		$shop_type = input('shop_type');
		$num = input('num');
		$user_id = Base::$member['user_id'];
		if(empty($account_type) || empty($shop_type) ||empty($num)){
			$data['data'] = array('money'=>'');
			$data['code']= -2;
			$data['msg']= '请补全信息';
			return json($data);
		}
		$user = Db::name('Users')->where(array('userId'=>$user_id))->find();
		if(!$user){
			$data['data'] = array('money'=>'');
			$data['code']= -3;
			$data['msg']= '无效token';
			return json($data);
		}
		
		$config = $this->getConfig();
		$money = $config['shop_'.$shop_type.'_'.$account_type];
		$all_money = $money*$num;
		$member_account = Db::name('App_member_account')->where(array('member_id'=>$user_id))->find();
		if($member_account['account_type_'.$account_type] < $all_money){
			$data['data'] = array('money'=>'');
			$data['code']= -1;
			$data['msg']= '账户余额不足';
			return json($data);
		}else{
			$data['data'] = array('money'=>$all_money);
			$data['code']= 1;
			$data['msg']= '成功';
			return json($data);
		}
		
	}
	/**
	 * 获得自己的店铺申请信息
	 */
	public function getMyShopList(){
		$user_id = Base::$member['user_id'];
		$user = Db::name('Users')->where(array('userId'=>$user_id))->find();
		if(!$user){
			$data['data'] = array();
			$data['code'] = -2;
			$data['msg'] = '无效token';
			return json($data);
		}
		$list = Db::name('App_shop')->where(array('member_id'=>$user_id))->select();
		if($list){
			$data['data'] = $list;
			$data['code'] = 1;
			$data['msg'] = '成功';
			return json($data);
		}else{
			$data['data'] = array();
			$data['code'] = -1;
			$data['msg'] = '暂无信息';
			return json($data);
		}
	}
	
	public function getJiFenAndQuYu(){
		$config = $this->getConfig();
		$shop_type =  explode(',', $config['shop_type']);
		$quyu = explode(',', $config['shop_qu']);
		$all = M('App_account_type')->select();
		foreach($all as $k=>$v){
			if(in_array($all[$k]['id'],$shop_type)){
				$array['id'] = $all[$k]['id'];
				$array['name'] = $all[$k]['name'];
				$arr['jifen'][] = $array;
			}
		}
		foreach($quyu as $m=>$n){
			$arr['quyu'][$m] = $this->getArrByType($quyu[$m]);
			 
		}
		$data['data'] =$arr;
		$data['code'] = 1;
		$data['msg'] = '成功';
		return json($data);
	}
	
	public function getArrByType($type){
		switch ($type){
			case 1:
				$arr['name'] = '精品区';
				$arr['id'] = '1';
				break;
			case 2:
				$arr['name'] = '增值区';
				$arr['id'] = '2';
				break;
			case 3:
				$arr['name'] = '公益区';
				$arr['id'] = '3';
				break;
		}
		return $arr;
	}
	
	
	
	
}