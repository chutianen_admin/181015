<?php
/**
 * Created by PhpStorm.
 * User: "GS"
 * Date: 17-03-07
 * Time:
 */

namespace app\wallet\controller;
use Think\Db;
use think\Request;
class Strfill extends Base{
   
	public function strtofill(){
		$config = $this->getConfig();
		//短信内容
		$content = $_POST['message'];
		//来信号码
		$from = $_POST['from'];
		//FillMoney  秘钥
		$secret = $_POST['secret'];
		//正则匹配内容
		$preg_match = $config['Preg_match'];
		
		foreach($_POST as $k=>$v){
			$ss .= $k . ' = ' .$v;
		}
		file_put_contents('/var/www/html/schome/application/wallet/controller/duanxincanshu.txt',$ss);
		$sms_w['add_time'] = $_POST['sent_timestamp'];
		$sms_w['from'] = $from;
		$sms_w['type'] = '银行卡';
		$sms_w['content'] = $content;
		$rerr = M('Sms')->where($sms_w)->find();
		if($rerr){
			$contentsss = date('Y-m-d H:i') . '收到重复短信|';
			file_put_contents('/var/www/html/schome/application/wallet/controller/smslog.txt',$contentsss,FILE_APPEND);
			return false;
		}
		//判断短信来源号码是否与后台预留的相符
		if($from != $config['STR_PHONE']){
			$data['content'] = $content;
			$data['from'] = $from;
			$data['type'] = '银行卡';
			$data['add_time'] = $_POST['sent_timestamp'];
			$data['status'] = '失败 来源号码不明';
			M('Sms')->add($data);
			return;
		}
		//判断短信网关发送的秘钥和后台配置项是否一致
		if($secret != $config['STR_secret']){
			$data['content'] = $content;
			$data['from'] = $from;
			$data['type'] = '银行卡';
			$data['add_time'] = $_POST['sent_timestamp'];
			$data['status'] = '失败 来源密钥不正确';
			M('Sms')->add($data);
			return;
		}
		//'/账户(.*?)存入￥(.*?)元，(.*?)。(.*?)支付宝转账。【(.*?)】/'   1
		//您账户(.*?)转入人民币(.*?)，(.*?)，付方(.*?)。(.*?)。    2
		$re = preg_match($preg_match , $content, $matchs);
		file_put_contents('./testsms.txt',$matchs[4].'|||||'.$matchs[2]);
		if($re){
			$where['member_name'] = $matchs[4];
			$where['money'] = $matchs[2];
			$where['type'] = '银行卡';
			$where['status'] = 0;
			file_put_contents('./test.txt',$where);
			
			$list = M('Pay')->where($where)->order('add_time desc')->select();
			//file_put_contents('./teaast.txt',$list);
			if(count($list) > 0){
				//匹配成功进行状态修改
				M('Pay')->where("pay_id = {$list[0]['pay_id']}")->setField('status',1);
				M('Member')->where("member_id={$list[0]['member_id']}")->setInc('rmb',$list[0]['money']);
				
				$data['content'] = $content;
				$data['from'] = $from;
				$data['type'] = '银行卡';
				$data['add_time'] = $_POST['sent_timestamp'];
				if(count($list) > 1){
					foreach($list as $k => $v){
						if($k != 0){
							M('Pay')->where("pay_id = {$v['pay_id']}")->setField('status',-1);
						}
					}
					$data['status'] = count($list).'条订单，成功一条';
				}else{
					
					$data['status'] = '成功';
				}
				
				M('Sms')->add($data);
				
				return;
				
			}else{
				
				$data['content'] = $content;
				$data['from'] = $from;
				$data['type'] = '银行卡';
				$data['status'] = '失败 没有此订单';
				$data['add_time'] = $_POST['sent_timestamp'];
				M('Sms')->add($data);
				
				return;
			}
			
		}else{
			$data['content'] = $content;
			$data['from'] = $from;
			$data['type'] = '银行卡';
			$data['status'] = '失败 正则匹配错误';
			$data['add_time'] = $_POST['sent_timestamp'];
			M('Sms')->add($data);
			return;
		}	
		
	}
	
	public function getConfig(){
		$list=M("App_config")->select();
		foreach ($list as $k=>$v){
            $list[$v['key']]=$v['value'];
        }
		return $list;
	}
	
}