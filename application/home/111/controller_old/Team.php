<?php
/**
 * 	app 日志
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-18
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
use think\Request;
class Team extends Base {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    
    // 团队管理
   	//参数  token  用户token 获取用户id
  	public function administer(){
  		//获取用户id
  		$user_id = Base::$member['user_id'];
  		//获取此用户的信息
  		$list = db('app_member_relation')->where(['member_id'=>$user_id])->find();
  		$list1 = db('users')->where(['userId'=>$user_id])->find();
  		//获取推荐人为此用户的用户
  		$relation = db('app_member_relation')->where(['zhuceid'=>$user_id])->select();
  		//获取总业绩
  		$yeji = $list['yeji'];
  		//获取今日业绩
  		$new_yeji =	$list['new_yeji'];
  		//昨日业绩 ？？？？？？
  		$old_yeji = $list['old_yeji'];
 		//获取总消费额
 		$all_trade_money = $list['all_trade_money'];
 		//获取今日消费
 		$new_trade_money = $list['new_trade_money'];
 		//获取昨日消费
 		$old_trade_money = $list['old_trade_money'];
 		//结余业绩
 		$surplus_yeji = $list['surplus_yeji'];
  		// 该用户的信息
  		$data['yeji'] =$yeji;
  		$data['new_yeji'] =$new_yeji;
  		$data['old_yeji'] =$old_yeji;
  		$data['all_trade_money'] =$all_trade_money;
  		$data['new_trade_money'] =$new_trade_money;
  		$data['old_trade_money'] =$old_trade_money;
  		$data['surplus_yeji'] =$surplus_yeji;
  		$data['name'] =$list['name'];
  		$data['time'] =$list1['createTime'];
  		// 直推人的信息
  		$arr = array();
  		foreach($relation as $k=>$v){
  			$user = Db::name('Users')->where(array('userId'=>$relation[$k]['member_id']))->find();
  			$arr[$k]['yeji'] = $v['yeji'];
  			$arr[$k]['new_yeji'] = $v['new_yeji'];
  			$arr[$k]['old_yeji'] = $v['old_yeji'];
  			$arr[$k]['all_trade_money'] = $v['all_trade_money'];
  			$arr[$k]['new_trade_money'] = $v['new_trade_money'];
  			$arr[$k]['old_trade_money'] = $v['old_trade_money'];
  			$arr[$k]['surplus_yeji'] = $v['surplus_yeji'];
  			$arr[$k]['name'] = $v['name'];
  			$arr[$k]['time'] = $user['createTime'];
  		}
  		return json (['data'=>$data,'arr'=>$arr,'code'=>1,'msg'=>'操作成功']);
  		
  		
		
  	}
    
    
    
}