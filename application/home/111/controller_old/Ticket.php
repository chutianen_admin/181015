<?php
/**
 * 	app 日志
 * 	=============================
 *	Author: 褚天恩
 *  Date: 2017-10-18
 * @author Administrator
 *
 */
namespace app\wallet\controller;
use Think\Db;
use think\Request;
require_once (APP_PATH . 'api/FinanceApi.php');
class Ticket extends Base {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    /*
     *判断火车票开关 
     */
    public function train(){
    	$config = M('app_config')->where(['key'=>'train'])->find();
    	return json (['data'=>$config,'code'=>1,'msg'=>'操作成功']);
    }
    /*
     *判断飞机票开关
    */
    public function plane(){
    	$config = M('app_config')->where(['key'=>'plane'])->find();
    	return json (['data'=>$config,'code'=>1,'msg'=>'操作成功']); 
    }
    
    /**
     * 判断购买火车票币种和余额是否充足
     * 参数 token 用户token
     * 参数 type 积分类别  1消费积分2现金钱包3购物积分4富通积分
     * 参数 money 支付钱数 
     */
    public function judgeBalance(){
        //获取用户id
    	$user_id = Base::$member['user_id'];
        // 判断该积分是否可用来购买火车票
        $config = M('app_config')->where(['key'=>'car_ticket'])->find();
        $type = I('type');
        if($type!=$config['value']){
            return json (['data'=>'','code'=>-1,'msg'=>'该币种不能支付火车票']);
        }
        //判断该用户的积分是否够支付
        //用户现有积分
        $user = M('app_member_account')->where(['member_id'=>$user_id])->field('account_type_'.$type)->find();
        //获取手续费
        $car_fee = M('app_config')->where(['key'=>'car_fee'])->find();
        //火车票用积分
        $money = (float)I('money');
        $money += $money*($car_fee['value']/100);
        if($money>$user['account_type_'.$type]){
             return json (['data'=>'','code'=>-2,'msg'=>'用户积分不足']);
        }
        return json (['data'=>'','code'=>1,'msg'=>'可以购买']);    
    }
    /**
     * 添加火车票订单
     * 参数 token 用户token
     * 参数 start 起始地点
     * 参数 starttime 起始时间 
     * 参数 end 终点 
     * 参数 endtime 终点时间
     * 参数 time 发车日期
     * 参数 seating 座位
     * 参数 tran 车次
     * 参数 money 价钱
     * 参数 ordernum 订单号
     * 参数 orderid 聚合订单号
     * 参数 ordernmber 12306订单号
     * 参数 ticket_no  车票号
     */
    public function addTicket()  {
        //获取用户id
        $user_id = Base::$member['user_id'];
        if(!$user_id){
           return json (['data'=>'','code'=>-9,'msg'=>'用户token错误']); 
        }
        //获取起始地点并判断
        $start = I('start');
        if(empty($start)){
            return json (['data'=>'','code'=>-1,'msg'=>'起始地点不能空']);
        }
        //获取起始时间并判断
        $starttime = I('starttime');
        if(empty($starttime)){
            return json (['data'=>'','code'=>-2,'msg'=>'起始时间不能空']);
        }
        //获取终点并判断
        $end = I('end');
        if(empty($end)){
            return json (['data'=>'','code'=>-3,'msg'=>'终点不能空']);
        }
         //获取终点时间并判断
        $endtime = I('endtime');
        if(empty($endtime)){
            return json (['data'=>'','code'=>-4,'msg'=>'终点时间不能空']);
        }
         //获取发车日期并判断
        $time = I('time');
        if(empty($time)){
            return json (['data'=>'','code'=>-5,'msg'=>'发车日期不能空']);
        }
         //获取座位并判断
        $seating = I('seating');
        if(empty($seating)){
            return json (['data'=>'','code'=>-6,'msg'=>'座位不能空']);
        }
         //获取车次并判断
        $tran = I('tran');
        if(empty($tran)){
            return json (['data'=>'','code'=>-7,'msg'=>'车次不能空']);
        }
         //获取价钱并判断
        $money = (float)I('money');
        if(empty($money)){
            return json (['data'=>'','code'=>-11,'msg'=>'价钱不能空']);
        }
        //获取12306订单号并判断
        $orderid = I('orderid');
        if(empty($orderid)){
            return json (['data'=>'','code'=>-12,'msg'=>'12306订单号不能空']);
        }
        //获取车票号并判断
        $ticket_no = I('ticket_no');
        if(empty($ticket_no)){
            return json (['data'=>'','code'=>-13,'msg'=>'车票号不能空']);
        }
        //获取聚合订单号并判断
        $ordernmber = I('ordernmber');
        if(empty($ordernmber)){
            return json (['data'=>'','code'=>-14,'msg'=>'聚合订单号不能空']);
        }
        //获取订单号并判断
        $ordernum = I('ordernum');
        if(empty($ordernum)){
        	return json (['data'=>'','code'=>-18,'msg'=>'订单号不能空']);
        }
        // 扣除相应积分
        $config = M('app_config')->where(['key'=>'car_ticket'])->find();
        M('app_member_account')->where(['member_id'=>$user_id])->setDec('account_type_'.$config['value'],$money);
        //添加财务日志
        //实例化日志api
        $content = '购买火车票 从'.$start.'到'.$end.' 车次：'.$tran.'发车时间：'.$time;
        $finance = new \FinanceApi();
//         $finance->addFinance($user_id,12,$content,2,$money,$config['value']);
		$finance->addFinance($user_id, 12, $content, 2, $money, $config['value']);
        // 添加操作
        $data['uid'] = $user_id;
        $data['start'] = $start;
        $data['starttime'] = $starttime;
        $data['end'] = $end;
        $data['endtime'] = $endtime;
        $data['time'] = $time;
        $data['addtime'] = date('Y-m-d H:i:s',time());
        $data['seating'] = $seating;
        $data['tran'] = $tran;
        $data['money'] = $money;
        $data['ordernum'] = $ordernum;
        $data['orderid'] = $orderid;
        $data['ticket_no'] = $ticket_no;
        $data['ordernmber'] = $ordernmber;
        $r = M('ticket')->add($data);
        if($r){
            return json (['data'=>'','code'=>1,'msg'=>'操作成功']);
        }else{
            return json (['data'=>'','code'=>-10,'msg'=>'操作失败']);
        }
    }
    /**
     * 判断购买飞机票币种和余额是否充足
     * 参数 token 用户token
     * 参数 type 积分类别  1消费积分2现金钱包3购物积分4富通积分
     * 参数 money 支付钱数 
     */
    public function judgePlaneBalance(){
        //获取用户id
        $user_id = Base::$member['user_id'];
        $token = I('token');
        if(!$token){
            return json (['data'=>'','code'=>-5,'msg'=>'用户不存在']);
        }
        // 判断该积分是否可用来购买火车票
        $config = M('app_config')->where(['key'=>'plane_ticket'])->find();
        $type = I('type');
        if($type!=$config['value']){
            return json (['data'=>'','code'=>-1,'msg'=>'该币种不能支付飞机票']);
        }
        //判断该用户的积分是否够支付
        //用户现有积分
        $user = M('app_member_account')->where(['member_id'=>$user_id])->field('account_type_'.$type)->find();
        //获取手续费
        $plane_fee = M('app_config')->where(['key'=>'plane_fee'])->find();
        //火车票用积分
        $money = (float)I('money');
        $money += $money*($plane_fee['value']/100);
        if($money>$user['account_type_'.$type]){
             return json (['data'=>'','code'=>-2,'msg'=>'用户积分不足']);
        }
        return json (['data'=>'','code'=>1,'msg'=>'可以购买']);    
    }

    /**
     * 添加飞机票订单
     * 参数 token 用户token
     * 参数 start 起始地点
     * 参数 starttime 起始时间 
     * 参数 end 终点 
     * 参数 endtime 终点时间
     * 参数 time 发车日期
     * 参数 seating 仓
     * 参数 tran 车次
     * 参数 money 价钱
     * 参数 ordernum 订单号
     */
    public function addPlaneTicket()  {
        //获取用户id
        $user_id = Base::$member['user_id'];
        if(!$user_id){
           return json (['data'=>'','code'=>-9,'msg'=>'用户token错误']); 
        }
        //获取起始地点并判断
        $start = I('start');
        if(empty($start)){
            return json (['data'=>'','code'=>-1,'msg'=>'起始地点不能空']);
        }
        //获取起始时间并判断
        $starttime = I('starttime');
        if(empty($starttime)){
            return json (['data'=>'','code'=>-2,'msg'=>'起始时间不能空']);
        }
        //获取终点并判断
        $end = I('end');
        if(empty($end)){
            return json (['data'=>'','code'=>-3,'msg'=>'终点不能空']);
        }
         //获取终点时间并判断
        $endtime = I('endtime');
        if(empty($endtime)){
            return json (['data'=>'','code'=>-4,'msg'=>'终点时间不能空']);
        }
         //获取发车日期并判断
        $time = I('time');
        if(empty($time)){
            return json (['data'=>'','code'=>-5,'msg'=>'发车日期不能空']);
        }
         //获取座位并判断
        $seating = I('seating');
        if(empty($seating)){
            return json (['data'=>'','code'=>-6,'msg'=>'仓不能空']);
        }
         //获取车次并判断
        $tran = I('tran');
        if(empty($tran)){
            return json (['data'=>'','code'=>-7,'msg'=>'机次不能空']);
        }
         //获取价钱并判断
        $money = (float)I('money');
        if(empty($money)){
            return json (['data'=>'','code'=>-11,'msg'=>'价钱不能空']);
        }
         // 扣除相应积分
        $config = M('app_config')->where(['key'=>'plane_ticket'])->find();
        M('app_member_account')->where(['member_id'=>$user_id])->setDec('account_type_'.$config['type'],$money);
        // 添加操作
        $data['uid'] = $user_id;
        $data['start'] = $start;
        $data['starttime'] = $starttime;
        $data['end'] = $end;
        $data['endtime'] = $endtime;
        $data['time'] = $time;
        $data['addtime'] = date('Y-m-d H:i:s',time());
        $data['seating'] = $seating;
        $data['tran'] = $tran;
        $data['money'] = $money;
        $data['ordernum'] = $ordernum;
        $data['status'] = 1;
        $r = M('ticket_plane')->add($data);
        if($r){
            return json (['data'=>'','code'=>1,'msg'=>'操作成功']);
        }else{
            return json (['data'=>'','code'=>-10,'msg'=>'操作失败']);
        }
    }
    
    
    
    
    
    
}