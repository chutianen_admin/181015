<?php
namespace app\home\controller;

class Address extends Base {
	//空操作
	public function _initialize(){
		parent::_initialize();
		//define('SC_SYS','http://sksc.quyum99.com');
	}
	
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	/**
	 * 收货地址
	 */
	public function index(){
		$member_id = $_SESSION['think']['USER_KEY_ID'];
		$where ['member_id'] = $member_id;
 		$list = M('App_address')->where($where)->select();
		$this->assign('list',$list);
		return $this->fetch();
	}
	/**
	 * 修改收货地址显示
	 */
	public function update(){
		//三级联动
		$parent_id['parentId'] = 0;
		$region = M('Areas')->where($parent_id)->select();
		$this->assign('region',$region);
		//三级联动结束
		$address_id = I('address');
		$list = M('App_address')->where(array('id'=>$address_id))->find();
		$this->assign('detailed',$list['detailed']);
		$this->assign('phone',$list['phone']);
		$this->assign('code',$list['code']);
		$this->assign('linkman',$list['linkman']);
		$this->assign('id',$list['id']);
		return $this->fetch();
	}
	/**
	 * 修改收获地址提交
	 */
	public function doupdate(){

		if (empty ( $_POST ['linkman'] )) {
			$data['info'] = "请输入收件人";
			$data['status'] = -1;
			return json($data);
		}
		if (empty ( $_POST ['phone'] )) {
			$data['info'] = "请输入手机号";
			$data['status'] = -2;
			return json($data);
		}
		if (empty ( $_POST ['code'] )) {
			$data['info'] = "请输入邮编";
			$data['status'] = -3;
			return json($data);
		}
		if (empty ( $_POST ['address'] )) {
			$data['info'] = "请输入详细地址";
			$data['status'] = -4;
			return json($data);
		}
		if (empty ( $_POST ['sheng'] )) {
			$data['info'] = "请选择省地";
			$data['status'] = -6;
			return json($data);
		}
		if (empty ( $_POST ['shi'] )) {
			$data['info'] = "请选择城市";
			$data['status'] = -7;
			return json($data);
		}
		if (empty ( $_POST ['qu'] )) {
			$data['info'] = "请选择区域";
			$data['status'] = -8;
			return json($data);
		}
		$sheng_id = I('sheng');
		$sh = M('Areas')->where("areaId='{$sheng_id}'")->find();
		$sheng = $sh['area_name'];
		
		$s = M('Areas')->find(I('shi'));
		$shi = $s['area_name'];
		
		$q = M('Areas')->find(I('qu'));
		$qu = $q['area_name'];
		$_POST ['detailed'] = I('address');//详细地址
		$_POST ['address'] = $sheng.$shi.$qu.I('address');//收货地址
		$_POST ['province_id'] = $sheng_id;//省_id
		$_POST ['city_id'] = I('shi');//市_id
		$_POST ['area_id'] = I('qu');//区_id
		
		$list = M('App_address')->update($_POST);
		if($list){
			$data['info'] = "修改成功";
			$data['status'] = 1;
			return json($data);
		}else{
			$data['info'] = "修改失败";
			$data['status'] = -5;
			return json($data);
		}
		
	}
	
	/**
	 * 选择收货地址
	 */
	public function area(){
		$parent_id['parent_id'] = I('post.pro_id','addslashes');
		$region = M('Areas')->where($parent_id)->select();
		$opt = '<option>--请选择市区--</option>';
		foreach($region as $key=>$val){
			$opt .= "<option value='{$val['areaId']}'>{$val['areaName']}</option>";
		}
		//dump($opt);die();
		echo $opt;
	
	}
	/**
	 * 删除收货地址
	 */
	public function Del(){
		$id=I('id');
		$address = M('App_address')->where(array('id'=>$id))->find();
		if($address['state']==1){
			$this->error('不要删除默认的收货地址');
		}
		$res = M("App_address")->delete($id);
		if($res){
			$msg['status']=1;
			$msg['info']="删除成功";
			return json($msg);exit();
		}else{
			$msg['status']=-1;
			$msg['info']="删除失败";
			return json($msg);exit();
		}
	}
	/**
	 *添加地址显示 
	 */
	public function insert(){
		if(IS_POST){
			//三级联动
			$parent_id['parentId'] = I('post.pro_id','addslashes');
			$region = M('Areas')->where($parent_id)->select();
			$opt = '<option>--请选择市区--</option>';
			foreach($region as $key=>$val){
				$opt .= "<option value='{$val['areaId']}'>{$val['areaName']}</option>";
			}
			//dump($opt);die();
			echo $opt;
			//三级联动结束
		}else{
			//三级联动(省)
			$parent_id['parentId'] = 0;
			$region = M('Areas')->where($parent_id)->select();
			$this->assign('region',$region);
			return $this->fetch();
			//三级联动结束
		}
	}
	
	/**
	 * 添加收货地址提交
	 */
	public function DoInsert(){
		if (empty ( $_POST ['address'] )) {
			$data['info'] = "请输入收货地址";
			$data['status'] = -1;
			return json($data);
		}
		if (empty ( $_POST ['shi'] )) {
			$data['info'] = "请选择市区";
			$data['status'] = -2;
			return json($data);
		}
		if (empty ( $_POST ['sheng'] )) {
			$data['info'] = "请选择省份";
			$data['status'] = -3;
			return json($data);
		}
		if (empty ( $_POST ['address'] )) {
			$data['info'] = "请输入详细地址";
			$data['status'] = -4;
			return json($data);
		}
		if (empty ( $_POST ['code'] )) {
			$data['info'] = "请输入邮编";
			$data['status'] = -5;
			return json($data);
		}
		if (empty ( $_POST ['phone'] )) {
			$data['info'] = "请输入联系电话";
			$data['status'] = -7;
			return json($data);
		}
		if (empty ( $_POST ['linkman'] )) {
			$data['info'] = "请输入收件人";
			$data['status'] = -7;
			return json($data);
		}
		
		//根据Ajax传过来的城市id查询城市名
		$res ['phone'] = I('phone');//收件人电话
		$sheng_id = I('sheng');
		$sh = M('Areas')->where("areaId='{$sheng_id}'")->find();
		$sheng = $sh['areaName'];
		$s = M('Areas')->find(I('shi'));
		
		$shi = $s['areaName'];
		$q = M('Areas')->find(I('qu'));
		$qu = $q['areaName'];
		$res ['code'] = I('code');//收件人邮编
		$res ['linkman'] = I('linkman');//收件人姓名
		$res ['address'] = $sheng.$shi.$qu.I('address');//收货地址
		$res ['member_id'] = $_SESSION['think']['USER_KEY_ID'];//地址的用户
		$res ['province_id'] = $sheng_id;//省_id
		$res ['city_id'] = I('shi');//市_id
		$res ['area_id'] = I('qu');//区_id
		$res ['detailed'] = I('address');//详细地址
		$dizhi = M('App_address')->add($res);
		if($dizhi){
			$data['info'] = "添加成功";
			$data['status'] = 1;
			return json($data);
		}else{
			$data['info'] = "添加失败";
			$data['status'] = -6;
			return json($data);
		}
	}
	/**
	 * 设置默认收货地址
	 */
	public function Def(){
		$res ['id'] = I(id);
		$res ['status'] = '1';
		$uid= $_SESSION['think']['USER_KEY_ID'];
		$data['status'] ="0";
		$res1 = M('App_address')->where("member_id={$uid}")->update($data);
		$list = M('App_address')->update($res);
		if($list && $res1){
			$data['info'] = "设置成功";
			$data['status'] = 1;
			return json($data);
		}else{
			$data['info'] = "设置失败";
			$data['status'] = -1;
			return json($data);
		}
	}
	
}