<?php
namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
class Art extends Base {
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	//发件箱
	public function outbox(){
		//二级密码验证
		$this->checkTwoPwd();
		session('checkTwoPwd_success',null);
		$where['member_id']=session('USER_KEY_ID');
		$where['status']=0;
		$count = M('Message_kuang')->where($where)->count();//根据分类查找数据数量
		$page = new \Think\Page($count,15);//实例化分页类，传入总记录数和每页显示数
		//给分页传参数
		//setPageParameter($page, array('cuid'=>I("cuid"),'email'=>$name['member_id']));
		$show = $page->show();//分页显示输出性
		$list=M('Message_kuang')->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('list',$list);
		$this->assign('page',$show);
		$this->assign('count',$count);
		return $this->fetch();
	}
	//收件箱
	public function inbox(){
		//二级密码验证
		$this->checkTwoPwd();
		session('checkTwoPwd_success',null);
		$where['member_id']=session('USER_KEY_ID');
		$where['status']=1;
		$count = M('Message_kuang')->where($where)->count();//根据分类查找数据数量
		$page = new \Think\Page($count,15);//实例化分页类，传入总记录数和每页显示数
		//给分页传参数
		//setPageParameter($page, array('cuid'=>I("cuid"),'email'=>$name['member_id']));
		$show = $page->show();//分页显示输出性
		$list=M('Message_kuang')->where($where)->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('list',$list);
		$this->assign('page',$show);
		$this->assign('count',$count);
		return $this->fetch();
	}
	//发送邮件
	public function send(){
		//二级密码验证
		$this->checkTwoPwd();
		session('checkTwoPwd_success',null);
		return $this->fetch();
	}
	//发送邮件
	public function sendAjaxReturn(){
		$title=I('post.title');
		$content=I('post.content');
		if(empty($title)||empty($content)){
			$data['status'] = -1;
			$data['info'] = '请填写完整信息';
			$this->ajaxReturn($data);
		}
		$data['member_id']=session('USER_KEY_ID');
		$data['title']=$title;
		$data['content']=$content;
		$data['add_time']=time();
		$data['status']=0;
		$r=M('Message_kuang')->add($data);
		if($r){
			$data['status'] = 1;
			$data['info'] = '提交成功';
			$this->ajaxReturn($data);
		}else{
			$data['status'] = -2;
			$data['info'] = '服务器繁忙,请稍后重试';
			$this->ajaxReturn($data);
		}
	}
	
	public function details(){
		$id = input('id');
		$list = Db::name('App_article')->where(array('article_id'=>$id))->find();
		
// 		dump($list);
		
		$this->assign('list',$list);
		return $this->fetch();
	}
 
}