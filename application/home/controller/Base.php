<?php
namespace app\home\controller;
use think\Db;
use think\Controller;
use think\Session;
use traits\model\Transaction;
class Base extends Controller {
 	protected $member;
	protected $mills;
	public function _initialize(){
		parent::_initialize();
		$session = session::get();
		if(empty($session['USER_KEY_ID'])){
			$this->redirect("login/index");
  		} 
  		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$session['USER_KEY_ID']))->find();
  		//dump($member_relation);
  		$this->assign('sess',$member_relation);
  		//区分是双轨的制度还是太阳线的制度
  		$system_type = config("system_type");//1是双轨    2太阳线
  		$this->assign('system_type',$system_type);
  		
	}
	//二级密码验证
	//         $this->checkTwoPwd();
	//         session('checkTwoPwd_success',null);
	//检测二级密码，显示二级密码页面
	protected function checkTwoPwd(){
		if($_SESSION['checkTwoPwd_success']==null){
			session('url',__ACTION__);
			$this->redirect("Two/twopwd");
		} 
	
	}
    protected  function getMemberByMemberId($id){
    	$where['member_id']=$id;
    	return M('Member')->where($where)->find();
    }
	protected function getUpMemberByZhuceId($jiedianid,$lv,$k=0){
		$user=M("Member")->where("member_id='$jiedianid'")->find();
		$k++;
		if (!empty($user)) {
			$item[]=$user;
			if ($k<$lv) {
				$r=$this->getUpUserByJiedianid($user['zhuceid'],$lv,$k);
				foreach ($r as $v){
					if (!empty($v)) {
						$item[]=$v;
					}
				}
			}
		}
		return $item;
	}
    protected  function getMemberByUsername($email){
    	$where['email']=$email;
    	return M('Member')->where($where)->find();
    }
	//检测是否设置二级密码
	protected function checkTwoPwdSet(){
		if(!$this->member['pwdtrade']){
			//取消二级密码检测  1是通过即不检测 
			session('two_pwd_check',1);
			session('checkTwoPwd_success',rand(111111,999999));
			$this->redirect("SetTradePwd/index");
		}
	}
	//图片处理
	public function upload($file){
		 
		switch($file['type'])
		{
			case 'image/jpeg': $ext = 'jpg'; break;
			case 'image/gif': $ext = 'gif'; break;
			case 'image/png': $ext = 'png'; break;
			case 'image/tiff': $ext = 'tif'; break;
			default: $ext = ''; break;
		}
		if (empty($ext)){
			return false;
		}
		$upload = new \Think\Upload();// 实例化上传类
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->savePath  =      './Public/Uploads/'; // 设置附件上传目录
		// 上传文件
		$info   =  $upload->uploadOne($file);
		if(!$info) {
			// 上传错误提示错误信息
			$this->error($upload->getError());exit();
		}else{
			// 上传成功
			$pic=$info['savepath'].$info['savename'];
			$url='/Uploads'.ltrim($pic,".");
		}
		return $url;
	}
	/**
	 * 判断会员等级
	 * @param unknown $level
	 */
	public function getUserMoneyByLevel($level){
		$class = Db::name('App_level')->where(array('id'=>$level))->find();
		return $class;
	}
	//查询配置
	public function configs(){
		$list = Db::name('App_config')->select();
		foreach($list as $key=>$value){
			$list[$value['key']] = $value['value'];
		}
		return $list;
	}

}