<?php
namespace Kuang\Controller;
use Kuang\Controller\KuangController;
class BuyController extends KuangController {
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	//矿机托管
	public function index(){
		//二级密码验证
		//$this->checkTwoPwd();
		session('checkTwoPwd_success',null);		
		$where['member_id']=session('USER_KEY_ID');
		$where[C("DB_PREFIX").'kuangji_log.status']=0;
		$list=M('Kuangji_log')
		->field(C("DB_PREFIX").'kuangji_log.*,'.C("DB_PREFIX").'kuangji.name,'.C("DB_PREFIX").'kuangji.price,'.C("DB_PREFIX").'kuangji.usetime,'.C("DB_PREFIX").'kuangji.num')
		->join('left join '.C("DB_PREFIX").'kuangji on '.C("DB_PREFIX").'kuangji.kuangji_id='.C("DB_PREFIX").'kuangji_log.kid')
		->where($where)
		->order(C("DB_PREFIX").'kuangji_log.add_time desc')
		->select();
		$count=M('Kuangji_log')->where($where)->count();
		$this->assign('num',$count);
		foreach($list as $k=>$v){
			//到期日
			$list[$k]['end_time']=intval($v['add_time'])+intval($v['usetime'])*24*3600;
			//合同剩余天
			$list[$k]['deal']=intval(($list[$k]['end_time']-strtotime(date('Y-m-d',time())))/(24*3600));
			//总量
			$list[$k]['count']=floatval($v['num']*$v['usetime']);
			//获取已经挖出的卡莱币数量   同时  检验自动程序生成足够币之后修改 log表 status
			$list[$k]['get_count']=$this->getWachuCountByLogId($v['id'],session('USER_KEY_ID'),$list[$k]['count']);
			$list[$k]['shengyu']=$list[$k]['count']-$list[$k]['backmoney'];
		}
		$this->assign('list',$list);
		//财务日志按天统计
		//显示日志
		$finance_where[C("DB_PREFIX")."finance.member_id"]=$_SESSION['USER_KEY_ID'];
        $count = M('Finance')->where ( $finance_where )->count (); // 查询满足要求的总记录数
        $Page =new \Think\Page($count,5); // 实例化分页类 传入总记录数和每页显示的记录数(25)
        //给分页传参数
        $show = $Page->show (); // 分页显示输出
		$finance=M('Finance')
		->field(C("DB_PREFIX")."finance.*,".C("DB_PREFIX")."member.email as username,".C("DB_PREFIX")."currency.currency_name,".C("DB_PREFIX")."finance_type.name as typename")
		->join("left join ".C("DB_PREFIX")."member on ".C("DB_PREFIX")."member.member_id=".C("DB_PREFIX")."finance.member_id")
		->join("left join ".C("DB_PREFIX")."finance_type on ".C("DB_PREFIX")."finance_type.id=".C("DB_PREFIX")."finance.finance_type")
		->join("left join ".C("DB_PREFIX")."currency on ".C("DB_PREFIX")."currency.currency_id=".C("DB_PREFIX")."finance.currency_id")
		->limit($Page->firstRow.','.$Page->listRows)
		->where($finance_where)
		->order('add_time desc')
		->select();
		$this->assign('finance',$finance);
		$this->assign('page_finance',$show);
		$this->display();
	}
	//获取已经挖出的卡莱币数量
	private function getWachuCountByLogId($log_id,$uid,$count,$type=61){
		$where['log_id']=$log_id;
		$where['member_id']=$uid;
		$where['type']=$type;
		$list=M('KFinance')->where($where)->sum('money');
		if($list>=$count){
			M('Kuangji_log')->where('member_id='.$uid.' and id='.$log_id)->setField('status',1);
			$list=$count;
		}
		if($list){
			return $list;
		}else{
			return 0;
		}
	}
		//矿机购买
	public function buy(){
		//二级密码验证
    	$this->checkTwoPwd();
    	session('checkTwoPwd_success',null);
		$list=M('Kuangji')->order('kuangji_id')->where('type=1 and status=1')->select();
		$this->assign('list',$list);
		$this->display();
	}
	//购买矿机处理
	public function buy_kuangji(){
		$id=I('post.id');
		if(!$id){
			$data['status']=-1;
			$data['info'] = "参数错误,请稍后尝试";
			$this->ajaxReturn($data);
		}
		$kuangji=M('Kuangji')->where('kuangji_id='.$id)->find();
		if(!kuangji){
			$data['status']=-2;
			$data['info'] = "参数错误,请稍后尝试";
			$this->ajaxReturn($data);
		}
		if($this->member['is_kaitong']!=1){
			$data['status']=-2;
			$data['info'] = "请先激活后购买矿机";
			$this->ajaxReturn($data);
		}
		if($this->member['xnb']<$kuangji['price']){
			$data['status']=-3;
			$data['info'] = "尊敬的卡莱币矿主，您的余额不足，正在跳转充值页面。。。或联系上级进行报单";
			$this->ajaxReturn($data);
		}
		$log=M('Kuangji_log')->where('member_id='.session('USER_KEY_ID'))->find();
// 		if($log){
// 			$data['status']=-4;
// 			$data['info'] = "已经购买矿机,请不要重复购买";
// 			$this->ajaxReturn($data);
// 		} 
		if(empty($log)){
			$up=$this->getUpMemberByZhuceId($this->member['zhuceid']);
			foreach ($up as $k=>$v){
				M('Member')->where('member_id='.$v['member_id'])->setInc('tuanduinum',1);
				//echo M('Member')->_sql();
				if($v['boss']<5){
					$next_tuan_num=$this->mills['bosstuandui'.($v['boss']+1)];
					$next_tuijian_num=$this->mills['bosstuijian'.($v['boss']+1)];
					if($v['tuanduinum']+1>=$next_tuan_num){
						$num=M('Member')->where('zhuceid='.$v['member_id'].' and is_kaitong=1')->count();
						if($num>=$next_tuijian_num){
							M('Member')->where('member_id='.$v['member_id'])->setInc('boss',1);
						}
					}
				}
			}
		} 
		$kuangji_info=M('Kuangji')->where('kuangji_id ='.$id)->find();
		//购买矿机，添加日志
		$data['kid']=$id;
		$data['member_id']=session('USER_KEY_ID');
		$data['buy_time']=time();
		$data['add_time']=time();
		$data['backday']=$kuangji_info['usetime'];
		$data['useday']=$kuangji_info['usetime'];
		$data['status']=0;
		$r=M('Kuangji_log')->add($data);
		unset($data);
		if(!$r){
			$data['status']=-5;
			$data['info'] = "购买失败，请稍后尝试";
			$this->ajaxReturn($data);
		}
		//扣钱，添加财务日志
		M('Member')->where('member_id='.session('USER_KEY_ID'))->setDec('xnb',$kuangji['price']);
		addKFinanceLog($this->member['member_id'],51,'购买矿机扣除人民币'.$kuangji['price'],$kuangji['price'],2,0);
		//产生上级业绩
// 		$this->addUpUserYeji($this->member['pid'],$kuangji['price']);
		
		$data['status']=1;
		$data['info'] = "购买成功";
		$this->ajaxReturn($data);
	}
	//增加上级业绩
	private function addUpUserYeji($pid,$yeji){
		$list=M('Member')->where('member_id='.$pid." and status=1")->find();
		if(!$list){
			return;
		}else{
			M('Member')->where('member_id='.$pid)->setInc('yeji_'.$list['quyu'],$yeji);
			M('Member')->where('member_id='.$pid)->setInc('yeji',$yeji);
			$this->addUpUserYeji($list['pid'], $yeji);
		}
		
	}
}