<?php
namespace Kuang\Controller;
use Kuang\Controller\KuangController;
class CeshiController extends KuangController {
	//市场分享奖金
	public function index(){
		//入金人id
		$id = 42;
		$user = M('Member')->where('member_id='.$id)->find();
		
		$money = 20000;
// 		$bili = $this->config['fenxiang_1'];
// 		dump($bili);
// 		die();
// 		$this->fenxiangjiang($user,$money);

// 		//先触发奖励在为上线加业绩
// 		$this->fuwuJiang($user,$money);
// 		//为上线添加业绩
// 		$this->addYeJi($user,$money);
		$upuser=$this->getUpUserByTuijianId($user['zhuceid'],9999);
		$this->jiChaJiang($upuser,$money,$user['name']);

	}
	/**
	 * 分享奖方法     就是推荐奖
	 * @param unknown $user 入金人
	 * @param unknown $money 入金金额
	 */
	public function fenxiangjiang($user,$money){
		$id = $user['zhuceid'];
		$list = $this->getUpUserByTuijianId($id,3);
		for($i=0;$i<count($list);$i++){
			if($list[$i]['status'] ==0){
				continue;
			}
			if ($list[$i]['status']== -1) {
				continue;
			}
			if($list[$i]['level'] == 0){
				continue;
			}
			$j = $i+1;
			if($list[$i]['level']>$i){
				//才会获得推荐奖
				$bili = $this->config['fenxiang_'.$j]*0.01;
				dump($bili);
				$jiangjin = $money*$bili;
				//加钱
				$res = M('Member')->where('member_id='.$list[$i]['member_id'])->setInc('integral',$jiangjin);
				//添加财务日志
				$re = $this->addFinance($list[$i]['member_id'],24,"市场分享奖奖励，来自".$user['name'],$jiangjin,1,2);
			}
		}
		return true;
	}
	
	/**
	 * 市场服务奖
	 * @param unknown $user
	 * @param unknown $money
	 * @return unknown
	 */
	public function fuwuJiang($user,$money){
		$id = $user['zhuceid'];
		$list = $this->getUpUserByTuijianId($id,9999);
		for($i=0;$i<count($list);$i++){
			$level = $list[$i]['fenhong_level'];
			$bili = $this->config['fenhong_bili_'.$level]*0.01;
			$jiangjin = $money*$bili;
			//加服务奖
			$res[] = M('Member')->where('member_id='.$list[$i]['member_id'])->setInc('integral',$jiangjin);
			//添加财务日志
			$re[] = $this->addFinance($list[$i]['member_id'],25,"市场服务奖奖励，来自".$user['name'],$jiangjin,1,2);
		}
		return $res;
		
	}
	/**
	 * 为上线增长业绩
	 * @param 入金的会员信息 $user
	 * @param 入金金额 $money
	 */
	public function addYeJi($user,$money){
		$id = $user['zhuceid'];
		$list = $this->getUpUserByTuijianId($id,9999);
		for($i=0;$i<count($list);$i++){
			//为上线加业绩
			$res[] = M('Member')->where('member_id='.$list[$i]['member_id'])->setInc('yeji',$money);
			//升级方法
			$this->upFenHongLevel($list[$i]['member_id']);
		}
		return $res;
	}
	/**
	 * 服务奖等级升级
	 * @param 升级人id $id
	 * @return unknown
	 */
	public function upFenHongLevel($id){
		for($i=6;$i>0;$i--){
			$fenhong = $this->config['fenhong_'.$i]*10000;
			$fenhong_num = $this->config['fenhong_num_'.$i];
			$map['yeji'] = $fenhong;
			$map['zhuceid'] = $id; 
			$count = M('Member')->where($map)->count();
			if($count>$fenhong_num){
				//满足条件升级
				$data['fenhong_level'] = $i;
				$res = M('Member')->where('member_id='.$id)->save($data);
				break;
			}			
		}
		return $res;
	}
	/**
	 *	查询会员的推荐人
	 * @param unknown $tuijianid
	 * @param unknown $lv 需要查几代上线
	 * @param number $k 默认0
	 * @return Ambigous <unknown, mixed, boolean, NULL, multitype:, string>
	 */
	protected function getUpUserByTuijianId($id,$lv,$k=0){
		$user=M("Member")->where("member_id='$id'")->find();
		$k++;
		if (!empty($user)) {
			$item[]=$user;
			if ($k<$lv) {
				$r=$this->getUpUserByTuijianId($user['zhuceid'],$lv,$k);
				foreach ($r as $v){
					if (!empty($v)){
						$item[]=$v;
					}
				}
			}
		}
		return $item;
	}
	
	/**
	 * 市场服务奖
	 * 极差奖
	 * @param array 被激活人上线 $user
	 * @param 被激活人的金额 $money
	 */
	public function jiChaJiang($user,$money,$name){
		$jichaleavel=0;
		$jichabili=0;
		dump($money);
		dump($user);
		foreach ($user as $k=>$v){
			//下面是极差奖
			if($v['fenhong_level']>0&&$v['fenhong_level']>$jichaleavel){
				$jichaleavel=$v['fenhong_level'];
				$jichamoney=$money*($this->config['fenhong_bili_'.$v['fenhong_level']]*0.01-$jichabili);
				dump($jichamoney);
				$jichabili=$this->config['fenhong_bili_'.$v['fenhong_level']]*0.01;
				dump($jichabili);
				// 				$this->addUserJiangjin($v,$jichamoney);
				M('Member')->where('member_id='.$v['member_id'])->setInc('integral',$jichamoney);
// 				$this->addFinance($v['member_id'], $jichamoney, $v['uname'].'消费获得极差奖');
				$this->addFinance($v['member_id'],32,"市场服务奖奖励，来自".$name,$jichamoney,1,2);
// 				$up=M("Member")->where("member_id='".$v['zhuceid']."'")->find();
			}
		}
	}
}