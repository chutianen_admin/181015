<?php
namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
class Finance extends Base {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    //组织销售报告
    public function index(){
    	
    	$finance_type = Db::name('App_finance_type')->where(array('status'=>1))->select();
    	foreach($finance_type as $k=>$v){
    		$arr[$k] = $finance_type[$k]['id'];
    	}
    	$session = session::get();
    	$where['member_id'] = $session['USER_KEY_ID'];
        $where['finance_type'] = array('in',$arr);
        $type = input('type');
        if($type){
            $where['finance_type'] = $type;
        }
        $finance = Db::table('wst_app_finance')->alias('a')
    									   ->join('App_finance_type w','a.finance_type = w.id')
    									   ->order('add_time desc')
    									   ->where($where)->paginate(20,false,['query'=>input()]);
        $page = $finance->render();
        $finance = $finance->all();
//     	dump($finance);
        $this->assign('page',$page);
    	$this->assign('finance_type',$finance_type);
    	$this->assign('list',$finance);
        return $this->fetch();
    }
    //收入总结报告
    public function income(){
    	return $this->fetch();
    }
    //收入明细报告
    public function incomeDetails(){
    	return $this->fetch();
    }
    //余额报告
    public function balance(){
    	return $this->fetch();
    }
    
    
}