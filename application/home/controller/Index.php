<?php
namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
class Index extends Base {
 	public function _initialize(){
 		parent::_initialize();
 	}
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}

	/**
	 * 首页显示
	 */
	public function index(){
		$session = session::get();
		//用户账户信息
		$uid = $session['USER_KEY_ID'];
		$member = Db::name('Users')->where(array('userId'=>$uid))->find();
		$tuijian_user = Db::name('Users')->where(array('userId'=>$member['zhuceid']))->find();
		$jiedian_user = Db::name('Users')->where(array('userId'=>$member['jiedianid']))->find();
		//钱包信息
		$account_type = Db::name('App_account_type')->select();
		$where['member_id'] = $_SESSION['think']['USER_KEY_ID'];
		$member_account =Db::name('App_member_account')->where($where)->find();
		for($i=1;$i<count($account_type)+1;$i++){
			$arr[$i] = Db::name('App_account_type')->field('name')->where(array('id'=>$i))->find();
			$arr[$i]['num'] = $member_account['account_type_'.$i];  
		}
		$level = Db::name('App_level')->where(array('id'=>$member['level']))->find();
		
		$map['member_id'] = $uid;
		$map['status'] = 0;
		$line = Db::name('Line')->where($map)->find();
		
		$member_relation = Db::name('App_member_relation')->where($where)->find();
		// 		dump($member_relation);
		$line_arr['order_num'] = $member_relation['paihao'];
		//复投次数
		$map_fu['member_id'] = $uid;
		$map_fu['num'] = 0;
		$line_fu = Db::name('Line')->where($map_fu)->select();
		$line_arr['futou_num'] = count($line_fu)-1;
		//还需多少人才能出局
		$map_two['status'] = 0;
		$line_now = Db::name('Line')->where($map_two)->order('id asc')->find();
// 		dump($line_now);
// 		dump($line);
		$line_arr['need_num'] = $line['order_num'] - $line_now['order_num'] + 1;
		//出局次数
		$map_chuju['member_id'] = $uid;
		$map_chuju['num'] = array('neq',0);
		$line_chuju = Db::name('Line')->where($map_chuju)->select();
		$line_arr['chuju_num'] = count($line_chuju);
// 		dump($line_chuju);
		
		$date = date('Y-m-d');
		$time = date('H:m:s');
		$art = Db::name('App_article')->limit(5)->select();
		$this->assign('art',$art);
		$this->assign('date',$date);
		$this->assign('time',$time);
		$this->assign('level',$level);
		$this->assign('member',$member);
		$this->assign('line_arr',$line_arr);
		$this->assign('arr',$arr);
        return $this->fetch();
     }
     /**
      * 获得左侧底部节点
      * @param unknown $uid
      */
     public function getLeftMember($uid){
     	$map['jiedianid'] = $uid;
     	$map['quyu'] = 'left';
     	$left = Db::name('App_member_relation')->where($map)->find();
     	for($i = 0;$i<9999;$i++){
     		if($i == 0){
     			$map_left['jiedianid'] = $left['member_id'];
     		}else{
     			$map_left['jiedianid'] = $relation_arr[$i-1]['member_id'];
     		}
     		$map_left['quyu'] = 'left';
     		$relation_arr[$i] =  Db::name('App_member_relation')->where($map_left)->find();
     		if(empty($relation_arr[$i])){
     			if(!empty($relation_arr[$i-1])){
     				$aa = $relation_arr[$i-1];
     			}else{
     				$aa = $left;
     			}
     			break;
     		}else{
     			$left_arr[$i] = $relation_arr[$i];
     		}
     	}
     	$user = Db::name('Users')->where(array('userId'=>$aa['member_id']))->find();
     	return $user;
     }
     /**
      * 获得左侧底部节点
      * @param unknown $uid
      */
     public function getRighMember($uid){
     	$map['jiedianid'] = $uid;
     	$map['quyu'] = 'righ';
     	$righ = Db::name('App_member_relation')->where($map)->find();
     	for($i = 0;$i<9999;$i++){
     		if($i == 0){
     			$map_left['jiedianid'] = $righ['member_id'];
     		}else{
     			$map_left['jiedianid'] = $relation_arr[$i-1]['member_id'];
     		}
     		$map_left['quyu'] = 'left';
     		$relation_arr[$i] =  Db::name('App_member_relation')->where($map_left)->find();
     		if(empty($relation_arr[$i])){
     			if(!empty($relation_arr[$i-1])){
     				$aa = $relation_arr[$i-1];
     			}else{
     				$aa = $righ;
     			}
     			break;
     		}else{
     			$left_arr[$i] = $relation_arr[$i];
     		}
     	}
     	$user = Db::name('Users')->where(array('userId'=>$aa['member_id']))->find();
     	return $user;
     }
     
     
	private function getCountFromFinanceByMember($uid,$type){
		$where['member_id']=$uid;
		$where['type']=$type;
		$where['currency_id']=-1;
// 		$list=M('KFinance')->where($where)->sum('money');
		$list=0;
		if($list){
			return $list;
		}else{
			return '0.00000000';
		}
	}
	
	public function shenqing(){
		$id = I('id');
		$arr['status'] = 1;
		$res = M('Chenghaojiang')->where(array('id'=>$id))->save($arr);
		if($res){
			$data['info'] = "兑换成功，等待审核";
			$data['status'] = 1;
			$this->ajaxReturn($data);
		}else{
			$data['info'] = "系统繁忙，请稍后重试";
			$data['status'] = 0;
			$this->ajaxReturn($data);
		}
	}
	
	public function xieyi(){
		$cb=I('cb');
		if($cb == 'true'){
			$xx=M("Member")->where(['member_id'=>$_SESSION['USER_KEY_ID']])->find();
			dump($xx);
			$re=M("Member")->where(['member_id'=>$_SESSION['USER_KEY_ID']])->setField("xieyi",'1');
			dump($re);
			if($re){
				$msg['status']=1;
// 				$msg['info']="已阅读";
				$this->ajaxReturn($msg);
			}
		}
	}
	public function chakan(){
		$map['id'] = I('id');
		$list = M('Chenghaojiang')->where($map)->find();
		$member = M('Member')->where(array('member_id'=>$list['member_id']))->find();
		$arr['alt'] = '权益证';
		$arr['pid'] = $member['member_id'];
		$arr['src'] = $member['pic'];
		$arr['thumb'] = $member['pic'];

		if($arr){
			$this->ajaxReturn($arr);
		}else{
			$this->ajaxReturn($arr);
		}	
	}
	
	public function detailed(){
		$type = I('type');
		//直推奖
		if($type == 1){
			$map['type'] = 2;
			
		}
		//对碰奖
		if($type == 2){
			$map['type'] = 6;
		}
		//领导奖
		if($type == 3){
			$map['type'] = 7;
		}
		//交易奖
		if($type == 4){
			$map['type'] = 8;
		}
		$map['member_id'] = $_SESSION['USER_KEY_ID'];
		$list = M('K_finance')->where($map)->order('add_time desc')->select();
		foreach($list as $k=>$v){
			$type = $list[$k]['type'];
			$type_name = M('Finance_type')->where(array('id'=>$type))->find();
			$list[$k]['type_name'] =$type_name['name'];
			$list[$k]['time'] = date('Y-m-d H:i:s',$list[$k]['add_time']);
			
			$aa .= '<tr><td class="row">'.$list[$k]['type_name'].'</td>
				<td class="row">'.$list[$k]['content'].'</td>
				<td class="row">'.$list[$k]['money'].'</td>
				<td class="row">'.$list[$k]['time'].'</td>
				<td class="row">'.$list[$k]['ip'].'</td></tr>';
		
		}
		
		$data['title'] = '<table id="mytable" cellspacing="0"><caption></caption>
				<tr><th scope="col">分类</th><th scope="col">内容</th>
				<th scope="col">金额</th><th scope="col">时间</th>
				<th scope="col">ip</th></tr>
				'.$aa.'</table>';
		if($list){
			$this->ajaxReturn($data);
		}else{
			$data['info'] = "暂无数据";
			$data['status'] = -1;
			$this->ajaxReturn($data);
		}
	}
	
	public function sousuo(){
		$name = I('name');
		if(empty($name)){
			$this->display('Index:index');
		}else{
			$where['name']=$name;
			$list = M('Kuaijie')->where($where)->find();
			if($list){
				$this->redirect($list['url']);
			}else{
				$this->display('Index:index');
			}
		}
	}
}