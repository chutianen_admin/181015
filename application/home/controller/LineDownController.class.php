<?php
namespace Home\Controller;
use Home\Controller\HomeController;

class LineDownController extends HomeController {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    public function lineDownPay(){
    	//获得用户信息
    	$member_id = $_SESSION['USER_KEY_ID'];
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	$goods_user = M('Goods_member')->where(array('member_id'=>$member_id))->find();
    	//注册积分持有量
    	$member_account = M('Member_account')->where(array('member_id'=>$member_id))->find();
    	$this->assign('member_account',$member_account);
    	$this->assign('goods_user',$goods_user);
    	$this->assign('member',$member);
    	$this->display();
    }
    /**
     * 股票挂售
     */
    public function lineSell(){
    	//获得用户信息
    	$member_id = $_SESSION['USER_KEY_ID'];
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	$goods_user = M('Goods_member')->where(array('member_id'=>$member_id))->find();
    	//注册积分持有量
    	$member_account = M('Member_account')->where(array('member_id'=>$member_id))->find();
    	//求现在股票金额
    	$where['status'] = 1;
    	$where['type'] = 'buy';
    	$last_order = M('Orders')->where($where)->order('add_time desc')->find();
    	if(empty($last_order)){
    		$last_order['price'] = 1;
    	}
    	
    	$shizhi = $last_order['price']*$goods_user['num'];
    	
    	//查询下1股的价格
    	$aaa = $this->getNowPrice(1);
    	$price = $aaa[0]['money'];
    	$this->assign('shizhi',$shizhi);
    	$this->assign('last_order',$last_order);
    	$this->assign('member_account',$member_account);
    	$this->assign('price',$price);
    	$this->assign('goods_user',$goods_user);
    	$this->assign('member',$member);
    	$this->display();
    }
    /**
     * 股票出售方法
     */
    public function sellOrders(){
    	$member_id = $_SESSION['USER_KEY_ID'];
    	$num = I('num');
    	$password = I('password');
    	//用户信息
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	if(empty($member)){
    		$data['info'] = '请先登录再进行此操作';
    		$data['status'] = -1;
    		$this->ajaxReturn($data);
    	}
    	if($member['pwdtrade'] != md5($password)){
    		$data['info'] = '二级密码不正确';
    		$data['status'] = -2;
    		$this->ajaxReturn($data);
    	}
    	$switch = M('Switch')->where(array('member_id'=>$member_id))->find();
    	if($switch['out_limit'] == 1){
    		$data['info'] = '您已出局无法继续挂单';
    		$data['status'] = -5;
    		$this->ajaxReturn($data);
    	}
    	 
    	//判断卖入数量
    	$goods_member = M('Goods_member')->where(array('member_id'=>$member_id))->find();	
    	if(is_int($num)){
    		$data['status']=-3;
    		$data['info']='数量有误';
    		$this->ajaxReturn($data);
    	}
    	//数量小于零
    	if($num<=0){
    		$data['status']=-4;
    		$data['info']='数量有误';
    		$this->ajaxReturn($data);
    	}
    	if($num>$goods_member['trade_num']){
    		$data['status']=-5;
    		$data['info']='数量不足';
    		$this->ajaxReturn($data);
    	}
	
    	//求现在股票金额
    	$money = $this->getNowPrice($num);
    	//     	dump($money);
    	//     	die();
    	$j = count($money);
    	$h = $j-1;
    	for($i=0;$i<$j;$i++){
    		$trade_order_money += $money[$i]['all_money'];
    		if($i == $h){
    			$price = $money[$i]['money'];
    		}
    	}
    	//账户积分信息
//     	$member_account = M('member_account')->where(array('member_id'=>$_SESSION['USER_KEY_ID']))->find();
//     	if(floatval($trade_order_money) > floatval($member_account['account_type_1'])){
//     		$data['status']=-7;
//     		$data['info']='资金余额不足';
//     		$this->ajaxReturn($data);
//     	}
    	$arr['member_id'] = $member_id;
    	$arr['type'] = 'sell';
    	$arr['price'] =  $price;
    	$arr['num'] = $num;
    	$arr['add_time'] = time();
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	$arr['ratio_id'] = $ratio['id'];
    	$arr['all_money'] = $trade_order_money;
    	$res = M('Orders')->add($arr);
    	if($res){
    		//Ratio表添加数量
    		M('Ratio')->where(array('status'=>0))->setInc('num',$num);
			//持有量减少
    		$re =M('Goods_member')->where(array('member_id'=>$_SESSION['USER_KEY_ID']))->setDec('trade_num',$num);
    		$data['info'] = '成功';
    		$data['status'] = 1;
    		$this->ajaxReturn($data);
    	}else{
    		$data['info'] = '网络异常';
    		$data['status'] = -2;
    		$this->ajaxReturn($data);
    	}
    }
    
    
   
    /**
     * 交易大厅购买功能
     */
    public function buy(){
    	$id = I('id');
    	//订单信息
    	$order =M('Orders')->where(array('order_id'=>$id))->find();
    	if($order['status'] == 1){
    		$data['info'] = '此订单已锁定请从新选择';
    		$data['status'] = -2;
    		$this->ajaxReturn($data);
    	}
    	
    	$member_id = $_SESSION['USER_KEY_ID'];
    	//用户信息
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	if(empty($member)){
    		$data['info'] = '请先登录再进行此操作';
    		$data['status'] = -1;
    		$this->ajaxReturn($data);
    	}
    	$switch = M('Switch')->where(array('member_id'=>$member_id))->find();
    	if($switch['out_limit'] == 1){
    		$data['info'] = '您已出局无法继续挂单';
    		$data['status'] = -5;
    		$this->ajaxReturn($data);
    	}
    	$arr['member_id'] = $member_id;
    	$arr['type'] = 'buy';
    	$arr['price'] =  $order['price'];
    	$arr['num'] =  $order['num'];
    	$arr['add_time'] = time();
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	$arr['ratio_id'] = $ratio['id'];
    	$arr['all_money'] = $order['all_money'];
    	$arr['status'] = 1;
    	$res = M('Orders')->add($arr);
    	if($res){
    		//成功加入交易表
    		$nn['status'] = 1;
			$r[] = M('Orders')->where(array('order_id'=>$id))->save($nn);
    		$r[] = $this->addTrade($member_id,$order['member_id'],'1',$order['num'],$order['all_money'],$res,$order['order_id']);
    		
    		$data['info'] = '成功';
    		$data['status'] = 1;
    		$this->ajaxReturn($data);
    	}else{
    		$data['info'] = '网络异常';
    		$data['status'] = -2;
    		$this->ajaxReturn($data);
    	}   	
    }

    /**
     * 凭证上传
     */
    public function picUpLond(){
    	if($_FILES["pic"]["tmp_name"]){
    		$_POST['pic']=$this->upload($_FILES["pic"]);
    	}
    	$id = I('id');
    	$res = M('Trade')->where(array('id'=>$id))->save($_POST);
		$this->redirect('myBuying');
    }
    /**
     * 查看打款凭证
     */
    public function pic(){
    	$id =I('id');
    	$list = M('Trade')->where(array('id'=>$id))->find();
    	$arr['title'] = '凭证';
    	$arr['id'] = 123;
    	$arr['start'] = 0;
    	$aa['alt'] = '凭证';
    	$aa['pid'] =  666;
    	$aa['src'] = $list['pic'];
    	$aa['thumb'] = '';
    	$arr['data'] = [$aa];
    	if($list){
    		echo json_encode($arr);
    	}else{
    		$data['info'] = '失败';
    		$data['status'] = -1;
    		$this->ajaxReturn($data);
    	}
    	
    	
    	
    }
    /**
     * 卖出记录
     */
    public function mySelling(){
    	$where['sell_member'] = $_SESSION['USER_KEY_ID'];
    	$where['goods_id'] = 1;
    	$count = M('Trade')->where($where)->count();
    	$Page    = new \Think\Page($count,15);
    	$show    = $Page->show();
    	$list = M('Trade')->where($where)->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	foreach($list as $k=>$v){
    		$buy_member = M('Member')->where(array('member_id'=>$list[$k]['sell_member']))->find();
    		$list[$k]['sell_name'] = $buy_member['name'];
    		$list[$k]['phone'] = $buy_member['phone'];
    	}
    	//     	dump($list);die();
    	$this->assign('list',$list);
    	$this->assign('page',$show);
    	
    	
    	$this->display();
    }
    public function myLineSelling(){
    	$where['type'] = 'sell';
    	$where['status'] = 0;
    	 
    	$where['member_id'] = $_SESSION['USER_KEY_ID'];
    	$count = M('Orders')->where($where)->count();
    	$Page    = new \Think\Page($count,20);
    	$show    = $Page->show();
    	$list = M('Orders')->where($where)->select();
    	foreach($list as $k=>$v){
    		$member = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->find();
    		$list[$k]['account'] = $member['account'];
    		$list[$k]['name'] = $member['name'];
    	}
    	//     	dump($list);die();
    	$this->assign('list',$list);
    	$this->display();
    }
    /************************************************************************************************/
 

    /**
     * 股票购买方法
     */
    public function buyOrders(){
    	$member_id = $_SESSION['USER_KEY_ID'];
    	$num = I('num');
    	$password = I('password');
    	//用户信息
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	if(empty($member)){
    		$data['info'] = '请先登录再进行此操作';
    		$data['status'] = -1;
    		$this->ajaxReturn($data);
    	}
    	if($member['pwdtrade'] != md5($password)){
    		$data['info'] = '二级密码不正确';
    		$data['status'] = -2;
    		$this->ajaxReturn($data);
    	}
    	$switch = M('Switch')->where(array('member_id'=>$member_id))->find();
    	if($switch['out_limit'] == 1){
    		$data['info'] = '您已出局无法继续挂单';
    		$data['status'] = -5;
    		$this->ajaxReturn($data);
    	}
    	
    	//判断买入数量
    	if(is_int($num)){
    		$data['status']=-3;
    		$data['info']='数量有误';
    		$this->ajaxReturn($data);
    	}
    	//数量小于零
    	if($num<=0){
    		$data['status']=-4;
    		$data['info']='数量有误';
    		$this->ajaxReturn($data);
    	}
    	//求现在股票金额
    	$money = $this->getNowPrice($num);
//     	dump($money);
//     	die();
    	$j = count($money);
    	$h = $j-1;
    	for($i=0;$i<$j;$i++){
    		$trade_order_money += $money[$i]['all_money'];
    		if($i == $h){
    			$price = $money[$i]['money'];
    		}
    	}
    	//账户积分信息
    	$member_account = M('member_account')->where(array('member_id'=>$_SESSION['USER_KEY_ID']))->find();
    	if(floatval($trade_order_money) > floatval($member_account['account_type_1'])){
    		$data['status']=-7;
    		$data['info']='资金余额不足';
    		$this->ajaxReturn($data);
    	}
    	$arr['member_id'] = $member_id;
    	$arr['type'] = 'buy';
    	$arr['price'] =  $price;
    	$arr['num'] = $num;
    	$arr['add_time'] = time();
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	$arr['ratio_id'] = $ratio['id'];
    	$arr['all_money'] = $trade_order_money;
    	$res = M('Orders')->add($arr);
    	if($res){
    		//Ratio表添加数量
    		M('Ratio')->where(array('status'=>0))->setInc('num',$num);
    		
    		//计算金额
    		$trade_money = $trade_order_money;
    		//账户余额
    		$r[] = $this->subMoney($member_id,$trade_money,'account_type_1');
    		$data['info'] = '成功';
    		$data['status'] = 1;
    		$this->ajaxReturn($data);
    	}else{
			$data['info'] = '网络异常';
			$data['status'] = -2;
			$this->ajaxReturn($data);
		}
    }
  	
    
    
    
    public function aa(){
    	$aa = $this->getNowPrice(201);
    	dump($aa);
    	
    }
    /**
     * 获得当前股票交易金额
     * @param unknown $now_num
     * @return number
     */
    public function getNowPrice($now_num){//200
    	$where['type'] = 'sell';
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	$where['ratio_id'] = $ratio['id'];
  		//当前比例的数量
    	$num = $ratio['num'];
    	$ttt = $this->ratio();
    	if($ttt){
    		$price = $ttt;
    	}else{
    		$order = M('Orders')->where($where)->order('add_time desc')->find();
    		$price = $order['price'];
    	}
		$base = $this->config['vr_danwei'];
    	$bili = $this->config['vr_money'];
    	//取余
    	$r = fmod($num,$base); 
    	//没满足第一次涨价的股票量
    	$a = $base-$r;		
    	if($now_num>$a){		        
    		$f = $a;
    	}else{
    		$f = $now_num;		//1  
    	}
    	//满足涨价的股票量
    	$b = $now_num-$f;   	
    	//涨了几次价格 (取整)
    	$c = ceil($b/$base);  
    	$d = fmod($b,$base);  
    	for($i=0;$i<$c+1;$i++){
    		if($i == 0){
    			$arr[$i]['num'] = $f;
    				//判断是否涨价
    				if($r == 0){
    					if($num !=0){
    						$now_bili = $bili;
    					}else{
    						$now_bili = $i*$bili;
    					}
    				}else{
    					$now_bili = $i*$bili;
    				}
    		}else{
    			if($i == $c){
    				if($d == 0){
    					//整除的
    					$arr[$i]['num'] = $base;
    				}else{
    					$arr[$i]['num'] = $d;
    				}
    				
    			}else{
    				$arr[$i]['num'] = $base;
    			}
    			$now_bili =	($i+1)*$bili;
    		}
    		$arr[$i]['money'] = $price+$now_bili;
    		$arr[$i]['all_money'] = $arr[$i]['money']*$arr[$i]['num'];
    	}
    	return $arr;
    }
    
    public function ratio(){
    	//查看股票进程表
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	if($ratio){
    		if($ratio['num'] == 0){
    			$price = $ratio['first_price'];
    		}else{
    			return false;
    		}
    	}	
//     	echo '111111111111';
//     	dump($price);
    	return $price;
    }
    
    
    
    
    
  
	
   
    /**
     * 获得当前的st价格
     */
    public function getNowStPrice(){
    	$st_up_num  = $this->config['st_up_num'];
    	$st_up_bili = $this->config['st_up_bili'];
    	$all_money = M('Trade')->sum('num');
    	$num = floor($all_money/$st_up_num);
    	$money= $num*$st_up_bili;
    	$first_order = M('Orders')->where(array('order_id'=>1))->find();
    	$now_price = $first_order['price']+$money;
    	return $now_price; 
    }
    /**
     * 为用户加钱
     * @param 用户id $member_id
     * @param 金额   $money
     * @param 类型   $type rmb forzen
     * @return unknown
     */
    public function addMoney($member_id,$money,$type){
    	$res = M('Member_account')->where(array('member_id'=>$member_id))->setInc($type,$money);
    	return $res;
    }
    /**
     * 为用户减钱
     * @param 用户id $member_id
     * @param 金额   $money
     * @param 类型   $type
     * @return unknown
     */
    public function subMoney($member_id,$money,$type){
    	$res = M('Member_account')->where(array('member_id'=>$member_id))->setDec($type,$money);
    	return $res;
    }
   
    /**
     *
     * @param 买入用户id  $buy_member
     * @param 卖出用户id  $sell_member
     * @param 商品id     $goods_id
     * @param 数量                     $num
     * @param 金额                     $price
     * @param 买入订单id  $order_buy_id
     * @param 卖出订单id  $order_sell_id
     * @return unknown
     */
    public function addTrade($buy_member,$sell_member,$goods_id,$num,$price,$order_buy_id,$order_sell_id){
    	$data['buy_member'] = $buy_member;
    	$data['sell_member'] = $sell_member;
    	$data['goods_id'] = $goods_id;
    	$data['num'] = $num;
    	$data['price'] = $price;
    	$data['order_buy_id'] = $order_buy_id;
    	$data['order_sell_id'] = $order_sell_id;
    	$data['add_time'] = time();
    	$data['status'] = 0;
    	// 		dump($data);
    	$res = M('Trade')->add($data);
    	return $res;
    }

    /**
     * 用户商品持有量
     * @param 用户id $member_id
     * @param 商品id $goods_id
     * @param 商品数量   $num
     * @param 买卖   $type sell/buy
     * @return unknown
     */
    public function addGoodsUser($member_id,$goods_id,$num,$type){
    	$where['member_id'] = $member_id;
    	$where['goods_id'] = $goods_id;
    	$goods_user = M('Goods_member')->where($where)->find();
    	if($type == 'buy'){
    		if($goods_user){
    			$res = M('Goods_member')->where($where)->setInc('num',$num);
    		}else{
    			$data['num'] = $num;
    			$data['member_id'] = $member_id;
    			$data['goods_id'] = $goods_id;
    			$data['status']=0;
    			$res = M('Goods_member')->add($data);
    
    		}
    	}
    	if($type == 'sell'){
    		if($goods_user){
    			$res = M('Goods_member')->where($where)->setDec('num',$num);
    		}
    	}
    	return $res;
    }
    /**
     * 买入记录页面
     */
    public function mairu(){
    	$member_id = $_SESSION['USER_KEY_ID'];
    	//查看自己购买的股票记录
    	$list = M('Trade')->where(array('buy_member'=>$member_id))->select();
    	foreach($list as $k=>$v){
    		$list[$k]['money'] = $list[$k]['price']/$list[$k]['num'];
    	}
    	$this->assign('list',$list);
    	$this->display();
    }
    
    /**
     * 股票拆分页面
     */
    public function chaifen(){
    	
    	$this->display();
    }
	public function selling(){
		$member_id = $_SESSION['USER_KEY_ID'];
		$where['member_id'] = $member_id;
		$where['type'] = 'sell';
		$orders = M('Orders')->where($where)->select();
		foreach($orders as $k=>$v){
			$orders[$k]['all_money'] = $orders[$k]['num']*$orders[$k]['price'];
		}
		$this->assign('orders',$orders);
		$this->display();
	}
	/**
	 * 成交记录页面
	 */
	public function trade_list(){
		//查看自己完成的订单
		$map['member_id'] = $_SESSION['USER_KEY_ID'];
		$map['status'] = 1;
		$count      = M('Orders')->where($map)->count();// 查询满足要求的总记录数
		$Page       = new \Think\Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)
		$show       = $Page->show();// 分页显示输出
		
		
		$list = M('Orders')->where($map)->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);
		$this->assign('page',$show);// 赋值分页输出
		$this->display();
	}
	
	
	public function maichu(){
		$this->display();
	}
	/*********************************************************************2017.6.15*****************褚天恩********************************/
	/**
	 * 公排一条线
	 */
	public function line_list(){
		$where['status'] = 0;
		$line = M('Line')->where($where)->order('id asc')->find();
		$arr[] = $line['id']-7;
		$arr[] = $line['id']+7;
		$map['id'] = array('between',$arr);
		$list = M('Line')->where($map)->select();
		foreach($list as $k=>$v){
			$member = M('Member')->where(array('member_id'=>$list[$k]['member_id']))->find();
			$list[$k]['account'] =$member['account'];
		}
		$this->assign('list',$list);
		$this->display();
	}
	/**
	 * 历史记录
	 */
	public function myBuying(){
		$where['member_id'] = $_SESSION['USER_KEY_ID'];
		$count = M('Line')->where($where)->count();
		
		$Page    = new \Think\Page($count,15);
		$show    = $Page->show();
		$list = M('Line')->where($where)->order('id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		//     	dump($list);die();
		$this->assign('list',$list);
		$this->assign('page',$show);
		$this->display();
	}
	
	
	
	
	
	
	
}