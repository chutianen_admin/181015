<?php
namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
require_once (APP_PATH . 'api/RewardApi.php');
class Member extends Base {
    public function _initialize(){
        parent::_initialize();
       
    }
    /**
     * 系谱图页面
     */
	public function xipu(){
		$account = I('account');
		$session = session::get();
		if($account){
			$member = Db::name('Users')->where(array('loginName'=>$account))->find();
			$id= $member['userId'];
			if($member['userId'] < $session['USER_KEY_ID']){
				$this->error('无法查看此会员');
			}
		}else{
			$id = $session['USER_KEY_ID'];
		}
		$user = Db::name('Users')->where(array('userId'=>$id))->find();
		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$id))->find();
		$user['add_time'] = $member_relation['add_time'];
		$user['zhitui_num'] = $member_relation['zhitui_num'];
		$where['id'] = $member_relation['level'];
		$level = Db::name('App_level')->where($where)->find();
		$this->assign ('baodan_level',$level['name']);
		$this->assign ( 'user', $user );
		return $this->fetch();
	}
	
	/**
	 * 获得下线信息
	 */
	public function getInivt() {
		$username = $_POST ['username'];
		$list = db('Users')->where(array('loginName'=>$username))->find();
		$r = Db::name('App_member_relation')->where (array('zhuceid'=>$list['userId']))->select();
		foreach ( $r as $k => $v ) {
			$users = Db::name('Users')->where('userId='.$v['member_id'])->find();
			$r[$k]['add_time']  =   date('Y-m-d H:i:s',$users['createTime']);
			$r[$k]['loginName'] =   $users['loginName'];
			$where ['id'] = $r[$k]['level'];
			$level = db('App_level')->where($where)->find();
			$r[$k]['baodan'] = $level['name'];
		}
		if (! empty ( $r )) {
			$data ['status'] = 1;
			$data ['user'] = $r;
			return json ( $data );
		} else {
			$data ['status'] = 0;
			return json ( $data );
		}
	}
	/**
	 * 结构图
	 */
	public function jiegou(){
		$session = session::get();
		$member_id = I('myuser');
		$account = I('account');
		$type = I('type');
		
		if($account){
			$member = Db::name('Users')->where(array('loginName'=>$account))->find();
			$member_id = $member['userId'];
		}
		//上一层
		if($type){ 
			$id = I('id');
			$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$id))->find();
			$member_id = $member_relation['jiedianid'];
			if($member_id<$session['USER_KEY_ID']){
				$this->error('无法查看上线关系');
			}
		}
		$id = $member_id?$member_id:$session['USER_KEY_ID'];
		$where['member_id'] = $id;
		$this->assign('username',$id);
		$one = Db::table('wst_app_member_relation')
				->field('a.member_id,a.jiedianid,a.quyu,a.level,a.status,a.yeji_left,a.yeji_righ,b.userId,b.loginName as account')
				->alias('a')
				->join('wst_users b','a.member_id = b.userId')
				->where($where)
				->find();
		//根据用户status判断用户是否购买过资产包
		$one['sclass']=$this->getClassByUserStatus($one);
		$one['level_detail']=$this->getLevel($one['level']);
		$two_one=$this->getUperUsername($one['member_id'],left);//A区
		
		$two_two=$this->getUperUsername($one['member_id'],righ);//B区
		$three_one=$this->getUperUsername($two_one['member_id'],left);//A区
		$three_two=$this->getUperUsername($two_one['member_id'],righ);//B区
		$three_three=$this->getUperUsername($two_two['member_id'],left);//A区
		$three_four=$this->getUperUsername($two_two['member_id'],righ);//B区
		if ($_POST['level']==4) {
			$four_one=$this->getUperUsername($three_one['member_id'], left);
			$four_two=$this->getUperUsername($three_one['member_id'], righ);
			$four_three=$this->getUperUsername($three_two['member_id'], left);
			$four_four=$this->getUperUsername($three_two['member_id'], righ);
			$four_five=$this->getUperUsername($three_three['member_id'], left);
			$four_six=$this->getUperUsername($three_three['member_id'], righ);
			$four_seven=$this->getUperUsername($three_four['member_id'], left);
			$four_eignt=$this->getUperUsername($three_four['member_id'], righ);
		}
		$this->assign('one',$one);
		$this->assign('two_one',$two_one);
		$this->assign("two_two",$two_two);
		$this->assign("three_one",$three_one);
		$this->assign("three_two",$three_two);
		$this->assign("three_three",$three_three);
		$this->assign("three_four",$three_four);
		$this->assign("four_one",$four_one);
		$this->assign("four_two",$four_two);
		$this->assign("four_three",$four_three);
		$this->assign("four_four",$four_four);
		$this->assign("four_five",$four_five);
		$this->assign("four_six",$four_six);
		$this->assign("four_seven",$four_seven);
		$this->assign("four_eignt",$four_eignt);
		$this->assign('level',$_POST['level']);
		return $this->fetch();
	}
 	//获取当前人的下线
	private function getUperUsername($member_id,$quyu){
		$where['jiedianid'] = $member_id;
		$where['quyu']=$quyu;
		$list = Db::table('wst_app_member_relation')
				->field('a.member_id,a.jiedianid,a.quyu,a.level,a.status,a.yeji_left,a.yeji_righ,b.userId,b.loginName as account')
				->alias('a')
				->join('wst_users b','a.member_id = b.userId')
				->where($where)
				->find();
		if($list){
			$list['level_detail']=$this->getLevel($list['level']);
			$list['sclass']=$this->getClassByUserStatus($list);
		}
		return $list;
	}
	public function getLevel($level){
		$class = Db::name('App_level')->where(array('id'=>$level))->find();
		return $class;
	}
	
	//查找当前人，以当前人为起点
	private function getUserByusername($username,$quyu=''){
		$where['account']=$username;
		if(!empty($quyu)){
			$where['quyu']=$quyu;
		}
		$list=db('App_member')->where($where)->select();
		return $list;
	}
	//获取级别显示样式的class
	protected function getClassByUserStatus($member){
		if($member['status'] == 0){
			$status = 9;
		}else{
			$status = $member['level'];
		}
		switch ($status){
			case 0:$class='xpt_one';break;
			case 1:$class='xpt_zero';break;
			case 2:$class='xpt_two';break;
			case 3:$class='xpt_three';break;
			case 4:$class='xpt_four';break;
			case 5:$class='xpt_five';break;
			case 6:$class='xpt_six';break;
			case 7:$class='xpt_seven';break;
			case 8:$class='xpt_eight';break;
			default:$class='xpt_wjh';
		}
		return $class;
	}
	
	/**
	 * 转账记录
	 */
	public function transfer_list(){
		$Transfer = M('Transfer'); // 实例化User对象
		$where['member_id']=$_SESSION['USER_KEY_ID'];
		$count      = $Transfer->where($where)->count();// 查询满足要求的总记录数
		$Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
		$show       = $Page->show();// 分页显示输出
		// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
		$list = $Transfer->where($where)->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$show);// 赋值分页输出
		return $this->fetch();
	}
	
	/**
	 * 获得上线Line关系
	 * @param unknown $id
	 * @param unknown $lv
	 * @param number $k
	 * @return unknown
	 */
	public function getUpLine($id,$lv,$k=0){
		$where['id'] = $id;
// 		$where['status'] = 0;
		$member_line = Db::name("Line")->where($where)->find();
		$k++;
		if (!empty($member_line)) {
			$item[]=$member_line;
			if ($k<$lv) {
				$list = $this->getUpLine($member_line['up'],$lv,$k);
				for($i = 0;$i<count($list);$i++){
					if(!empty($list[$i])){
						$item[] = $list[$i];
					}
				}
			}
		}
		return $item;
	}
	
	
	
	
}