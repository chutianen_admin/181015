<?php
namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
require_once (APP_PATH . 'api/FinanceApi.php');
class Money extends Base {
    public function _initialize(){
        parent::_initialize();
    }
	//空操作
	public function _empty(){
		header("HTTP/1.0 404 Not Found");
		$this->display('Public:404');
	}
	public function email(){
		$my_info=$this->getMemberByMemberId($_SESSION['USER_KEY_ID']);
		$this->assign('my_info',$my_info);
		return $this->fetch();
	}
	public function emailKaluo(){
		$my_info=$this->getMemberByMemberId($_SESSION['USER_KEY_ID']);
		$this->assign('my_info',$my_info);
		return $this->fetch();
	}
	public function searchJihuo(){
		$list = $this->getMemberByMemberId($_SESSION['USER_KEY_ID']);
		$price = memberLevel($list['level']);
		$this->assign('list',$list);
		$this->assign('price',$price);
		return $this->fetch();
	}
	/******************************此段程序修改购买资产包 2016.12.20**************************************************/
	public function jihuoDetail(){
		if(IS_POST){
			$type = I('post.type');
			$btype = I('post.btype');
			$password = I('post.password','','md5');
			if(empty($type)){
				$this->error('请先选择购买类型');
			}
			if(empty($btype)){
				$this->error('请先选择支付类型');
			}
			if(empty($password)){
				$this->error('请先填写交易密码');
			}
			$my=$this->getMemberByMemberId($_SESSION['USER_KEY_ID']);
			
			if($my['type'] == 1){
				$this->error('请勿重复购买');
			}
			if($my['pwdtrade'] != $password){
				$this->error('交易密码不正确');
			}
			if($btype == 1){
				if($my['integral'] < 50000){
					$this->error('权益值不足');
				}
			
			}else{
				if($my['integral'] < 25000){
					$this->error('权益值不足');
				}
				if($my['rmb'] < 25000){
					$this->error('复投宝不足');
				}
			}
			$data['integral'] = 75000;
			$data['member_id'] = $_SESSION['USER_KEY_ID'];
			$data['btype'] = $btype;
			$data['zichanbao_id'] = $type;
// 			$data['time_shengyu'] = 50;
			$data['time_shengyu'] = $this->config['shifang_time'];
			$data['add_time'] = time();
			$data['status'] = 0;
			$re = M('Member_zichanbao')->add($data);
			if($re){
				if($btype == 1){
					M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->setDec('integral',50000);
				}else{
					M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->setDec('integral',25000);
					M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->setDec('rmb',25000);
				}
				//改变会员状态
				$aaaaa = M('Member')->where(array('member_id'=>$_SESSION['USER_KEY_ID']))->setField('type',1);
// 				dump($aaaaa);
// 				die();
				/**************加入日志*****************/
				$member_id = $_SESSION['USER_KEY_ID'];
				$typee = 30;
				$content = '购买5万资产包';
				if($btype == 1){
					$money = 50000;
					$money_type = 2;
					$currency_id = 2;
					$this->addFinance($member_id,$typee,$content,$money,$currency_id);  //给发送方加入日志
				}else{
					$money = 25000;
					$money_type = 2;
					$currency_id = 2;
					$this->addFinance($member_id,$typee,$content,$money,$currency_id);  //给发送方加入日志
					$currency_id = 1;
					$this->addFinance($member_id,$typee,$content,$money,$currency_id);  //给发送方加入日志
				}
				//************************************奖     励************************************
				//购买成功触发奖励
				$member = $this->getMemberById($member_id);
				//1级称号奖
				$this->chenghao_one($member_id);
				//直推奖励
				$this->zhitui($member);
				//加业绩
				$this->addYeJi($member,$money);
				//加剩余业绩
				$this->addCengPengYeJi($member,$money);
				//************************************奖     励************************************
				$this->success('购买成功',U('Money/searchJihuo'));
			}else{
				$this->error('购买失败');
			}

		}else{
			$this->error('请求异常');
		}
	}
	/*********************************************************************************/
	public function kaitong(){
		$user=I('username');
		$user=$this->getMemberByUsername($user);
		if(empty($user)){
			$this->error('此用户已经激活');
		}
		if($user['is_kaitong']!=0){
			$this->error('此用户已经激活');
		}
		$mills=$this->getMemberMillsByMemberId($user['member_id']);
		if(!empty($mills)){
			$this->error('此用户已经拥有矿机');
		}
		$my=$this->getMemberByMemberId($_SESSION['USER_KEY_ID']);
		if($my['jihuobi']<$this->mills['jihuomoney']){
			$this->error('您的激活币不足');
		}
		$this->addMillsByMemberId($user['member_id']);
		$where['member_id']=$_SESSION['USER_KEY_ID'];
		M('Member')->where($where)->setDec('jihuobi',$this->mills['jihuomoney']);
		$where['member_id']=$user['member_id'];
		M('Member')->where($where)->setField('is_kaitong',1);
		addFinanceLog($this->member['member_id'],64,'开通会员'.$user['eamil'],$this->mills['jihuomoney'],0);
		$this->success('开通成功');
	}

	private function getMemberMillsByMemberId($member_id){
		$where['member_id']=$member_id;
		$where['kuangji_id']=1;
		return M('Kuangji_log')->where($where)->find();
	}
	private function addMillsByMemberId($member_id){
		$kuangji_info=M('Kuangji')->where('kuangji_id = 1')->find();
		//购买矿机，添加日志
		$data['kid']=1;
		$data['member_id']=$member_id;
		$data['buy_time']=time();
		$data['add_time']=time();
		$data['backday']=$kuangji_info['usetime'];
		$data['useday']=$kuangji_info['usetime'];
		$data['status']=0;
		$r=M('Kuangji_log')->add($data);
		return $r;
	}
	/**
	 * 奖励开始
	 * 12.22褚天恩
	 * 推荐奖
	 */
	public function zhitui($member){
		//为推荐人加推荐人数
		$zhitui_id = $member['zhuceid'];
		$res = M('Member')->where(array('member_id'=>$zhitui_id))->setInc('zhitui_num',1);
		//查看用户zhutui_num多少如果是9判断超出一天没有
		$tuijian = $this->getMemberById($zhitui_id);
		if($tuijian['zhitui_num'] == 9){
			$first_time = $tuijian['add_time'];
			$aa = time()-$first_time;
			//判断是否在一天时间内
			if($aa<86400){
				if($tuijian['type'] !=0){
					$tt['vip'] = 1;
					M('Member')->where(array('member_id'=>$zhitui_id))->save($tt);
					//成为vip
					$arr['member_id'] = $zhitui_id;
					$arr['level'] = 2;
					$arr['status'] = 0;
					$arr['add_time'] = time();
					M('Chenghaojiang')->add($arr);
				}
			}
		}
		for($i=9;$i>0;$i--){
			$zhitui_num = $this->config['zhitui_'.$i];
			if($tuijian['zhitui_num']>$zhitui_num-1){
				//满足升级条件
				$data['zhitui_level'] = $i;
				$res = M('Member')->where(array('member_id'=>$zhitui_id))->save($data);
				//修改直推奖励表
				$this->addZhiTui($zhitui_id,$member);

				break;
			}
		}
		return $res;
	}
	/**
	 * 直推等级升级触发奖励   
	 * 为会员冻结钱包加钱
	 * @param unknown $user  被推荐的用户信息
	 * @param unknown $id
	 * @return unknown
	 */
	public function addZhiTui($id,$user){
		
		$list = M('Zhitui')->where(array('member_id'=>$id))->find();
		$member = $this->getMemberById($id);
		if($member['type'] == 0){
			return true;
		}
		
		if($list){
			//不是第一次升级
			$zhitui_level = $member['zhitui_level'];
			$ago_level = $zhitui_level-1;
			$money = $this->config['zhitui_jiangli_'.$ago_level];
			$money_now = $this->config['zhitui_jiangli_'.$zhitui_level];
			$difference = $money_now-$money;
			$res = M('Zhitui')->where(array('member_id'=>$id))->setInc('money_shengyu',$difference);
			//为冻结钱包加钱
// 			$re = M('Member')->where(array('member_id'=>$id))->setInc('forzen',$difference);
			//添加财务日志
			$this->addFinance($id,35,"分享奖奖励，加入冻结钱包，来自".$user['account'],$difference,2);
		}else{
			$data['member_id'] = $id;
			$data['time_shengyu'] = 50;
			$data['money_shengyu'] = $this->config['zhitui_jiangli_1'];
			$data['status'] = 0;
			$res = M('Zhitui')->add($data);
			//为冻结钱包加钱
// 			$re = M('Member')->where(array('member_id'=>$id))->setInc('forzen',$data['money_shengyu']);
			//添加财务日志
			$this->addFinance($id,35,"分享奖奖励，加入冻结钱包，来自".$user['account'],$data['money_shengyu'],2);
		}
		return $res;
	}
	/**
	 * 根据id获取用户信息
	 * @param unknown $id
	 * @return unknown
	 */
	public function getMemberById($id){
		$map['member_id'] = $id;
		$list = M('Member')->where($map)->find();
		return $list;
	}

	/**
	 * 为上线增长业绩
	 * @param 入金的会员信息 $user
	 * @param 入金金额 $money
	 */
	public function addYeJi($user,$money){
		$id = $user['member_id'];
		$list = $this->getUpUserByjiedianId($id,9999);
		for($i=1;$i<count($list);$i++){
			if($list[$i-1]['quyu'] == 'righ'){
				$res[] = M('Member')->where('member_id='.$list[$i]['member_id'])->setInc('yeji_righ',$money);
			}else{
				$res[] = M('Member')->where('member_id='.$list[$i]['member_id'])->setInc('yeji_left',$money);
			}
			//调用称号奖
			$this->chenghaoJiang($list[$i]['member_id']);
			//为上线加业绩
			$res[] = M('Member')->where('member_id='.$list[$i]['member_id'])->setInc('yeji',$money);
			
		}
		return $res;
	}
	/**
	 * 1级称号奖
	 * @param unknown $id
	 * @return unknown
	 */
	public function chenghao_one($id){
		$map['member_id'] = $id;
		$map['level'] = 1;
		$chenghao = M('Chenghaojiang')->where($map)->find();
		
		if(empty($chenghao)){
			$data['member_id'] = $id;
			$data['level'] = 1;
			$data['status'] = 0;
			$data['add_time'] = time();
			$re = M('Chenghaojiang')->add($data);
			$pic_zhengshu = $this->addwater($this->config['quanyizheng'],$id);
			M('Member')->where("`member_id`={$id}")->setField('pic',$pic_zhengshu);
		}
		return $re;
	}
	


	public function aaaa(){
		$id= 95;
		$money = 50000;
		$this->chenghaoJiang($id);
	}
	
	
	
	/**
	 * 称号奖
	 * @param unknown $member_id
	 * @return unknown
	 */
	public function chenghaoJiang($member_id){
		$member = $this->getMemberById($member_id);
		$xiaoqu = min($member['yeji_righ'],$member['yeji_left']);
		for($i=7;$i>2;$i--){
			$tiaojian = $this->config['chenghao_tiaojian_'.$i]*10000;
			if($xiaoqu>=$tiaojian){
				$map['member_id'] = $member_id;
				$map['level'] = $i;
				$list =  M('Chenghaojiang')->where($map)->find();
				if(empty($list)){
					$data['member_id'] = $member_id;
					$data['level'] = $i;
					$data['status'] = 0;
					$data['add_time'] = time();
					$res = M('Chenghaojiang')->add($data);
				}
				break;
			}
			
		}
		return $res;
	}
	
	/**
	 * 无限代层碰对碰奖
	 * @param unknown $user
	 * @param unknown $money
	 * @return unknown
	 */
	public function addCengPengYeJi($user,$money){
		$id = $user['member_id'];
		$list = $this->getUpUserByjiedianId($id,9999);
		for($i=1;$i<count($list);$i++){
			if($list[$i-1]['quyu'] == 'righ'){
				//给第i层加
				$where['member_id'] = $list[$i]['member_id'];
				$where['layer'] = $i;
				$layer = M('layer')->where($where)->find();
				if($layer){
					$res = M('Layer')->where($where)->setInc('righ_yeji',$money);
				}else{
					$data['member_id'] = $list[$i]['member_id'];
					$data['layer'] = $i;
					$data['left_yeji'] = 0;
					$data['righ_yeji'] = $money;
					$res =M('Layer')->add($data);
				}
			}else{
				$where['member_id'] = $list[$i]['member_id'];
				$where['layer'] = $i;
				$layer = M('layer')->where($where)->find();
				if($layer){
					$res = M('Layer')->where($where)->setInc('left_yeji',$money);
				}else{
					$data['member_id'] = $list[$i]['member_id'];
					$data['layer'] = $i;
					$data['left_yeji'] = $money;
					$data['righ_yeji'] = 0;
					$res = M('Layer')->add($data);
				}
			}
			$map['member_id'] = $list[$i]['member_id'];
			$map['layer'] = $i;
			$member_layer = M('Layer')->where($map)->find();
			$member = M('Member')->where(array('member_id'=>$list[$i]['member_id']))->find();
			if($member['type'] == 0){
				continue;
			}
			if($member_layer['left_yeji']>0 && $member_layer['righ_yeji']>0){
				$yeji=min($member_layer['left_yeji'],$member_layer['righ_yeji']);
				if ($yeji>0) {
					//对碰
					$bili = $this->config['duipengbili_'.$member['zhitui_level']]*0.01;
					$money_jiangjin=$yeji*$bili;
					
					//判断今天获得的对碰奖
					$duipeng = $this->panduan($list[$i]['member_id']);
					$aa = $money_jiangjin+$duipeng;
					if($aa>100000){
						continue;
					}
					if ($money_jiangjin>0){
						//加钱
						M("Member")->where(array('member_id'=>$list[$i]['member_id']))->setInc('jiangli',$money_jiangjin);
						//冻结钱包加钱
						M('Member')->where(array('member_id'=>$list[$i]['member_id']))->setInc('forzen',$money_jiangjin);
						//添加到奖励释放计划表中
						$this->jiangJinShiFang($list[$i]['member_id'],$money_jiangjin,1);//动力奖type为1 
						
						$this->addFinance($member['member_id'],36,"动力奖奖励，加入冻结钱包，来自".$user['account'],$money_jiangjin,2);
						//产生动力奖之后为上线加***************管      理      奖***************************************************************
						$this->guanliJiang($list[$i]['member_id'],$money_jiangjin);
			
						//对碰之后开始减钱
						M('Layer')->where($map)->setDec("left_yeji",$yeji);
						M('Layer')->where($map)->setDec("righ_yeji",$yeji);
					}
				}
			}
		}
		return $res;
	}
	/**
	 * 获得今天获得的动力奖金额
	 * @param 用户id $id
	 * @return unknown|string
	 */
	public function panduan($id){
		$time = strtotime(date('Ymd'));
		$end_time = $time+86400;
		$where_new['member_id'] = $id;
		//奖励类型1 对碰奖
		$where_new['type'] = 1;
		$where_new['add_time'] = array('between',array($time,$end_time));
		$duipeng = M('Jiangli')->where($where_new)->sum('money');
		if($duipeng){
			return $duipeng;
		}else{
			return '0';
		}
	}
	/**
	 * 管理奖  2017.1.1
	 * @param unknown $id
	 * @param unknown $money
	 * @return unknown
	 */
	public function guanliJiang($id,$money){
		$list = $this->getUpUserByTuijianId($id,10);
		for($i=9;$i>0;$i--){
			if($list[$i]){
				if($list[$i]['type'] == 0){
					continue;
				}
				$bili = $this->config['guanli_'.$i]*0.01;
				$jiangjin = $money*$bili;
				if ($jiangjin>0){
					//加钱
					$res = M("Member")->where(array('member_id'=>$list[$i]['member_id']))->setInc('jiangli',$jiangjin);
					//冻结钱包加钱
					M('Member')->where(array('member_id'=>$list[$i]['member_id']))->setInc('forzen',$jiangjin);
					//添加到奖励释放计划表中
					$this->jiangJinShiFang($list[$i]['member_id'],$jiangjin,2);//管理奖type为2
					$this->addFinance($list[$i]['member_id'],37,$i."代管理奖奖励，加入冻结钱包，来自".$list[0]['account'],$jiangjin,2);
				
				}
			}
		}
		return $res;
	}
	
	/**
	 * 释放计划表添加
	 * @param unknown $id
	 * @param unknown $money
	 */
	public function jiangJinShiFang($id,$money,$type){
		//添加奖励释放表
		$data['member_id'] = $id;
		$data['add_time'] = time();
		$data['money'] = $money;
		$data['type'] = $type;//状态值type为1  动力奖
		$data['status'] = 0;
		M('jiangli')->add($data);
	}
	
	/**
	 *	查询会员的推荐人
	 * @param unknown $tuijianid
	 * @param unknown $lv 需要查几代上线
	 * @param number $k 默认0
	 * @return Ambigous <unknown, mixed, boolean, NULL, multitype:, string>
	 */
	public function getUpUserByTuijianId($id,$lv,$k=0){
		$user=M("Member")->where("member_id='$id'")->find();
		$k++;
		if (!empty($user)) {
			$item[]=$user;
			if ($k<$lv) {
				$r=$this->getUpUserByTuijianId($user['zhuceid'],$lv,$k);
				foreach ($r as $v){
					if (!empty($v)){
						$item[]=$v;
					}
				}
			}
		}
		return $item;
	}

	//通过节点ID查找上级
	public function getUpUserByjiedianId($id,$lv,$k=0){
		$user=M("Member")->where("member_id='$id'")->find();
		$k++;
		if (!empty($user)) {
			$item[]=$user;
			if ($k<$lv) {
				$r=$this->getUpUserByjiedianId($user['jiedianid'],$lv,$k);
				foreach ($r as $v){
					if (!empty($v)){
						$item[]=$v;
					}
				}
			}
		}
		return $item;
	}

		/**
	*强制复投页面
	*/
	public function reg_futou(){
		if(empty($_SESSION['USER_KEY_ID'])){
			$this->error('异常访问');
		}
		$user = M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->find();
		if($user['type'] != 1 || $user['rmb'] <= 25000 ){
			$this->error('异常访问');
		}
		return $this->fetch();
	}
	
	

	/**
	 * 复投处理
	 */
	public function add_futou() {
		
		if(empty($_SESSION['USER_KEY_ID'])){
			$data['info'] = "异常复投";
			$data['status'] = -1;
			return json($data);
		}
		$user = M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->find();
		if($user['type'] != 1 || $user['rmb'] <= 25000 ){
			$data['info'] = "异常复投";
			$data['status'] = -1;
			return json($data);
		}
		if($user['integral'] < 25000){
			$data['info'] = "权益值不足；请联系管理员充值";
			$data['status'] = -1;
			return json($data);
		}
		
		$Users = M ( "Member" );
		$user=$Users->where("account='".$_POST['uname']."'")->find();
		if($user){
			$data['info'] = "用户名重复";
			$data['status'] = -1;
			return json($data);
		}
		if (empty ( $_POST ['pwd'] )) {
			$data['info'] = "请输入密码";
			$data['status'] = -2;
			return json($data);
		}
		if ( $_POST ['pwd']!=$_POST['twopwd'] ) {
			$data['info'] = "密码输入不相同";
			$data['status'] = -3;
			return json($data);
		}
		if ( $_POST ['topwd']!=$_POST['twotopwd'] ) {
			$data['info'] = "二级密码不相同";
			$data['status'] = -4;
			return json($data);
		}

		$list ['jiedianid'] = I('jiedianid'); // 接点人!!!!!!!!!!!!!!
		if (empty ( $_POST ['jiedianid'] )) {
			$data['info'] = "请输入接点人";
			$data['status'] = -5;
			return json($data);
		}
		$contacts = $Users->where("account = '".$list ['jiedianid']."'")->find();//判断是否有这个接点人
		if(!$contacts){
			$data['info'] = "无此接点人";
			$data['status'] = -6;
			return json($data);
		}
		if($contacts['status']<1){
			$this->error("接点人未开通");
			$data['info'] = "接点人未开通";
			$data['status'] = -7;
			return json($data);
		}
		if (empty ( $_POST ['quyu'] )) {
			$data['info'] = "分支不能为空";
			$data['status'] = -8;
			return json($data);
		}
		
		if ($_POST ['quyu']=='righ') {
			$left=$Users->where("jiedianid='".$contacts ['member_id']."' and quyu='left'")->find();
			if(empty($left)){
				$data['info'] = "该用户左区还没有用户";
				$data['status'] = -9;
				return json($data);
			}
			if($left['zhuceid'] != $left['jiedianid']){
				$ttt = I('tuijianid');
				//判断接点人是不是第一次推荐
				if($contacts['num'] == 0){
					if($ttt == $list ['jiedianid']){
						$data['info'] = "该用户不能放置在这";
						$data['status'] = -30;
						return json($data);
					}
				}
			}
		}
		$fenzhi=$Users->where("jiedianid='".$contacts ['member_id']."' and quyu='".I ( 'quyu' )."'")->find();
		
		if(!empty($fenzhi)){
			$data['info'] = "此分支已经有用户了";
			$data['status'] = -9;
			return json($data);
		}
		// 		$this->isBaodan($_POST ['jiedianid']);
		$list ['password'] = md5(I('pwd')); // 接点人!!!!!!!!!!!!!!
		$list ['securitypwd'] = md5(I('topwd')); // 接点人!!!!!!!!!!!!!!
		$list ['quyu'] = I ( 'quyu' ); // 分支
		$tuijianid = I('tuijianid');//获取推荐信息
		if($tuijianid){
			$tuijian = $Users->where("account = '$tuijianid'")->find();//判断是否有这个推荐人
			if (!$tuijian){
				$data['info'] = "无此推荐人";
				$data['status'] = -10;
				return json($data);
			}
			if($tuijian['num']>8){
				$data['info'] = "此用户无法再直推";
				$data['status'] = -20;
				return json($data);
			}	
		}
		$list ['account'] = trim(I('uname'));
		$list ['name'] = I('fullname');
		$list ['idcard'] = I('shenfenzheng');
		$list ['phone'] = I('tel');
		$list ['sex'] = I('sex');
		$list ['zhuceid'] = $tuijian['member_id'];
		$list ['jiedianid'] = $contacts['member_id'];
		$list ['status'] =1;//未激活
		$list ['add_time']=time();
		$list ['type']=1;
		//注册
		$ure = $Users->add ($list);
		if($ure){
			/***为下线增加资产包***/
			$data_a['integral'] = 75000;
			$data_a['member_id'] = $ure;
			$data_a['btype'] = 1;
			$data_a['zichanbao_id'] = 1;
// 			$data_a['time_shengyu'] = 50;
			$data_a['time_shengyu'] = $this->config['shifang_time'];
			$data_a['add_time'] = time();
			$data_a['status'] = 0;
			$re = M('Member_zichanbao')->add($data_a);
			if($re){
				
					M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->setDec('integral',25000);
					M('Member')->where("member_id={$_SESSION['USER_KEY_ID']}")->setDec('rmb',25000);
				
				
				/**************加入日志*****************/
				
				$typee = 30;
				$content = '花费25000复投宝+25000权益值进行复投';
				
				$money = 50000;
				$money_type = 2;
				$currency_id = 2;
				$this->addFinance($_SESSION['USER_KEY_ID'],$typee,$content,$money,$currency_id);  //给发送方加入日志
				
				$member_id = $ure;
				$typee = 30;
				$content = '被复投账号自带5万资产包';
				
				$money = 0;
				$money_type = 2;
				$currency_id = 2;
				$this->addFinance($member_id,$typee,$content,$money,$currency_id);  //给发送方加入日志
			
				//************************************奖     励************************************
				//购买成功触发奖励
				$member = $this->getMemberById($member_id);
				//1级称号奖
				$this->chenghao_one($member_id);
				//直推奖励
				$this->zhitui($member);
				//加业绩
				$this->addYeJi($member,50000);
				//加剩余业绩
				$this->addCengPengYeJi($member,$money);
				//************************************奖     励************************************
		
			//注册成功为上线num加1
			$re = M('Member')->where(array('member_id'=>$tuijian['member_id']))->setInc('num',1);
			$this->success ( "注册成功" );
			$data['info'] = "注册成功";
			$data['status'] = 1;
			return json($data);
		}else{
			$data['info'] = "注册失败";
			$data['status'] = -11;
			return json($data);
		}
	}

}


	/** 
	 * 提现页面
	 */
	public function withdraw(){
		$session = session::get();
		$where['member_id'] = $session['USER_KEY_ID'];
		// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
		$list = Db::name('Withdraw')->where($where)->order('add_time desc')->paginate(10);
		$page = $list->render();
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page', $page);
		
		$type = Db::name('App_account_type')->where(array('tixian'=>1))->select();
		$this->assign('type',$type);
		$member_account = db('App_member_account')->where(array('member_id'=>$session['USER_KEY_ID']))->find();
		$this->assign('member_account',$member_account);
		$member =Db::name('App_member_relation')->where(array('member_id'=>$session['USER_KEY_ID']))->find();
		$this->assign('member',$member);
		return $this->fetch();
	}
	/**
	 * 提现操作
	 */
	public function tixian(){
		if(empty($_POST['cardnum'])){
			return json(array('result_code'=>99,'message'=>'请补全信息'));
		}
		
		$session = session::get();
		$member = Db::name('App_member_relation')->where(array('member_id'=>$session['USER_KEY_ID']))->find();
		if($member['level'] == 1){
			return json(array('result_code'=>11,'message'=>'普通会员无法提现'));
		}
		//$pwd = md5($_POST['pwd']);
// 		if($member['pwdtrade']==$pwd){
			$account = Db::name('App_member_account')->where(array('member_id'=>$member['member_id']))->find();
			switch($_POST['type']){
				case 1:
					$max = $account['account_type_1'];
					break;
				case 2:
					$max = $account['account_type_2'];
					break;
				case 3:
					$max = $account['account_type_3'];
					break;
				case 4:
					$max = $account['account_type_4'];
					break;
				case 5:
					$max = $account['account_type_5'];
					break;
			}
			$config = $this->configs();
			$money = $_POST['money'];
			if($money < $config['tixian_min']){
				return json(array('result_code'=>11,'message'=>'提现最小金额'.$config['tixian_min']));
			}
			if($money%$config['tixian_min'] != 0){
				return json(array('result_code'=>12,'message'=>'提现金额必须是'.$config['tixian_min'].'整数倍'));
			}
			$fee = $money*$config['tixian_fee']*0.01;
			$after_num = $money-$fee;
			if($money > $max){
				return json(array('result_code'=>101,'message'=>'您的积分余额不足，无法提现'));
			}else{
				$data['member_id'] = $member['member_id'];
				$data['fee'] = $fee;
				$data['num'] = $after_num;
				$data['all_money'] = $money;
				$data['status'] = 0;
				$data['add_time'] = time();
				$data['type'] = $_POST['pay'];
				if($_POST['pay'] == 1){
					$pay_type = '支付宝';
				}else if($_POST['pay'] == 2){
					$pay_type = '银行卡';
				}
				$data['cnt'] = $_POST['type'];
				Db::name('withdraw')->add($data);
				$Finance = new \FinanceApi();
				switch($_POST['type']){
					case 1:
						Db::name('App_member_account')->where(array('member_id'=>$data['member_id']))->setDec('account_type_1',$money);
						$bi = db('App_account_type')->where(['id'=>1])->find()['name'];
						$content = $_SESSION["think"]['USER_KEY']."提现".$money.$bi.",提现方式:".$pay_type.",号码:".$_POST['cardnum'];
						$Finance->addFinance($session['USER_KEY_ID'], 16, $content, 2, $money, 1);
						break;
					case 2:
						Db::name('App_member_account')->where(array('member_id'=>$data['member_id']))->setDec('account_type_2',$money);
						$bi = db('App_account_type')->where(['id'=>2])->find()['name'];
						$content = $_SESSION["think"]['USER_KEY']."提现".$money.$bi.",提现方式:".$pay_type.",号码:".$_POST['cardnum'];
						$Finance->addFinance($session['USER_KEY_ID'], 16, $content, 2, $money, 2);
						break;
					case 3:
						Db::name('App_member_account')->where(array('member_id'=>$data['member_id']))->setDec('account_type_3',$money);
						$bi = db('App_account_type')->where(['id'=>3])->find()['name'];
						$content = $_SESSION["think"]['USER_KEY']."提现".$money.$bi.",提现方式:".$pay_type.",号码:".$_POST['cardnum'];
						$Finance->addFinance($session['USER_KEY_ID'], 16, $content, 2, $money, 3);
						break;
					case 4:
						Db::name('App_member_account')->where(array('member_id'=>$data['member_id']))->setDec('account_type_4',$money);
						$bi = db('App_account_type')->where(['id'=>4])->find()['name'];
						$content = $_SESSION["think"]['USER_KEY']."提现".$money.$bi.",提现方式:".$pay_type.",号码:".$_POST['cardnum'];
						$Finance->addFinance($session['USER_KEY_ID'], 16, $content, 2, $money, 4);
						break;
					case 5:
						Db::name('App_member_account')->where(array('member_id'=>$data['member_id']))->setDec('account_type_5',$money);
						$bi = db('App_account_type')->where(['id'=>5])->find()['name'];
						$content = $_SESSION["think"]['USER_KEY']."提现".$money.$bi.",提现方式:".$pay_type.",号码:".$_POST['cardnum'];
						$Finance->addFinance($session['USER_KEY_ID'], 16, $content, 2, $money, 5);
						break;
				}
				return json(array('result_code'=>100,'message'=>'操作成功'));
			}
// 		}else{
// 			return json(array('result_code'=>101,'message'=>'提现密码错误'));
// 		}
	}
	/**
	 * 添加财务日志方法
	 * @param unknown $member_id
	 * @param unknown $type
	 * @param unknown $content
	 * @param unknown $money
	 * @param unknown $currency_id   1 注册积分   2分红积分  3奖金积分  4消费积分
	 * @return
	 */
	public function addFinance($member_id,$money,$content,$type,$currency_id){
		$data['member_id']=$member_id;
		$data['type']=$type;
		$data['content']=$content;
		$data['money']=$money;
		$data['add_time']=time();
		$data['currency_id']=$currency_id;
		$data['ip'] = get_client_ip();
		$list=M('K_finance')->add($data);
		if($list){
			return $list;
		}else{
			return false;
		}
	}
	/**
	 * 充值页面
	 */
	public function chongzhi(){
		$where["member_id"] = $_SESSION['think']['USER_KEY_ID'];
		$list = Db::name('App_chongzhi')->where($where)->order('add_time desc')->paginate(10);
		$page = $list->render();
        $member =Db::name('App_member_relation')->where(array('member_id'=>$_SESSION['think']['USER_KEY_ID']))->find();
        
        //转换记录
        $where['member_id'] =$_SESSION['think']['USER_KEY_ID'];
        $where['finance_type'] = 2;
        $finance = Db::name('App_finance')->where($where)->order('add_time desc')->paginate(10);
        //     	dump($finance);
        $page = $finance->render();
        
        
        $this->assign('finance',$finance);
        $config = $this->configs();
        $this->assign('config',$config);
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page', $page);
		return $this->fetch();
	}
	//积分充值
	public function chongzhisave(){
		$data['member_id'] = $_SESSION['think']['USER_KEY_ID'];
		$data['num'] = input('money');
		$data['add_time'] = time();
		$data['status'] = 0;
		$data['type'] = input('type');
		$data['account_type'] = input('account_type');
		$result = db('App_chongzhi')->insert($data);
		if($result!==false){
			return json(array('status'=>1,'info'=>'添加成功'));
		}else{
			return json(array('status'=>2,'info'=>'系统忙请稍后再试'));
		}
	}

	
}