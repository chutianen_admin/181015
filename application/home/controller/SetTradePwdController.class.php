<?php
namespace Home\Controller;
use Home\Controller\HomeController;

class SetTradePwdController extends HomeController {
    //空操作
    public function _initialize(){
    	parent::_initialize();
    	session('two_pwd_check',1);
    	if($this->member['pwdtrade']){
    		$this->redirect('Index/index');
    	}
    	
    }
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    public function index(){
        $this->display();
    }
    /**
     * 修改安全密码
     */
    public function setTradePwd(){
    	$new_password     = I('post.new_trade_password');
    	$confirm_password = I('post.confirm_password');
    	if(md5($new_password)!=md5($confirm_password)){
    		$data['status'] = -2;
    		$data['info'] = '您的确认密码输入有误';
    		$this->ajaxReturn($data);
    	}
    	if(!checkPwd($new_password)){
    		$data['status'] = -4;
    		$data['info'] = '请输入正确的密码格式';
    		$this->ajaxReturn($data);
    	}
    	if(md5($new_password)==$this->member['password']){
    		$data['status'] = -5;
    		$data['info'] = '安全密码不能与登录密码一致';
    		$this->ajaxReturn($data);
    	}
    	//修改数据库
    	$data['member_id'] = session('USER_KEY_ID');
    	$data['pwdtrade']  = md5($new_password);
    	$r=M('Member')->save($data);
    	if($r){
    		$data['status'] = 1;
    		$data['info'] = '提交成功';
    		$this->ajaxReturn($data);
    	}else{
    		$data['status'] = -6;
    		$data['info'] = '服务器繁忙,请稍后重试';
    		$this->ajaxReturn($data);
    	}
    }
}