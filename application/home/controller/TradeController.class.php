<?php
namespace Kuang\Controller;
use Kuang\Controller\KuangController;

class TradeController extends KuangController {
    //空操作
    public function _initialize(){
        parent::_initialize();
    }
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    
    //交易大厅
    public function index(){
    	
    	//获得用户信息
    	$member_id = $_SESSION['USER_KEY_ID'];
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	$goods_user = M('Goods_member')->where(array('member_id'=>$member_id))->find();
    	//注册积分持有量
    	$member_account = M('Member_account')->where(array('member_id'=>$member_id))->find();
    	//求现在股票金额
    	$where['status'] = 1;
    	$where['type'] = 'buy';
    	$last_order = M('Orders')->where($where)->order('add_time desc')->find();
    	if(empty($last_order)){
    		$last_order['price'] = 1; 
    	}
    	
    	$shizhi = $last_order['price']*$goods_user['num'];
    	//查看自己购买的股票记录
    	//查看自己等待的订单
    	$map['member_id'] = $member_id;
    	$map['status'] = 0;
    	$map['type'] = 'buy';
    	$list = M('Orders')->where($map)->select();
    	
    	//查询下1股的价格
    	$aaa = $this->getNowPrice(1);
    	$price = $aaa[0]['money'];	
    	$this->assign('list',$list);
    	$this->assign('shizhi',$shizhi);
    	
    	$this->assign('last_order',$last_order);
    	
    	$this->assign('member_account',$member_account);
    	$this->assign('price',$price);
    	$this->assign('goods_user',$goods_user);
    	$this->assign('member',$member);
        $this->display();
    }

    /**
     * 股票购买方法
     */
    public function buyOrders(){
    	$member_id = $_SESSION['USER_KEY_ID'];
    	$num = I('num');
    	$password = I('password');
    	//用户信息
    	$member = M('Member')->where(array('member_id'=>$member_id))->find();
    	if(empty($member)){
    		$data['info'] = '请先登录再进行此操作';
    		$data['status'] = -1;
    		$this->ajaxReturn($data);
    	}
    	if($member['pwdtrade'] != md5($password)){
    		$data['info'] = '二级密码不正确';
    		$data['status'] = -2;
    		$this->ajaxReturn($data);
    	}
    	$switch = M('Switch')->where(array('member_id'=>$member_id))->find();
    	if($switch['out_limit'] == 1){
    		$data['info'] = '您已出局无法继续挂单';
    		$data['status'] = -5;
    		$this->ajaxReturn($data);
    	}
    	
    	//判断买入数量
    	if(is_int($num)){
    		$data['status']=-3;
    		$data['info']='数量有误';
    		$this->ajaxReturn($data);
    	}
    	//数量小于零
    	if($num<=0){
    		$data['status']=-4;
    		$data['info']='数量有误';
    		$this->ajaxReturn($data);
    	}
    	//求现在股票金额
    	$money = $this->getNowPrice($num);
//     	dump($money);
//     	die();
    	$j = count($money);
    	$h = $j-1;
    	for($i=0;$i<$j;$i++){
    		$trade_order_money += $money[$i]['all_money'];
    		if($i == $h){
    			$price = $money[$i]['money'];
    		}
    	}
    	//账户积分信息
    	$member_account = M('member_account')->where(array('member_id'=>$_SESSION['USER_KEY_ID']))->find();
    	if(floatval($trade_order_money) > floatval($member_account['account_type_1'])){
    		$data['status']=-7;
    		$data['info']='资金余额不足';
    		$this->ajaxReturn($data);
    	}
    	$arr['member_id'] = $member_id;
    	$arr['type'] = 'buy';
    	$arr['price'] =  $price;
    	$arr['num'] = $num;
    	$arr['add_time'] = time();
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	$arr['ratio_id'] = $ratio['id'];
    	$arr['all_money'] = $trade_order_money;
    	$res = M('Orders')->add($arr);
    	if($res){
    		//Ratio表添加数量
    		M('Ratio')->where(array('status'=>0))->setInc('num',$num);
    		
    		//计算金额
    		$trade_money = $trade_order_money;
    		//账户余额
    		$r[] = $this->subMoney($member_id,$trade_money,'account_type_1');
    		$data['info'] = '成功';
    		$data['status'] = 1;
    		$this->ajaxReturn($data);
    	}else{
			$data['info'] = '网络异常';
			$data['status'] = -2;
			$this->ajaxReturn($data);
		}
    }
  	
    public function aa(){
    	$aa = $this->getNowPrice(201);
    	dump($aa);
    	
    }
    /**
     * 获得当前股票交易金额
     * @param unknown $now_num
     * @return number
     */
    public function getNowPrice($now_num){//200
    	$where['type'] = 'buy';
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	$where['ratio_id'] = $ratio['id'];
  		//当前比例的数量
    	$num = $ratio['num'];
    	$ttt = $this->ratio();
    	if($ttt){
    		$price = $ttt;
    	}else{
    		$order = M('Orders')->where($where)->order('add_time desc')->find();
    		$price = $order['price'];
    	}
		$base = $this->config['vr_danwei'];
    	$bili = $this->config['vr_money'];
    	//取余
    	$r = fmod($num,$base); 
    	//没满足第一次涨价的股票量
    	$a = $base-$r;		
    	if($now_num>$a){		        
    		$f = $a;
    	}else{
    		$f = $now_num;		//1  
    	}
    	//满足涨价的股票量
    	$b = $now_num-$f;   	
    	//涨了几次价格 (取整)
    	$c = ceil($b/$base);  
    	$d = fmod($b,$base);  
    	for($i=0;$i<$c+1;$i++){
    		if($i == 0){
    			$arr[$i]['num'] = $f;
    				//判断是否涨价
    				if($r == 0){
    					if($num !=0){
    						$now_bili = $bili;
    					}else{
    						$now_bili = $i*$bili;
    					}
    				}else{
    					$now_bili = $i*$bili;
    				}
    		}else{
    			if($i == $c){
    				if($d == 0){
    					//整除的
    					$arr[$i]['num'] = $base;
    				}else{
    					$arr[$i]['num'] = $d;
    				}
    				
    			}else{
    				$arr[$i]['num'] = $base;
    			}
    			$now_bili =	($i+1)*$bili;
    		}
    		$arr[$i]['money'] = $price+$now_bili;
    		$arr[$i]['all_money'] = $arr[$i]['money']*$arr[$i]['num'];
    	}
    	return $arr;
    }
    
    public function ratio(){
    	//查看股票进程表
    	$ratio = M('Ratio')->where(array('status'=>0))->find();
    	if($ratio){
    		if($ratio['num'] == 0){
    			$price = $ratio['first_price'];
    		}else{
    			return false;
    		}
    	}	
//     	echo '111111111111';
//     	dump($price);
    	return $price;
    }
    
    
    
    
    
  
	
   
    /**
     * 获得当前的st价格
     */
    public function getNowStPrice(){
    	$st_up_num  = $this->config['st_up_num'];
    	$st_up_bili = $this->config['st_up_bili'];
    	$all_money = M('Trade')->sum('num');
    	$num = floor($all_money/$st_up_num);
    	$money= $num*$st_up_bili;
    	$first_order = M('Orders')->where(array('order_id'=>1))->find();
    	$now_price = $first_order['price']+$money;
    	return $now_price; 
    }
    /**
     * 为用户加钱
     * @param 用户id $member_id
     * @param 金额   $money
     * @param 类型   $type rmb forzen
     * @return unknown
     */
    public function addMoney($member_id,$money,$type){
    	$res = M('Member_account')->where(array('member_id'=>$member_id))->setInc($type,$money);
    	return $res;
    }
    /**
     * 为用户减钱
     * @param 用户id $member_id
     * @param 金额   $money
     * @param 类型   $type
     * @return unknown
     */
    public function subMoney($member_id,$money,$type){
    	$res = M('Member_account')->where(array('member_id'=>$member_id))->setDec($type,$money);
    	return $res;
    }
   
    /**
     *
     * @param 买入用户id  $buy_member
     * @param 卖出用户id  $sell_member
     * @param 商品id     $goods_id
     * @param 数量                     $num
     * @param 金额                     $price
     * @param 买入订单id  $order_buy_id
     * @param 卖出订单id  $order_sell_id
     * @return unknown
     */
    public function addTrade($buy_member,$sell_member,$goods_id,$num,$price,$order_buy_id,$order_sell_id){
    	$data['buy_member'] = $buy_member;
    	$data['sell_member'] = $sell_member;
    	$data['goods_id'] = $goods_id;
    	$data['num'] = $num;
    	$data['price'] = $price;
    	$data['order_buy_id'] = $order_buy_id;
    	$data['order_sell_id'] = $order_sell_id;
    	$data['add_time'] = time();
    	$data['status'] = 1;
    	// 		dump($data);
    	$res = M('Trade')->add($data);
    	return $res;
    }

    /**
     * 用户商品持有量
     * @param 用户id $member_id
     * @param 商品id $goods_id
     * @param 商品数量   $num
     * @param 买卖   $type sell/buy
     * @return unknown
     */
    public function addGoodsUser($member_id,$goods_id,$num,$type){
    	$where['member_id'] = $member_id;
    	$where['goods_id'] = $goods_id;
    	$goods_user = M('Goods_member')->where($where)->find();
    	if($type == 'buy'){
    		if($goods_user){
    			$res = M('Goods_member')->where($where)->setInc('num',$num);
    		}else{
    			$data['num'] = $num;
    			$data['member_id'] = $member_id;
    			$data['goods_id'] = $goods_id;
    			$data['status']=0;
    			$res = M('Goods_member')->add($data);
    
    		}
    	}
    	if($type == 'sell'){
    		if($goods_user){
    			$res = M('Goods_member')->where($where)->setDec('num',$num);
    		}
    	}
    	return $res;
    }
    /**
     * 买入记录页面
     */
    public function mairu(){
    	$member_id = $_SESSION['USER_KEY_ID'];
    	//查看自己购买的股票记录
    	$list = M('Trade')->where(array('buy_member'=>$member_id))->select();
    	foreach($list as $k=>$v){
    		$list[$k]['money'] = $list[$k]['price']/$list[$k]['num'];
    	}
    	$this->assign('list',$list);
    	$this->display();
    }
    
    /**
     * 股票拆分页面
     */
    public function chaifen(){
    	
    	$this->display();
    }
	public function selling(){
		$member_id = $_SESSION['USER_KEY_ID'];
		$where['member_id'] = $member_id;
		$where['type'] = 'sell';
		$orders = M('Orders')->where($where)->select();
		foreach($orders as $k=>$v){
			$orders[$k]['all_money'] = $orders[$k]['num']*$orders[$k]['price'];
		}
		$this->assign('orders',$orders);
		$this->display();
	}
	/**
	 * 成交记录页面
	 */
	public function trade_list(){
		//查看自己完成的订单
		$map['member_id'] = $_SESSION['USER_KEY_ID'];
		$map['status'] = 1;
		$count      = M('Orders')->where($map)->count();// 查询满足要求的总记录数
		$Page       = new \Think\Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数(25)
		$show       = $Page->show();// 分页显示输出
		
		
		$list = M('Orders')->where($map)->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);
		$this->assign('page',$show);// 赋值分页输出
		$this->display();
	}
	
	
	public function maichu(){
		$this->display();
	}

	
	
	
	
	
	
	
	
	
}