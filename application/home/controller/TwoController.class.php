<?php

namespace Home\Controller;

use Home\Controller\HomeController;

class TwoController extends HomeController {
	// 空操作
	public function _initialize() {
		parent::_initialize ();
	}
	public function _empty() {
		header ( "HTTP/1.0 404 Not Found" );
		$this->display ( 'Public:404' );
	}
	public function twopwd() {
		
		$this->display ();
	}
	public function twopassword() {
		$tradepwd = I ( 'twopwd' );
		$id = $_SESSION ['USER_KEY_ID'];
		$user = M ( 'Member' )->where ( "member_id = '{$id}'" )->find ();
		if ($user ['pwdtrade'] != md5 ( $tradepwd )) {
			$this->error ( "二级密码不正确" );
			return;
		} else {
			$_SESSION ['checkTwoPwd_success'] = rand ( 111111, 999999 );
			$this->redirect ( session ('url') );
		}
	}
}