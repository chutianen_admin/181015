<?php 

namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Db;
use think\Session;
class User extends Base {
    //空操作
    public function _initialize(){
    	parent::_initialize();
    }
    public function _empty(){
    	
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    /**
     * 个人资料页面
     */
    public function index(){
    	$session = session::get();
		$member = Db::name('Users')->where(array('userId'=>$session['USER_KEY_ID']))->find();
		$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$session['USER_KEY_ID']))->find();
// 		$tuijian = Db::name('App_member_relation')->where(array('member_id'=>$member_relation['zhuceid']))->find();
// 		$jiedian = Db::name('App_member_relation')->where(array('member_id'=>$member_relation['jiedianid']))->find();
		$tuijian = Db::name('Users')->where(array('userId'=>$member_relation['zhuceid']))->find();
		$jiedian = Db::name('Users')->where(array('userId'=>$member_relation['jiedianid']))->find();
		$sex = $member_relation['sex']=1?'男':'女';
		$quyu = $member_relation['quyu']=1?'左区':'右区';
		$this->assign('member_relation',$member_relation);
		$this->assign('sex',$sex);
		$this->assign('quyu',$quyu);
		$this->assign('member',$member);
		$this->assign('tuijian',$tuijian);
		$this->assign('jiedian',$jiedian);
		return $this->fetch();
    }
    /**
     * 个人资料修改页面
     */
    public function revise_massage(){
    	$session = session::get();
    	$member = Db::name('Users')->where(array('userId'=>$session['USER_KEY_ID']))->find();
    	$member_relation = Db::name('App_member_relation')->where(array('member_id'=>$session['USER_KEY_ID']))->find();
    	$this->assign('member_relation',$member_relation);
    	$this->assign('member',$member);
    	return $this->fetch();
    }
    public function revise_password(){
    	
    	return $this->fetch();
    }
     
    //会员结构  系谱图
    public function xiputu(){

    	//二级密码验证
    	//$this->checkTwoPwd();
    	session('checkTwoPwd_success',null);
    	//程序
    	$username=I('get.username');
    	$username=!empty($username)?$username:$this->member['nick'];//获取前台传递的username，默认的就是本人
		$user=!empty($username)?M('Member')->where("nick='$username'")->find():$this->member;
		
		//dump($user);echo M('Member')->_sql();die;
		$this->assign('user',$user);
    	$this->assign('username',$username);
    	$one=M('Member')->where("nick='$username'")->find();
    	$two_one=$this->getUperUsername($one['member_id'],left);//A区
    	$two_two=$this->getUperUsername($one['member_id'],cent);//B区
    	$two_three=$this->getUperUsername($one['member_id'],right);//C区
    	$three_one=$this->getUperUsername($two_one['member_id'],left);//A区
    	$three_two=$this->getUperUsername($two_one['member_id'],cent);//B区
    	$three_three=$this->getUperUsername($two_one['member_id'],right);//C区
    	$three_four=$this->getUperUsername($two_two['member_id'],left);//A区
    	$three_five=$this->getUperUsername($two_two['member_id'],cent);//B区
    	$three_six=$this->getUperUsername($two_two['member_id'],right);//C区
    	$three_seven=$this->getUperUsername($two_three['member_id'],left);//A区
    	$three_eight=$this->getUperUsername($two_three['member_id'],cent);//B区
    	$three_nine=$this->getUperUsername($two_three['member_id'],right);//C区
    	$this->assign('one',$one);
    	$this->assign('two_one',$two_one);
    	$this->assign("two_two",$two_two);
    	$this->assign("two_three",$two_three);
    	$this->assign("three_one",$three_one);
    	$this->assign("three_two",$three_two);
    	$this->assign("three_three",$three_three);
    	$this->assign("three_four",$three_four);
    	$this->assign("three_five",$three_five);
    	$this->assign("three_six",$three_six);
    	$this->assign("three_seven",$three_seven);
    	$this->assign("three_eight",$three_eight);
    	$this->assign("three_nine",$three_nine);
    	$this->assign('level',$_POST['level']);
    	return $this->fetch();
    }
    //获取当前人的下线
    private function getUperUsername($id,$quyu){
    	$where['pid']=$id;
    	$where['quyu']=$quyu;
    	$list=M('Member')->where($where)->order("quyu asc")->find();
    	return $list;
    }
	
    /**
     * 修改登录密码
     */
    public function updatePwd(){
    	$old_password     = I('post.pwd');
    	$new_password     = I('post.newpwd_one');
    	$confirm_password = I('post.newpwd_two');
    	$session = session::get();
    	$member_id = $session['USER_KEY_ID'];
    	$user = Db::name('Users')->where(array('userId'=>$member_id))->find();
    	if(md5($old_password) != $user['loginPwd']){
    		$data['status'] = -1;
    		$data['info'] = '您的登录密码输入有误';
    		return json($data);
    	}
  		if(md5($new_password)!=md5($confirm_password)){
    		$data['status'] = -2;
    		$data['info'] = '您的确认密码输入有误';
    		return json($data);
    	}
    	if(md5($new_password) == $user['loginPwd']){
    		$data['status'] = -3;
    		$data['info'] = '新密码不能与旧密码一致';
    		return json($data);
    	}
//     	if(!checkPwd($new_password)){
//     		$data['status'] = -4;
//     		$data['info'] = '请输入正确的密码格式';
//     		return json($data);
//     	}
    	if(md5($new_password) == $user['payPwd']){
    		$data['status'] = -5;
    		$data['info'] = '新密码不能与安全密码一致';
    		return json($data);
    	}
    	//修改数据库
    	$data['loginPwd']        = md5($new_password);
    	$r = Db::name('Users')->where(array('userId'=>$member_id))->update($data);
    	if($r){
    		$data['status'] = 1;
    		$data['info'] = '提交成功';
    		return json($data);
    	}else{
    		$data['status'] = -6;
    		$data['info'] = '服务器繁忙,请稍后重试';
    		return json($data);
    	}
    }
    /**
     * 修改安全密码
     */
    public function updateTradePwd(){
    	$old_password     = I('post.pwdt');
    	$new_password     = I('post.newpwd_onet');
    	$confirm_password = I('post.newpwd_twot');
    	
    	$session = session::get();
    	$member_id = $session['USER_KEY_ID'];
    	$user = Db::name('Users')->where(array('userId'=>$member_id))->find();
    	if(md5($old_password) != $user['payPwd']){
    		$data['status'] = -1;
	    	$data['info'] = '您的旧密码输入有误';
	    	return json($data);
	    }
    	if(md5($new_password)!=md5($confirm_password)){
    		$data['status'] = -2;
    		$data['info'] = '您的确认密码输入有误';
    		return json($data);
    	}
    	if(md5($new_password) == $user['payPwd']){
    		$data['status'] = -3;
    		$data['info'] = '新密码不能与旧密码一致';
    		return json($data);
    	}
//     	if(!checkPwd($new_password)){
//     		$data['status'] = -4;
//     		$data['info'] = '请输入正确的密码格式';
//     		return json($data);
//     	}
    	if(md5($new_password) == $user['loginPwd']){
    		$data['status'] = -5;
    		$data['info'] = '新密码不能与登录密码一致';
    		return json($data);
    	}
    	//修改数据库
    	$data['payPwd']  = md5($new_password);
    	$r = Db::name('Users')->where(array('userId'=>$member_id))->save($data);
    	if($r){
    		$data['status'] = 1;
    		$data['info'] = '提交成功';
    		return json($data);
    	}else{
    		$data['status'] = -6;
    		$data['info'] = '服务器繁忙,请稍后重试';
    		return json($data);
    	}
    }
    //修改个人信息  程序
    public function check(){
    	if(IS_POST){
    		$reg = D('Member');
    		if (!$reg->create()){
    			// 如果创建失败 表示验证没有通过 输出错误提示信息
    			$data['status'] = 0;
    			$data['info'] = $reg->getError();
    			return json($data);
    			return;
    		}else{
    			$r = $reg->save();
    			if($r){
    				$data['status'] = 1;
    				$data['info'] = '提交成功';
    				return json($data);
    			}else{
    				$data['status'] = 0;
    				$data['info'] = '服务器繁忙,请稍后重试';
    				return json($data);
    			}
    		}
    	}else{
    		$data['status'] = 0;
    		$data['info'] = '服务器繁忙,请稍后重试';
    		return json($data);
    	}
    }
    /**
     * 修改账号密码
     */
    public function updatePassword(){
    	//二级密码验证
    	$this->checkTwoPwd();
    	session('checkTwoPwd_success',null);
    	//显示模板
        return $this->fetch();
    }


    // 推介图
    public function tuijian(){
// 		$uname=isset($_POST['uname'])?$_POST['uname']:$_SESSION['username'];
		$uname=$_SESSION['USER_KEY'];
		$user=M('Member')->where("email='".$this->member['email']."'")->find();
		$my=$this->getMemberByMemberId($_SESSION['USER_KEY_ID']);
// 		if (!$user) {
// 			$this->error("无此会员信息");
// 		}
// 		$user['utype']=$this->getTypeNameById($user['utype']);
		$down_user=M('Member')->where("zhuceid='{$user['member_id']}'")->select();
		$this->assign('down',$down_user);
		$this->assign('user',$user);
		$this->assign('my',$my);
		$this->display ();
    }

    public function getInivttuijian() {
    	$username = I('username');
    	$user=$this->getMemberByUsername($username);
    	$U = M ( 'Member' );
    	$r = $U->where ( "zhuceid=".$user['member_id'] )->select ();
    	if (! empty ( $r )) {
    		$data ['status'] = 1;
    		$data ['list'] = $r;
    		return json ( $data );
    	} else {
    		$data ['status'] = 0;
    		return json ( $data );
    	}
    }
    /**
     * 会员信息修改65
     */
    public function save_member(){
    	$session = session::get();
    	if(IS_POST){
    		$data['kaihuren']       =    input('kaihuren')?input('kaihuren'):null;
    		$data['bank_card']      =    input('bank_card')?input('bank_card'):null;
    		$data['zhifubao']       =    input('zhifubao')?input('zhifubao'):null;
    		$data['kaihuhang']      =    input('kaihuhang')?input('kaihuhang'):null;
    		$data['sex']      =    input('sex')?input('sex'):null;
    		if($data['bank_card']){
    			$res = $this->regex('bankcard', $data['bank_card']);
    			if(!$res){
    				$arr['status'] = -1;
    				$arr['info'] = '请输入正确的银行卡号';
    				return json($arr);
    			}
    		}
    		foreach($_POST as $v){
    			if(empty($v)){
    				$arr['status']=-1;
    				$arr['info']="请补全信息";
    				return json($arr);exit();
    			}
    		}
    		$re = DB::name("App_member_relation")->where(array('member_id'=>$session['USER_KEY_ID']))->update($data);
    		if($re){
    			$arr['status']=1;
    			$arr['info']="修改成功";
    		}else{
    			$arr['status']=-4;
    			$arr['info']="修改失败";
    		}
    		return json($arr);
    	}
    }
    /**
     * 正则验证
     * @param unknown $rule
     * @param unknown $value
     * @return boolean
     */
    function regex($rule,$value) {
    	$validate = array(
    			'email'     =>  '/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/',
    			'url'       =>  '/^http(s?):\/\/(?:[A-za-z0-9-]+\.)+[A-za-z]{2,4}(:\d+)?(?:[\/\?#][\/=\?%\-&~`@[\]\':+!\.#\w]*)?$/',
    			'currency'  =>  '/^\d+(\.\d+)?$/',
    			'number'    =>  '/^\d+$/',
    			'num'    	=>  '/^[0-9]*$/',
    			'zip'       =>  '/^\d{6}$/',
    			'integer'   =>  '/^[-\+]?\d+$/',
    			'double'    =>  '/^[-\+]?\d+(\.\d+)?$/',
    			'english'   =>  '/^[A-Za-z]+$/',
    			'idcard'	=>	'/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/i' ,
    			// 			'name'		=>  '/[^\D]/g',
    			'img'		=>	'/\.(jpg|gif|png)$/i',
    			'phone'		=>  '#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#',
    			'password'  =>  '/^[\w-\.]{6,16}$/',
    			'bankcard'  =>  '/^(\d{16}|\d{19})$/',
    			'username'  =>  '/^[a-zA-Z][a-zA-Z0-9_]{1,7}$/',
    	);
    	// 检查是否有内置的正则表达式
    	if(isset($validate[strtolower($rule)])){
    		$rule       =   $validate[strtolower($rule)];
    	}
    	return preg_match($rule,$value);
    }
    
    
    
    
    
    
    public  function updateTx(){
    	$id = $_SESSION['USER_KEY_ID'];
    	$upload = new \Think\Upload();// 实例化上传类
    	$upload->maxSize   =     3145728 ;// 设置附件上传大小
    	$upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
    	$upload->savePath  =      './goods/'; // 设置附件上传目录
    	// 上传文件
    	$info   =   $upload->upload();
    	if(!$info) {// 上传错误提示错误信息
    		if(!empty($id)){
    			$c_pic=$_POST['c_pic'];
    		}else{
    			$this->error($upload->getError());
    		}
    	}else{// 上传成功
    		$pic=$info['file']['savepath'].$info['file']['savename'];
    		$c_pic='/Uploads'.ltrim($pic,".");
    	}
    	 
    	$_POST['pic']=$c_pic;
    	$re=M("Member")->where(['member_id'=>$id])->save($_POST);
    	
    	//dump(M('Goods')->_sql());
    	//die();
    	if($re){
    		$data['status']=1;
    		$data['info']="修改成功,请稍等....";
    		return json($data);
    	}else{
    		$data['status']=-1;
    		$data['info']="修改失败,请稍等....";
    		return json($data);
    	}
    }
    
    
    
    
    
    
    
    
    
}